<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_base_ext');

return [
    'IMIA\ImiaBaseExt\Annotation\SQL\Table'              => $extPath . 'Classes/Annotation/SQL/Table.php',
    'IMIA\ImiaBaseExt\Annotation\SQL\Index'              => $extPath . 'Classes/Annotation/SQL/Index.php',
    'IMIA\ImiaBaseExt\Annotation\SQL\Column'             => $extPath . 'Classes/Annotation/SQL/Column.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Table'              => $extPath . 'Classes/Annotation/TCA/Table.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Column'             => $extPath . 'Classes/Annotation/TCA/Column.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Check'              => $extPath . 'Classes/Annotation/TCA/Check.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Copy'               => $extPath . 'Classes/Annotation/TCA/Copy.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Flex'               => $extPath . 'Classes/Annotation/TCA/Flex.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Group'              => $extPath . 'Classes/Annotation/TCA/Group.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Inline'             => $extPath . 'Classes/Annotation/TCA/Inline.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Input'              => $extPath . 'Classes/Annotation/TCA/Input.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputLink'          => $extPath . 'Classes/Annotation/TCA/InputLink.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputDate'          => $extPath . 'Classes/Annotation/TCA/InputDate.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputDateTime'      => $extPath . 'Classes/Annotation/TCA/InputDateTime.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputTime'          => $extPath . 'Classes/Annotation/TCA/InputTime.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputNumber'        => $extPath . 'Classes/Annotation/TCA/InputNumber.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputSlider'        => $extPath . 'Classes/Annotation/TCA/InputSlider.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputColorpicker'   => $extPath . 'Classes/Annotation/TCA/InputColorpicker.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\InputImagePosition' => $extPath . 'Classes/Annotation/TCA/InputImagePosition.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Passthrough'        => $extPath . 'Classes/Annotation/TCA/Passthrough.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\None'               => $extPath . 'Classes/Annotation/TCA/None.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Radio'              => $extPath . 'Classes/Annotation/TCA/Radio.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Select'             => $extPath . 'Classes/Annotation/TCA/Select.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Text'               => $extPath . 'Classes/Annotation/TCA/Text.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\TextRTE'            => $extPath . 'Classes/Annotation/TCA/TextRTE.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\User'               => $extPath . 'Classes/Annotation/TCA/User.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Files'              => $extPath . 'Classes/Annotation/TCA/Files.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\FilesImage'         => $extPath . 'Classes/Annotation/TCA/FilesImage.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\FilesPDF'           => $extPath . 'Classes/Annotation/TCA/FilesPDF.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\FilesVideo'         => $extPath . 'Classes/Annotation/TCA/FilesVideo.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\FilesVideoYoutube'  => $extPath . 'Classes/Annotation/TCA/FilesVideoYoutube.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Categories'         => $extPath . 'Classes/Annotation/TCA/Categories.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\SearchField'        => $extPath . 'Classes/Annotation/TCA/SearchField.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\AddToTypes'         => $extPath . 'Classes/Annotation/TCA/AddToTypes.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\AddToPalette'       => $extPath . 'Classes/Annotation/TCA/AddToPalette.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\AddPaletteToTypes'  => $extPath . 'Classes/Annotation/TCA/AddPaletteToTypes.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\AddPalettesToTypes' => $extPath . 'Classes/Annotation/TCA/AddPalettesToTypes.php',

    /* @deprecated since version 1.1.14 */
    'IMIA\ImiaBaseExt\Annotation\TCA\DateTime'           => $extPath . 'Classes/Annotation/TCA/DateTime.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Colorpicker'        => $extPath . 'Classes/Annotation/TCA/Colorpicker.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\Number'             => $extPath . 'Classes/Annotation/TCA/Number.php',
    'IMIA\ImiaBaseExt\Annotation\TCA\ImageFiles'         => $extPath . 'Classes/Annotation/TCA/ImageFiles.php',
];