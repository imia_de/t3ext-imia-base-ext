<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\IMIA\ImiaComposer\Utility\Composer::fetch();
\IMIA\ImiaBaseExt\Utility\DynamicConfiguration::build('localconf');

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'] = [];
}
array_unshift($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'],
    \IMIA\ImiaBaseExt\Hook\Core\TableConfigurationPostProcessing::class);

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'] = [];
}
array_unshift($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'],
    \IMIA\ImiaBaseExt\Hook\Core\TableConfigurationPostProcessingCached::class);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc'][$_EXTKEY] =
    \IMIA\ImiaBaseExt\Hook\Frontend\TypoScriptFrontendController::class . '->checkAlternativeIdMethods';

$GLOBALS['TYPO3_CONF_VARS']['SYS']['lang']['cache']['clear_menu'] = true;

/* Hooks */
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['extbase']['typeConverters'][] =
    \IMIA\ImiaBaseExt\Hook\Extbase\ArrayConverter::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc']['tx_imiabaseext'] =
    \IMIA\ImiaBaseExt\Hook\Core\DataHandler::class . '->clearCachePostProc';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['additionalBackendItems']['cacheActions']['tx_imiabaseext'] =
    \IMIA\ImiaBaseExt\Hook\Backend\ClearCacheActions::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_page.php']['getRecordOverlay'][$_EXTKEY] =
    \IMIA\ImiaBaseExt\Hook\Frontend\PageRepositoryGetRecordOverlayHook::class;

/* Xclasses */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Property\TypeConverter\DateTimeConverter::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\ViewHelpers\TranslateElementErrorViewHelper::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Form\ViewHelpers\TranslateElementErrorViewHelper::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Configuration\TypoScript\ConditionMatching\ConditionMatcher::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Configuration\TypoScript\ConditionMatching\ConditionMatcher::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\NodeFactory::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\NodeFactory::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataCompiler::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataCompiler::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\EvaluateDisplayConditions::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataProvider\EvaluateDisplayConditions::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\TcaRecordTitle::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataProvider\TcaRecordTitle::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\TcaColumnsOverrides::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataProvider\TcaColumnsOverrides::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\Element\SelectCheckBoxElement::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\Element\SelectCheckBoxElement::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\Element\InputTextElement::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Form\Element\InputTextElement::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Controller\Wizard\SuggestWizardController::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Controller\Wizard\SuggestWizardController::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Template\ModuleTemplate::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Backend\Template\ModuleTemplate::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Backend::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Backend::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extensionmanager\Controller\ConfigurationController::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extensionmanager\Controller\ConfigurationController::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extensionmanager\Utility\ConfigurationUtility::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extensionmanager\Domain\Repository\ConfigurationItemRepository::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extensionmanager\Domain\Repository\ConfigurationItemRepository::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extensionmanager\ViewHelpers\Form\TypoScriptConstantsViewHelper::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extensionmanager\ViewHelpers\Form\TypoScriptConstantsViewHelper::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\View\TemplatePaths::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Fluid\View\TemplatePaths::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Fluid\ViewHelpers\TranslateViewHelper::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Imaging\GifBuilder::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Frontend\Imaging\GifBuilder::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\DataHandling\DataHandler::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Core\DataHandling\DataHandler::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Database\ReferenceIndex::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Core\Database\ReferenceIndex::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Typolink\PageLinkBuilder::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Frontend\Typolink\PageLinkBuilder::class,
];

// extbase query parsing (onlyFields)
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Typo3QuerySettings::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbBackend::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Storage\Typo3DbBackend::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Mapper\DataMapper::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\Session::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Session::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Configuration\BackendConfigurationManager::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult::class] = [
    'className' => \IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\QueryResult::class,
];

if (!class_exists('Psr\Log\LoggerInterface') && !\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('install')) {
    try {
        new \TYPO3\CMS\Core\Log\Logger(null);
    } catch (\Exception $e) {
        if (!function_exists('rrmdir')) {
            function rrmdir($dir)
            {
                if (!file_exists($dir)) {
                    return true;
                }
                if (!is_dir($dir)) {
                    return unlink($dir);
                }

                foreach (scandir($dir) as $item) {
                    if ($item != '.' && $item != '..') {
                        if (!rrmdir($dir . DIRECTORY_SEPARATOR . $item)) {
                            return false;
                        }
                    }
                }

                return rmdir($dir);
            }
        }

        @rrmdir(PATH_site . 'typo3temp/var/Cache/Data/cache_classes');
    }
}

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class)->connect(
    \TYPO3\CMS\Install\Service\SqlExpectedSchemaService::class,
    'tablesDefinitionIsBeingBuilt',
    \IMIA\ImiaBaseExt\SignalSlot\SqlExpectedSchemaServiceSignalSlot::class,
    'tablesDefinitionIsBeingBuilt'
);

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon('actions-system-cache-clear-extensions', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, [
    'source' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_base_ext') . 'Resources/Public/Images/CacheExt.svg',
]);

$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
if ($extConf['cacheWarmer']) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['insertPageIncache'][$_EXTKEY] =
        \IMIA\ImiaBaseExt\Hook\Frontend\TypoScriptFrontendController::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] =
        \IMIA\ImiaBaseExt\Controller\Command\CacheWarmerCommandController::class;

}

\IMIA\ImiaBaseExt\Xclass\Core\Database\ReferenceIndex::addNonRelationTable('tx_imiabaseext_cacheitem');