IMIA Base for TYPO3 Extensions
======================

Features
----------------------
-   Annotation driven SQL generation
    -   TypoScript Mapping generation
-   Annotation driven TCA generation

Documentation
----------------------

### Configuration ###

Enable imia_base_ext with the creation of a settings-file in:

    typo3conf/ext/{yourExtKey}/Configuration/ImiaBaseExt/settings.yml

#### Settings.yml ####

    # Annotations
    annotations:
        # SQL Annotations
        sql: true

        # TCA Annotations
        tca: true

### Annotations ###

#### SQL ####

    use IMIA\ImiaBaseExt\Annotation\SQL;

*Class Annotations*

-   @SQL/Table
    -   name: string, default: generated from class name

*Column Annotations*

-   @SQL/Column
    -   name: string
        *name of column in database*
        -   default: generated from property name
    -   type: string
        *type in database, common sql field types*
        -   mandatory
    -   length: integer
        *length of varchar, int, float, ...*
        -   default: 255
    -   decimal: integer
        *decimals of float, decimal, double, real*
        -    default: 2
    -   unsigned: boolean
        *unsigned int?*
        -   default: false
    -   nullable: boolean
        *NOT NULL?*
        -   default: false
    -   create: boolean
        *Create or overwrite the field in the database? (or map an existing field)*
        -   default: true

#### TCA ####

    use IMIA\ImiaBaseExt\Annotation\TCA;

*Class Annotations*

-   @TCA/Table
    -   name: string, default: generated from class name

*Column Annotations*

-   @TCA/Check
-   @TCA/Flex
-   @TCA/Group
-   @TCA/Inline
-   @TCA/Input
-   @TCA/None
-   @TCA/Passthrough
-   @TCA/Radio
-   @TCA/Select
-   @TCA/Text
-   @TCA/User

-   Predefined Explicit Types
    -   @TCA/DateTime
