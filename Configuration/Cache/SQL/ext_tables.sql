### EXTENSION: imia_base_ext ###

## CLASS: IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem
# Table structure for table 'tx_imiabaseext_cacheitem'
CREATE TABLE `tx_imiabaseext_cacheitem` (
    `uid` int(11) unsigned NOT NULL auto_increment,
    `pid` int(11) unsigned DEFAULT '0' NOT NULL,
    `tstamp` int(11) unsigned DEFAULT '0' NOT NULL,
    `crdate` int(11) unsigned DEFAULT '0' NOT NULL,
    `priority` int(11) unsigned DEFAULT '0' NOT NULL,
    `params` text NOT NULL,
    `warm` tinyint(2) DEFAULT '0' NOT NULL,
    `locked` tinyint(2) DEFAULT '0' NOT NULL,
    `uri` tinytext NOT NULL,
    `root` int(11) DEFAULT '0' NOT NULL,

    PRIMARY KEY (`uid`),
    KEY `parent` (`pid`),
    KEY `warm` (`warm`),
    KEY `warm_locked` (`warm`,`locked`)
);

