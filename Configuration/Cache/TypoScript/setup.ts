### EXTENSION: imia_base_ext ###

config.tx_extbase {
    persistence {
        classes {
            IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem {
                mapping {
                    tableName = tx_imiabaseext_cacheitem
                    columns {
                        crdate.mapOnProperty = crdate
                    }
                }
            }
        }
    }

}

plugin.tx_imiabaseext {
    persistence {
        classes {
            IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem {
                mapping {
                    tableName = tx_imiabaseext_cacheitem
                    columns {
                        crdate.mapOnProperty = crdate
                    }
                }
            }
        }
    }

}

module.tx_imiabaseext {
    persistence {
        classes {
            IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem {
                mapping {
                    tableName = tx_imiabaseext_cacheitem
                    columns {
                        crdate.mapOnProperty = crdate
                    }
                }
            }
        }
    }

}

