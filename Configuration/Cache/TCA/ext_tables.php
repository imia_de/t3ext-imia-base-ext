<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}


// ******************************************************************
// TCA for IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem
// (tx_imiabaseext_cacheitem)
// ******************************************************************
$GLOBALS['TCA']['tx_imiabaseext_cacheitem'] = [
    'ctrl' => [
        'title' => 'Cache-Item',
        'label' => 'pid',
        'dividers2tabs' => true,
        'hideTable' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'hideAtCopy' => true,
        'prependAtCopy' => '',
        'iconfile' => 'EXT:imia_base_ext/Resources/Public/Images/Icons/CacheItem.png',
        'rootLevel' => 1,
        'excludeOnPageCopy' => true,
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_base_ext', 'Configuration/Cache/TCA/CacheItem.php'),
    ]
];