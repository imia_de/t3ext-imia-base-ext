<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['tx_imiabaseext_cacheitem'] = [
    'ctrl' => $TCA['tx_imiabaseext_cacheitem']['ctrl'],
    'interface' => [
        'showRecordFieldList' => '',
    ],
    'types' => [
        0 => [
            'showitem' => '',
        ],
    ],
    'palettes' => [
    ],
    'columns' => [
        'pid' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tstamp' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'priority' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'params' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'warm' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'locked' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'uri' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'root' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
   ],
];