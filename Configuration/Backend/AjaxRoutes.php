<?php
return [
    // Open the image manipulation wizard
    'wizard_image_position' => [
        'path' => '/wizard/image-position',
        'target' => \IMIA\ImiaBaseExt\User\TCA\Wizard\ImagePositionWizard::class . '::getWizardAction'
    ],
];