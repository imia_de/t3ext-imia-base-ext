<?php
$EM_CONF[$_EXTKEY] = [
    'title'            => 'IMIA Base Ext',
    'description'      => 'Base for IMIA TYPO3 Extensions',
    'category'         => 'misc',
    'author'           => 'David Frerich',
    'author_email'     => 'd.frerich@imia.de',
    'author_company'   => 'IMIA net based solutions',
    'state'            => 'stable',
    'uploadfolder'     => 0,
    'clearCacheOnLoad' => 1,
    'version'          => '8.2.0',
    'constraints'      => [
        'depends'   => [
            'php'           => '7.0.0-0.0.0',
            'typo3'         => '8.7.0-8.7.999',
            'imia_composer' => '8.0.0-0.0.0',
        ],
        'conflicts' => [
            'powermail' => '2.0.0-2.99.99',
        ],
        'suggests'  => [
        ],
    ],
];