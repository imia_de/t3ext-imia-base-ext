<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @TCA\Table(
 *  origUid = "t3_origuid",
 *  default_sortby = "ORDER BY sorting",
 *  enablecolumns = {"disabled":"hidden","starttime":"starttime","endtime":"endtime"},
 *  languageField = "sys_language_uid",
 *  transOrigPointerField = "l10n_parent",
 *  transOrigDiffSourceField = "l10n_diffsource",
 *  copyAfterDuplFields = "sys_language_uid",
 *  useColumnsForDefaultValues = "sys_language_uid",
 *  translationSource = "l10n_source",
 *  sortby = "sorting",
 *  palettes = {
 *     "hidden": "hidden",
 *     "language": "sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent",
 *     "access": "starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel",
 *  }
 * )
 */
abstract class AbstractEntity extends AbstractSimpleEntity
{
    /**
     * @SQL\Column(type="tinyint", length=4, unsigned=true, nullable=false)
     * @TCA\Check(label="LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden", items={{"LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0"}}, exclude=true, l10n_mode="")
     *
     * @var boolean
     */
    protected $hidden;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $sorting;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\InputDateTime(label="LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.starttime", l10n_mode="", exclude=true)
     *
     * @var \DateTime
     */
    protected $starttime;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\InputDateTime(label="LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.endtime", l10n_mode="", exclude=true)
     *
     * @var \DateTime
     */
    protected $endtime;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, key="language", nullable=false)
     * @TCA\Select(
     *  label = "LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.language",
     *  exclude = true,
     *  l10n_mode = "",
     *  special = "languages",
     *  items = {
     *      {"LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages", -1},
     *  },
     *  default = "0",
     * )
     *
     * @var integer
     */
    protected $sysLanguageUid = 0;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, key="language", nullable=false)
     * @TCA\Select(
     *  label="LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent",
     *  exclude=true,
     *  l10n_mode = "",
     *  displayCond="FIELD:sys_language_uid:>:0",
     *  foreign_table = "###CURRENT_TABLE###",
     *  foreign_table_where = "AND ###CURRENT_TABLE###.pid=###CURRENT_PID### AND ###CURRENT_TABLE###.sys_language_uid IN (-1,0)",
     *  items = {
     *      {"": 0}
     *  },
     *  default = "0",
     * )
     *
     * @var integer
     */
    protected $l10nParent;

    /**
     * @SQL\Column(type="mediumblob")
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $l10nDiffsource;

    /**
     * @SQL\Column(type="int", length=11)
     * @TCA\Passthrough
     *
     * @var int
     */
    protected $l10nSource;

    /**
     * @SQL\Column(type="text", nullable=true)
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $l10nState;

    /**
     * @SQL\Column(type="int", length=11, key="t3_origuid", nullable=false)
     *
     * @var integer
     */
    protected $t3Origuid;

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @return integer
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @return \DateTime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * @return \DateTime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }
}