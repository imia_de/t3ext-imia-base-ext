<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="sys_domain")
 */
abstract class Domain extends BaseEntity
{
    /**
     * @SQL\Column(name="domainName", create=false)
     *
     * @var string
     */
    protected $domainName;

    /**
     * @SQL\Column(name="redirectTo", create=false)
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * @SQL\Column(name="redirectHttpStatusCode", create=false)
     *
     * @var integer
     */
    protected $redirectHttpStatusCode;

    /**
     * @var boolean
     */
    protected $prependParams;

    /**
     * @return boolean
     */
    public function isPrependParams()
    {
        return $this->prependParams;
    }

    /**
     * @param boolean $prependParams
     * @return $this
     */
    public function setPrependParams($prependParams)
    {
        $this->prependParams = $prependParams;

        return $this;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     * @return $this
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectTo()
    {
        return $this->redirectTo;
    }

    /**
     * @param string $redirectTo
     * @return $this
     */
    public function setRedirectTo($redirectTo)
    {
        $this->redirectTo = $redirectTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getRedirectHttpStatusCode()
    {
        return $this->redirectHttpStatusCode;
    }

    /**
     * @param int $redirectHttpStatusCode
     * @return $this
     */
    public function setRedirectHttpStatusCode($redirectHttpStatusCode)
    {
        $this->redirectHttpStatusCode = $redirectHttpStatusCode;

        return $this;
    }
}