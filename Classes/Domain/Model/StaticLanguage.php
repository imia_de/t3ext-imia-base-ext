<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="static_languages")
 */
abstract class StaticLanguage extends BaseEntity
{
    /**
     * @SQL\Column(name="lg_iso_2", create=false)
     *
     * @var string
     */
    protected $iso2;

    /**
     * @SQL\Column(name="lg_name_local", create=false)
     *
     * @var string
     */
    protected $nameLocal;

    /**
     * @SQL\Column(name="lg_name_en", create=false)
     *
     * @var string
     */
    protected $nameEn;

    /**
     * @SQL\Column(name="lg_typo3", create=false)
     *
     * @var string
     */
    protected $typo3;

    /**
     * @SQL\Column(name="lg_country_iso_2", create=false)
     *
     * @var string
     */
    protected $countryIso2;

    /**
     * @SQL\Column(name="lg_collate_locale", create=false)
     *
     * @var string
     */
    protected $collateLocale;

    /**
     * @SQL\Column(name="lg_sacred", create=false)
     *
     * @var boolean
     */
    protected $sacred;

    /**
     * @SQL\Column(name="constructed", create=false)
     *
     * @var boolean
     */
    protected $constructed;

    /**
     * @SQL\Column(name="lg_name_de", create=false)
     *
     * @var string
     */
    protected $nameDe;

    /**
     * @return string
     */
    public function getIso2()
    {
        return $this->iso2;
    }

    /**
     * @return string
     */
    public function getNameLocal()
    {
        return $this->nameLocal;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * @return string
     */
    public function getTypo3()
    {
        return $this->typo3;
    }

    /**
     * @return string
     */
    public function getCountryIso2()
    {
        return $this->countryIso2;
    }

    /**
     * @return string
     */
    public function getCollateLocale()
    {
        return $this->collateLocale;
    }

    /**
     * @return boolean
     */
    public function getSacred()
    {
        return $this->sacred;
    }

    /**
     * @return boolean
     */
    public function getConstructed()
    {
        return $this->constructed;
    }

    /**
     * @return string
     */
    public function getNameDe()
    {
        return $this->nameDe;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($GLOBALS['TSFE']) {
            $langKey = $GLOBALS['TSFE']->lang;
        } else {
            $langKey = $GLOBALS['LANG']->lang;
        }

        if ($langKey == 'de' && $this->getNameDe()) {
            return $this->getNameDe();
        } else {
            return $this->getNameEn();
        }
    }
}