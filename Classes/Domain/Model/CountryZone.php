<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="static_country_zones")
 * @TCA\Table(label="zn_name_local", default_sortby="ORDER BY zn_name_local")
 */
abstract class CountryZone extends BaseEntity
{
    /**
     * @SQL\Column(name="zn_country_iso_2", create=false)
     *
     * @var string
     */
    protected $iso2;

    /**
     * @SQL\Column(name="zn_country_iso_3", create=false)
     *
     * @var string
     */
    protected $iso3;

    /**
     * @SQL\Column(name="zn_country_iso_nr", create=false)
     *
     * @var integer
     */
    protected $isoNr;

    /**
     * @SQL\Column(name="zn_code", create=false)
     *
     * @var integer
     */
    protected $code;

    /**
     * @SQL\Column(name="zn_name_local", create=false)
     *
     * @var string
     */
    protected $nameLocal;

    /**
     * @SQL\Column(name="zn_name_en", create=false)
     *
     * @var string
     */
    protected $nameEn;

    /**
     * @SQL\Column(name="zn_name_de", create=false)
     *
     * @var string
     */
    protected $nameDe;

    /**
     * @SQL\Column(name="zn_country_uid", create=false)
     * @TCA\Select(
     *  foreign_table = "static_countries",
     *  foreign_table_where = "ORDER BY static_countries.cn_short_local",
     *  size = 1,
     *  minitems = 0,
     *  maxitems = 1,
     *  default = "54"
     * )
     *
     * @var \IMIA\ImiaBaseExt\Domain\Model\Country
     */
    protected $country;

    /**
     * @return string
     */
    public function getIso2()
    {
        return $this->iso2;
    }

    /**
     * @return string
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * @return int
     */
    public function getIsoNr()
    {
        return $this->isoNr;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getNameLocal()
    {
        return $this->nameLocal;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * @return string
     */
    public function getNameDe()
    {
        return $this->nameDe;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($GLOBALS['TSFE']) {
            $langKey = $GLOBALS['TSFE']->lang;
        } else {
            $langKey = $GLOBALS['LANG']->lang;
        }

        if ($langKey == 'de' && $this->getNameDe()) {
            return $this->getNameDe();
        } else {
            return $this->getNameEn() ?: $this->getNameLocal();
        }
    }
}