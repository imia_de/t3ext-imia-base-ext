<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="pages_language_overlay")
 */
abstract class PageLanguageOverlay extends BaseEntity
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var integer
     */
    protected $doktype;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var integer
     */
    protected $urltype;

    /**
     * @var integer
     */
    protected $shortcut;

    /**
     * @var integer
     */
    protected $shortcutMode;

    /**
     * @var string
     */
    protected $subtitle;

    /**
     * @var string
     */
    protected $media;

    /**
     * @var string
     */
    protected $keywords;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $abstract;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var string
     */
    protected $authorEmail;

    /**
     * @var string
     */
    protected $navTitle;

    /**
     * @var string
     */
    protected $txRealurlPathsegment;

    /**
     * @var integer
     */
    protected $sysLanguageUid;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return null
     */
    public function getPage()
    {
        return null;
    }

    /**
     * @return int
     */
    public function getRecordUid()
    {
        return $this->uid;
    }

    /**
     * @return integer
     */
    public function getUid()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getUid();
        } else {
            return null;
        }
    }

    /**
     * @param string $title
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return integer
     */
    public function getDoktype()
    {
        return $this->doktype;
    }

    /**
     * @param integer $doktype
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setDoktype($doktype)
    {
        $this->doktype = $doktype;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSiteroot()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getIsSiteroot();
        } else {
            return null;
        }
    }

    /**
     * @return boolean
     */
    public function getPhpTreeStop()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getPhpTreeStop();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUrltype()
    {
        return $this->urltype;
    }

    /**
     * @param integer $urltype
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setUrltype($urltype)
    {
        $this->urltype = $urltype;

        return $this;
    }

    /**
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function getShortcut()
    {
        return $this->shortcut;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay $shortcut
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setShortcut($shortcut)
    {
        $this->shortcut = $shortcut;

        return $this;
    }

    /**
     * @return integer
     */
    public function getShortcutMode()
    {
        return $this->shortcutMode;
    }

    /**
     * @param integer $shortcutMode
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setShortcutMode($shortcutMode)
    {
        $this->shortcutMode = $shortcutMode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNoCache()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getNoCache();
        } else {
            return null;
        }
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getFeGroup()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getFeGroup();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * @return integer
     */
    public function getLayout()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getLayout();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getUrlScheme()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getUrlScheme();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTarget();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param string $media
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastUpdated()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getLastUpdated();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCacheTimeout()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getCacheTimeout();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getCacheTags()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getCacheTags();
        } else {
            return null;
        }
    }

    /**
     * @return DateTime
     */
    public function getNewUntil()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getNewUntil();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNoSearch()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getNoSearch();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * @param string $abstract
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getModule();
        } else {
            return null;
        }
    }

    /**
     * @return boolean
     */
    public function getExtendToSubpages()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getExtendToSubpages();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * @param string $authorEmail
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getNavTitle()
    {
        return $this->navTitle;
    }

    /**
     * @param string $navTitle
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setNavTitle($navTitle)
    {
        $this->navTitle = $navTitle;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNavHide()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getNavHide();
        } else {
            return null;
        }
    }

    /**
     * @return \IMIA\ImiaBaseExt\Domain\Model\Page
     */
    public function getContentFromPid()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getContentFromPid();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getMountPid()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getMountPid();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getMountPidOl()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getMountPidOl();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getAlias();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getL18nCfg()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getL18nCfg();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getFeLoginMode()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getFeLoginMode();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getBackendLayout()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getBackendLayout();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getBackendLayoutNextLevel()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getBackendLayoutNextLevel();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaDs()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxTemplavoilaDs();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaTo()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxTemplavoilaTo();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaFlex()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxTemplavoilaFlex();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getTxRealurlPathsegment()
    {
        return $this->txRealurlPathsegment;
    }

    /**
     * @param string $txRealurlPathsegment
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setTxRealurlPathsegment($txRealurlPathsegment)
    {
        $this->txRealurlPathsegment = $txRealurlPathsegment;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getTxRealurlPathoverride()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxRealurlPathoverride();
        } else {
            return null;
        }
    }

    /**
     * @return boolean
     */
    public function getTxRealurlExclude()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxRealurlExclude();
        } else {
            return null;
        }
    }

    /**
     * @return boolean
     */
    public function getTxRealurlNocache()
    {
        if ($this->getPage() && is_object($this->getPage())) {
            return $this->getPage()->getTxRealurlNocache();
        } else {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * @param integer $sysLanguageUid
     * @return \IMIA\ImiaBaseExt\Domain\Model\PageLanguageOverlay
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;

        return $this;
    }
}