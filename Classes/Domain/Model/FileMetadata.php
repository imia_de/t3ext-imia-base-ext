<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="sys_file_metadata")
 */
abstract class FileMetadata extends BaseEntity
{
    /**
     * @var integer
     */
    protected $file;

    /**
     * @var integer
     */
    protected $categories;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $alternative;

    /**
     * @var integer
     */
    protected $width;

    /**
     * @var integer
     */
    protected $height;

    /**
     * @var boolean
     */
    protected $visible;

    /**
     * @var integer
     */
    protected $status;

    /**
     * @var string
     */
    protected $keywords;

    /**
     * @var string
     */
    protected $caption;

    /**
     * @var string
     */
    protected $creatorTool;

    /**
     * @var string
     */
    protected $downloadName;

    /**
     * @var string
     */
    protected $creator;

    /**
     * @var string
     */
    protected $publisher;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $locationCountry;

    /**
     * @var string
     */
    protected $locationRegion;

    /**
     * @var string
     */
    protected $locationCity;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var integer
     */
    protected $ranking;

    /**
     * @var \DateTime
     */
    protected $contentCreationDate;

    /**
     * @var \DateTime
     */
    protected $contentModificationDate;

    /**
     * @var string
     */
    protected $note;

    /**
     * @var string
     */
    protected $unit;

    /**
     * @var integer
     */
    protected $duration;

    /**
     * @var string
     */
    protected $colorSpace;

    /**
     * @var integer
     */
    protected $pages;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var string
     */
    protected $feGroups;

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getAlternative()
    {
        return $this->alternative;
    }

    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;

        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight()
    {
        return $this->Height;
    }

    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getFeGroups()
    {
        return $this->feGroups;
    }

    /**
     * @param string $feGroups
     * @return $this
     */
    public function setFeGroups($feGroups)
    {
        $this->feGroups = $feGroups;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     * @return $this
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return $this
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     * @return $this
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatorTool()
    {
        return $this->creatorTool;
    }

    /**
     * @param string $creatorTool
     * @return $this
     */
    public function setCreatorTool($creatorTool)
    {
        $this->creatorTool = $creatorTool;

        return $this;
    }

    /**
     * @return string
     */
    public function getDownloadName()
    {
        return $this->downloadName;
    }

    /**
     * @param string $downloadName
     * @return $this
     */
    public function setDownloadName($downloadName)
    {
        $this->downloadName = $downloadName;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     * @return $this
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param string $publisher
     * @return $this
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCountry()
    {
        return $this->locationCountry;
    }

    /**
     * @param string $locationCountry
     * @return $this
     */
    public function setLocationCountry($locationCountry)
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationRegion()
    {
        return $this->locationRegion;
    }

    /**
     * @param string $locationRegion
     * @return $this
     */
    public function setLocationRegion($locationRegion)
    {
        $this->locationRegion = $locationRegion;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCity()
    {
        return $this->locationCity;
    }

    /**
     * @param string $locationCity
     * @return $this
     */
    public function setLocationCity($locationCity)
    {
        $this->locationCity = $locationCity;

        return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return $this
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return $this
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return int
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * @param int $ranking
     * @return $this
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getContentCreationDate()
    {
        return $this->contentCreationDate;
    }

    /**
     * @param \DateTime $contentCreationDate
     * @return $this
     */
    public function setContentCreationDate($contentCreationDate)
    {
        $this->contentCreationDate = $contentCreationDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getContentModificationDate()
    {
        return $this->contentModificationDate;
    }

    /**
     * @param \DateTime $contentModificationDate
     * @return $this
     */
    public function setContentModificationDate($contentModificationDate)
    {
        $this->contentModificationDate = $contentModificationDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return string
     */
    public function getColorSpace()
    {
        return $this->colorSpace;
    }

    /**
     * @param string $colorSpace
     * @return $this
     */
    public function setColorSpace($colorSpace)
    {
        $this->colorSpace = $colorSpace;

        return $this;
    }

    /**
     * @return int
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     * @return $this
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }
}