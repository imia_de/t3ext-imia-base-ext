<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="pages")
 */
abstract class Page extends BaseEntity
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var integer
     */
    protected $doktype;

    /**
     * @var integer
     */
    protected $isSiteroot;

    /**
     * @var integer
     */
    protected $phpTreeStop;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var integer
     */
    protected $urltype;

    /**
     * @var integer
     */
    protected $shortcut;

    /**
     * @var integer
     */
    protected $shortcutMode;

    /**
     * @var boolean
     */
    protected $noCache;

    /**
     * @var integer
     */
    protected $feGroup;

    /**
     * @var string
     */
    protected $subtitle;

    /**
     * @var integer
     */
    protected $layout;

    /**
     * @var integer
     */
    protected $urlScheme;

    /**
     * @var string
     */
    protected $target;

    /**
     * @var string
     */
    protected $media;

    /**
     * @SQL\Column(name="lastUpdated", create=false)
     *
     * @var \DateTime
     */
    protected $lastUpdated;

    /**
     * @var string
     */
    protected $keywords;

    /**
     * @var integer
     */
    protected $cacheTimeout;

    /**
     * @var string
     */
    protected $cacheTags;

    /**
     * @var \DateTime
     */
    protected $newUntil;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $noSearch;

    /**
     * @var string
     */
    protected $abstract;

    /**
     * @var string
     */
    protected $module;

    /**
     * @var boolean
     */
    protected $extendToSubpages;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var string
     */
    protected $authorEmail;

    /**
     * @var string
     */
    protected $navTitle;

    /**
     * @var boolean
     */
    protected $navHide;

    /**
     * @var integer
     */
    protected $contentFromPid;

    /**
     * @var integer
     */
    protected $mountPid;

    /**
     * @var boolean
     */
    protected $mountPidOl;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var integer
     */
    protected $l18nCfg;

    /**
     * @var integer
     */
    protected $feLoginMode;

    /**
     * @var integer
     */
    protected $backendLayout;

    /**
     * @var integer
     */
    protected $backendLayoutNextLevel;

    /**
     * @var string
     */
    protected $txTemplavoilaDs;

    /**
     * @var string
     */
    protected $txTemplavoilaTo;

    /**
     * @var string
     */
    protected $txTemplavoilaFlex;

    /**
     * @var string
     */
    protected $txRealurlPathsegment;

    /**
     * @var boolean
     */
    protected $txRealurlPathoverride;

    /**
     * @var boolean
     */
    protected $txRealurlExclude;

    /**
     * @var boolean
     */
    protected $txRealurlNocache;

    /**
     * @var string
     */
    protected $txFedPageControllerAction;

    /**
     * @param string $txFedPageControllerAction
     * @return $this
     */
    public function setTxFedPageControllerAction($txFedPageControllerAction)
    {
        $this->txFedPageControllerAction = $txFedPageControllerAction;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getRecordUid()
    {
        return $this->uid;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return integer
     */
    public function getDoktype()
    {
        return $this->doktype;
    }

    /**
     * @param integer $doktype
     * @return $this
     */
    public function setDoktype($doktype)
    {
        $this->doktype = $doktype;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSiteroot()
    {
        return $this->isSiteroot;
    }

    /**
     * @param boolean $isSiteroot
     * @return $this
     */
    public function setIsSiteroot($isSiteroot)
    {
        $this->isSiteroot = $isSiteroot;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPhpTreeStop()
    {
        return $this->phpTreeStop;
    }

    /**
     * @param boolean $phpTreeStop
     * @return $this
     */
    public function setPhpTreeStop($phpTreeStop)
    {
        $this->phpTreeStop = $phpTreeStop;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUrltype()
    {
        return $this->urltype;
    }

    /**
     * @param integer $urltype
     * @return $this
     */
    public function setUrltype($urltype)
    {
        $this->urltype = $urltype;

        return $this;
    }

    /**
     * @return $this
     */
    public function getShortcut()
    {
        return $this->shortcut;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Domain\Model\Page $shortcut
     * @return $this
     */
    public function setShortcut($shortcut)
    {
        $this->shortcut = $shortcut;

        return $this;
    }

    /**
     * @return integer
     */
    public function getShortcutMode()
    {
        return $this->shortcutMode;
    }

    /**
     * @param integer $shortcutMode
     * @return $this
     */
    public function setShortcutMode($shortcutMode)
    {
        $this->shortcutMode = $shortcutMode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNoCache()
    {
        return $this->noCache;
    }

    /**
     * @param boolean $noCache
     * @return $this
     */
    public function setNoCache($noCache)
    {
        $this->noCache = $noCache;

        return $this;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getFeGroup()
    {
        return $this->feGroup;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     * @return $this
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * @return integer
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param integer $layout
     * @return $this
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return integer
     */
    public function getUrlScheme()
    {
        return $this->urlScheme;
    }

    /**
     * @param integer $urlScheme
     * @return $this
     */
    public function setUrlScheme($urlScheme)
    {
        $this->urlScheme = $urlScheme;

        return $this;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     * @return $this
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param string $media
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     * @return $this
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return $this
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCacheTimeout()
    {
        return $this->cacheTimeout;
    }

    /**
     * @param integer $cacheTimeout
     * @return $this
     */
    public function setCacheTimeout($cacheTimeout)
    {
        $this->cacheTimeout = $cacheTimeout;

        return $this;
    }

    /**
     * @return string
     */
    public function getCacheTags()
    {
        return $this->cacheTags;
    }

    /**
     * @param string $cacheTags
     * @return $this
     */
    public function setCacheTags($cacheTags)
    {
        $this->cacheTags = $cacheTags;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getNewUntil()
    {
        return $this->newUntil;
    }

    /**
     * @param \DateTime $newUntil
     * @return $this
     */
    public function setNewUntil($newUntil)
    {
        $this->newUntil = $newUntil;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNoSearch()
    {
        return $this->noSearch;
    }

    /**
     * @param boolean $noSearch
     * @return $this
     */
    public function setNoSearch($noSearch)
    {
        $this->noSearch = $noSearch;

        return $this;
    }

    /**
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * @param string $abstract
     * @return $this
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     * @return $this
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getExtendToSubpages()
    {
        return $this->extendToSubpages;
    }

    /**
     * @param boolean $extendToSubpages
     * @return $this
     */
    public function setExtendToSubpages($extendToSubpages)
    {
        $this->extendToSubpages = $extendToSubpages;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return $this
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * @param string $authorEmail
     * @return $this
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getNavTitle()
    {
        return $this->navTitle;
    }

    /**
     * @param string $navTitle
     * @return $this
     */
    public function setNavTitle($navTitle)
    {
        $this->navTitle = $navTitle;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getNavHide()
    {
        return $this->navHide;
    }

    /**
     * @param string $navHide
     * @return $this
     */
    public function setNavHide($navHide)
    {
        $this->navHide = $navHide;

        return $this;
    }

    /**
     * @return $this
     */
    public function getContentFromPid()
    {
        return $this->contentFromPid;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Domain\Model\Page $contentFromPid
     * @return $this
     */
    public function setContentFromPid($contentFromPid)
    {
        $this->contentFromPid = $contentFromPid;

        return $this;
    }

    /**
     * @return integer
     */
    public function getMountPid()
    {
        return $this->mountPid;
    }

    /**
     * @param integer $mountPid
     * @return $this
     */
    public function setMountPid($mountPid)
    {
        $this->mountPid = $mountPid;

        return $this;
    }

    /**
     * @return integer
     */
    public function getMountPidOl()
    {
        return $this->mountPidOl;
    }

    /**
     * @param integer $mountPidOl
     * @return $this
     */
    public function setMountPidOl($mountPidOl)
    {
        $this->mountPidOl = $mountPidOl;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return integer
     */
    public function getL18nCfg()
    {
        return $this->l18nCfg;
    }

    /**
     * @param integer $l18nCfg
     * @return $this
     */
    public function setL18nCfg($l18nCfg)
    {
        $this->l18nCfg = $l18nCfg;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFeLoginMode()
    {
        return $this->feLoginMode;
    }

    /**
     * @param integer $feLoginMode
     * @return $this
     */
    public function setFeLoginMode($feLoginMode)
    {
        $this->feLoginMode = $feLoginMode;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBackendLayout()
    {
        return $this->backendLayout;
    }

    /**
     * @param integer $backendLayout
     * @return $this
     */
    public function setBackendLayout($backendLayout)
    {
        $this->backendLayout = $backendLayout;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBackendLayoutNextLevel()
    {
        return $this->backendLayoutNextLevel;
    }

    /**
     * @param integer $backendLayoutNextLevel
     * @return $this
     */
    public function setBackendLayoutNextLevel($backendLayoutNextLevel)
    {
        $this->backendLayoutNextLevel = $backendLayoutNextLevel;

        return $this;
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaDs()
    {
        return $this->txTemplavoilaDs;
    }

    /**
     * @param integer $txTemplavoilaDs
     * @return $this
     */
    public function setTxTemplavoilaDs($txTemplavoilaDs)
    {
        $this->txTemplavoilaDs = $txTemplavoilaDs;

        return $this;
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaTo()
    {
        return $this->txTemplavoilaTo;
    }

    /**
     * @param string $txTemplavoilaTo
     * @return $this
     */
    public function setTxTemplavoilaTo($txTemplavoilaTo)
    {
        $this->txTemplavoilaTo = $txTemplavoilaTo;

        return $this;
    }

    /**
     * @return string
     */
    public function getTxTemplavoilaFlex()
    {
        return $this->txTemplavoilaFlex;
    }

    /**
     * @param string $txTemplavoilaFlex
     * @return $this
     */
    public function setTxTemplavoilaFlex($txTemplavoilaFlex)
    {
        $this->txTemplavoilaFlex = $txTemplavoilaFlex;

        return $this;
    }

    /**
     * @return string
     */
    public function getTxRealurlPathsegment()
    {
        return $this->txRealurlPathsegment;
    }

    /**
     * @param string $txRealurlPathsegment
     * @return $this
     */
    public function setTxRealurlPathsegment($txRealurlPathsegment)
    {
        $this->txRealurlPathsegment = $txRealurlPathsegment;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getTxRealurlPathoverride()
    {
        return $this->txRealurlPathoverride;
    }

    /**
     * @param string $txRealurlPathoverride
     * @return $this
     */
    public function setTxRealurlPathoverride($txRealurlPathoverride)
    {
        $this->txRealurlPathoverride = $txRealurlPathoverride;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getTxRealurlExclude()
    {
        return $this->txRealurlExclude;
    }

    /**
     * @param boolean $txRealurlExclude
     * @return $this
     */
    public function setTxRealurlExclude($txRealurlExclude)
    {
        $this->txRealurlExclude = $txRealurlExclude;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getTxRealurlNocache()
    {
        return $this->txRealurlNocache;
    }

    /**
     * @param boolean $txRealurlNocache
     * @return $this
     */
    public function setTxRealurlNocache($txRealurlNocache)
    {
        $this->txRealurlNocache = $txRealurlNocache;

        return $this;
    }

    /**
     * @return integer
     */
    public function getSysLanguageUid()
    {
        return 0;
    }
}