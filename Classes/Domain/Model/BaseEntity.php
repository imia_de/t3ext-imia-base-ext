<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class BaseEntity extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var DataMap
     */
    protected $dataMap;

    /**
     * @var integer
     */
    protected $_localizedLanguageUid;

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->getDataMap()->getTableName();
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->getDataMap()->getClassName();
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return strtolower(substr(get_class($this),
            strrpos(get_class($this), '\\') ? strrpos(get_class($this), '\\') + 1 : 0)
        );
    }

    /**
     * @param bool $lazy
     * @return array
     */
    public function getRow($lazy = true)
    {
        $row = [];
        foreach ($this->_getProperties() as $propertyName => $propertyValue) {
            if (in_array($propertyName, ['uid', 'pid'])) {
                $columnName = $propertyName;
            } else {
                $columnMap = $this->getDataMap()->getColumnMap($propertyName);
                if ($columnMap) {
                    $columnName = $columnMap->getColumnName();
                } else {
                    $columnName = null;
                }
            }

            if ($columnName) {
                if (is_object($propertyValue)) {
                    if ($propertyValue instanceof \DateTime) {
                        $row[$columnName] = $propertyValue->getTimestamp();
                    } elseif ($propertyValue instanceof ObjectStorage) {
                        if (!($propertyValue instanceof LazyObjectStorage) || $lazy) {
                            $values = [];
                            foreach ($propertyValue as $value) {
                                if (is_object($value) && method_exists($value, 'getUid')) {
                                    $values[] = $value->getUid();
                                }
                            }
                            if (count($values) > 0) {
                                $row[$columnName] = implode(',', $values);
                            } else {
                                $row[$columnName] = count($propertyValue);
                            }
                        }
                    } elseif (method_exists($propertyValue, 'getUid')) {
                        $row[$columnName] = $propertyValue->getUid();
                    }
                } else {
                    $row[$columnName] = $propertyValue;
                }
            }
        }

        if ($this instanceof PageLanguageOverlay && method_exists($this, 'getPage') && $this->getPage() instanceof BaseEntity) {
            $rowParent = $this->getPage()->getRow();
            $uid = $rowParent['uid'];
            $pid = $rowParent['pid'];

            ArrayUtility::mergeRecursiveWithOverrule($rowParent, $row);
            $row = $rowParent;
            $row['_LOCALIZED_UID'] = $row['uid'];
            $row['uid'] = $uid;
            $row['pid'] = $pid;
        }

        return $row;
    }

    /**
     * @return int
     */
    public function getLocalizedUid()
    {
        return $this->_localizedUid;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        if ($this->_localizedLanguageUid) {
            return $this->_localizedLanguageUid;
        } else {
            return $this->_languageUid;
        }
    }

    /**
     * @return DataMap
     */
    protected function getDataMap()
    {
        if (!$this->dataMap) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            /** @var DataMapper $dataMapper */
            $dataMapper = $objectManager->get(DataMapper::class);
            $this->dataMap = $dataMapper->getDataMap(get_class($this));
        }

        return $this->dataMap;
    }
}