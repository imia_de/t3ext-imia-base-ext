<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="static_countries")
 * @TCA\Table(label="cn_short_local", default_sortby="ORDER BY cn_short_local")
 */
abstract class Country extends BaseEntity
{
    /**
     * @SQL\Column(name="cn_iso_2", create=false)
     *
     * @var string
     */
    protected $iso2;

    /**
     * @SQL\Column(name="cn_iso_3", create=false)
     *
     * @var string
     */
    protected $iso3;

    /**
     * @SQL\Column(name="cn_iso_nr", create=false)
     *
     * @var integer
     */
    protected $isoNr;

    /**
     * @SQL\Column(name="cn_parent_tr_iso_nr", create=false)
     *
     * @var integer
     */
    protected $parentTrIsoNr;

    /**
     * @SQL\Column(name="cn_official_name_local", create=false)
     *
     * @var string
     */
    protected $officialNameLocal;

    /**
     * @SQL\Column(name="cn_official_name_en", create=false)
     *
     * @var string
     */
    protected $officialNameEn;

    /**
     * @SQL\Column(name="cn_capital", create=false)
     *
     * @var string
     */
    protected $capital;

    /**
     * @SQL\Column(name="cn_tldomain", create=false)
     *
     * @var string
     */
    protected $tldomain;

    /**
     * @SQL\Column(name="cn_currency_iso_3", create=false)
     *
     * @var string
     */
    protected $currencyIso3;

    /**
     * @SQL\Column(name="cn_currency_iso_nr", create=false)
     *
     * @var integer
     */
    protected $currencyIsoNr;

    /**
     * @SQL\Column(name="phone", create=false)
     *
     * @var integer
     */
    protected $phone;

    /**
     * @SQL\Column(name="eu_member", create=false)
     *
     * @var boolean
     */
    protected $euMember;

    /**
     * @SQL\Column(name="cn_address_format", create=false)
     *
     * @var integer
     */
    protected $addressFormat;

    /**
     * @SQL\Column(name="cn_zone_flag", create=false)
     *
     * @var boolean
     */
    protected $zoneFlag;

    /**
     * @SQL\Column(name="cn_short_local", create=false)
     *
     * @var string
     */
    protected $shortLocal;

    /**
     * @SQL\Column(name="cn_short_en", create=false)
     *
     * @var string
     */
    protected $shortEn;

    /**
     * @SQL\Column(name="cn_short_de", create=false)
     *
     * @var string
     */
    protected $shortDe;

    /**
     * @SQL\Column(name="cn_short_fr", create=false)
     *
     * @var string
     */
    protected $shortFr;

    /**
     * @SQL\Column(name="cn_short_it", create=false)
     *
     * @var string
     */
    protected $shortIt;

    /**
     * @SQL\Column(name="cn_short_cz", create=false)
     *
     * @var string
     */
    protected $shortCz;

    /**
     * @SQL\Column(name="cn_short_pl", create=false)
     *
     * @var string
     */
    protected $shortPl;

    /**
     * @SQL\Column(name="cn_short_pt", create=false)
     *
     * @var string
     */
    protected $shortPt;

    /**
     * @SQL\Column(name="cn_short_ro", create=false)
     *
     * @var string
     */
    protected $shortRo;

    /**
     * @SQL\Column(name="cn_short_zh", create=false)
     *
     * @var string
     */
    protected $shortZh;

    /**
     * @SQL\Column(name="cn_short_es", create=false)
     *
     * @var string
     */
    protected $shortEs;

    /**
     * @SQL\Column(name="cn_short_da", create=false)
     *
     * @var string
     */
    protected $shortDa;

    /**
     * @SQL\Column(name="cn_uno_member", create=false)
     *
     * @var boolean
     */
    protected $unoMember;

    /**
     * @return string
     */
    public function getIso2()
    {
        return $this->iso2;
    }

    /**
     * @return string
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * @return integer
     */
    public function getIsoNr()
    {
        return $this->isoNr;
    }

    /**
     * @return integer
     */
    public function getParentTrIsoNr()
    {
        return $this->parentTrIsoNr;
    }

    /**
     * @return string
     */
    public function getOfficialNameLocal()
    {
        return $this->officialNameLocal;
    }

    /**
     * @return string
     */
    public function getOfficialNameEn()
    {
        return $this->officialNameEn;
    }

    /**
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * @return string
     */
    public function getTldomain()
    {
        return $this->tldomain;
    }

    /**
     * @return string
     */
    public function getCurrencyIso3()
    {
        return $this->currencyIso3;
    }

    /**
     * @return integer
     */
    public function getCurrencyIsoNr()
    {
        return $this->currencyIsoNr;
    }

    /**
     * @return integer
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return boolean
     */
    public function getEuMember()
    {
        return $this->euMember;
    }

    /**
     * @return integer
     */
    public function getAddressFormat()
    {
        return $this->addressFormat;
    }

    /**
     * @return boolean
     */
    public function getZoneFlag()
    {
        return $this->zoneFlag;
    }

    /**
     * @return string
     */
    public function getShortLocal()
    {
        return $this->shortLocal;
    }

    /**
     * @return string
     */
    public function getShortEn()
    {
        return $this->shortEn;
    }

    /**
     * @return string
     */
    public function getShortDe()
    {
        return $this->shortDe;
    }

    /**
     * @return string
     */
    public function getShortFr()
    {
        return $this->shortFr;
    }

    /**
     * @return string
     */
    public function getShortIt()
    {
        return $this->shortIt;
    }

    /**
     * @return string
     */
    public function getShortCz()
    {
        return $this->shortCz;
    }

    /**
     * @return string
     */
    public function getShortPl()
    {
        return $this->shortPl;
    }

    /**
     * @return string
     */
    public function getShortPt()
    {
        return $this->shortPt;
    }

    /**
     * @return string
     */
    public function getShortRo()
    {
        return $this->shortRo;
    }

    /**
     * @return string
     */
    public function getShortZh()
    {
        return $this->shortZh;
    }

    /**
     * @return string
     */
    public function getShortEs()
    {
        return $this->shortEs;
    }

    /**
     * @return string
     */
    public function getShortDa()
    {
        return $this->shortDa;
    }

    /**
     * @return boolean
     */
    public function getUnoMember()
    {
        return $this->unoMember;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return (string)$this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = '';
        $langKey = null;
        if ($GLOBALS['TSFE']) {
            $langKey = $GLOBALS['TSFE']->lang;
        }
        if (!$langKey && $GLOBALS['LANG']) {
            $langKey = $GLOBALS['LANG']->lang;
        }

        if ($langKey) {
            $getter = 'getShort' . ucfirst($langKey);

            if (method_exists($this, $getter)) {
                $string = $this->$getter();
            }
        }

        if (!$string) {
            $string = $this->getShortEn();
        }

        return $string;
    }
}