<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @TCA\Table(
 *  tstamp = "tstamp",
 *  crdate = "crdate",
 *  delete = "deleted",
 *  cruser_id = "cruser_id"
 * )
 */
abstract class AbstractSimpleEntity extends BaseEntity
{
    /**
     * @SQL\Column(type="int", length=11, autoIncrement=true, primaryKey=true, unsigned=true)
     *
     * @var integer
     */
    protected $uid;

    /**
     * @SQL\Column(type="int", length=11, key="parent", unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $cruserId;

    /**
     * @SQL\Column(type="tinyint", length=3, unsigned=true, nullable=false)
     *
     * @var boolean
     */
    protected $deleted;

    /**
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}