<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model\CacheWarmer;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="tx_imiabaseext_cacheitem")
 * @TCA\Table(
 *  label = "pid",
 *  title = "Cache-Item",
 *  hideTable = true,
 *  tstamp = "tstamp",
 *  crdate = "crdate",
 *  rootLevel = 1,
 *  excludeOnPageCopy = true,
 * )
 * @SQL\Index(
 *  keys = {
 *      "warm": {"warm"},
 *      "warm_locked": {"warm", "locked"},
 *  }
 * )
 */
class CacheItem extends BaseEntity
{
    /**
     * @SQL\Column(type="int", length=11, autoIncrement=true, primaryKey=true, unsigned=true)
     *
     * @var integer
     */
    protected $uid;

    /**
     * @SQL\Column(type="int", length=11, key="parent", unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @SQL\Column(type="int", length=11, unsigned=true, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $priority;

    /**
     * @SQL\Column(type="text")
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $params;

    /**
     * @SQL\Column(type="tinyint", length=2, nullable=false)
     * @TCA\Passthrough
     *
     * @var boolean
     */
    protected $warm;

    /**
     * @SQL\Column(type="tinyint", length=2, nullable=false)
     * @TCA\Passthrough
     *
     * @var boolean
     */
    protected $locked;

    /**
     * @SQL\Column(type="tinytext", nullable=false)
     * @TCA\Passthrough
     *
     * @var string
     */
    protected $uri;

    /**
     * @SQL\Column(type="int", length=11, nullable=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $root;

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return int
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param int $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return json_decode($this->params, true) ?: [];
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setParams(array $params)
    {
        $this->params = json_encode($params);

        return $this;
    }

    /**
     * @return array
     */
    public function getSecondaryParams()
    {
        $params = $this->getParams();
        if (array_key_exists('L', $params)) {
            unset($params['L']);
        }
        if (array_key_exists('MP', $params)) {
            unset($params['MP']);
        }

        return $params;
    }

    /**
     * @return bool
     */
    public function getWarm()
    {
        return $this->warm;
    }

    /**
     * @param bool $warm
     * @return $this
     */
    public function setWarm($warm)
    {
        $this->warm = $warm;

        return $this;
    }

    /**
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param int $root
     * @return $this
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }
}