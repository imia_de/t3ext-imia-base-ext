<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="tt_content")
 */
abstract class Content extends BaseEntity
{
    /**
     * @SQL\Column(name="CType", create=false)
     *
     * @var string
     */
    protected $cType;

    /**
     * @var integer
     */
    protected $feGroup;

    /**
     * @var integer
     */
    protected $layout;

    /**
     * @SQL\Column(name="colPos", create=false)
     *
     * @var integer
     */
    protected $colPos;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $header;

    /**
     * @var string
     */
    protected $headerPosition;

    /**
     * @var string
     */
    protected $headerLink;

    /**
     * @var integer
     */
    protected $headerLayout;

    /**
     * @var string
     */
    protected $subheader;

    /**
     * @var string
     */
    protected $bodytext;

    /**
     * @var string
     */
    protected $textAlign;

    /**
     * @var integet
     */
    protected $textFace;

    /**
     * @var integer
     */
    protected $textSize;

    /**
     * @var integer
     */
    protected $textColor;

    /**
     * @var string
     */
    protected $textProperties;

    /**
     * @var integer
     */
    protected $image;

    /**
     * @var integer
     */
    protected $imagewidth;

    /**
     * @var integer
     */
    protected $imageheight;

    /**
     * @var integer
     */
    protected $imageorient;

    /**
     * @var boolean
     */
    protected $imageborder;

    /**
     * @SQL\Column(name="image_noRows", create=false)
     *
     * @var boolean
     */
    protected $imageNoRows;

    /**
     * @var string
     */
    protected $imageLink;

    /**
     * @var boolean
     */
    protected $imageZoom;

    /**
     * @var integer
     */
    protected $imageEffects;

    /**
     * @var integer
     */
    protected $imageFrames;

    /**
     * @var integer
     */
    protected $imageCompression;

    /**
     * @var integer
     */
    protected $imagecols;

    /**
     * @var string
     */
    protected $imagecaption;

    /**
     * @var string
     */
    protected $imagecaptionPosition;

    /**
     * @SQL\Column(name="altText", create=false)
     *
     * @var string
     */
    protected $altText;

    /**
     * @SQL\Column(name="titleText", create=false)
     *
     * @var string
     */
    protected $titleText;

    /**
     * @SQL\Column(name="longdescURL", create=false)
     *
     * @var string
     */
    protected $longdescURL;

    /**
     * @var integer
     */
    protected $cols;

    /**
     * @var integer
     */
    protected $pages;

    /**
     * @var integer
     */
    protected $recursive;

    /**
     * @var integer
     */
    protected $menuType;

    /**
     * @var string
     */
    protected $listType;

    /**
     * @var string
     */
    protected $selectKey;

    /**
     * @SQL\Column(name="table_bgColor", create=false)
     *
     * @var integer
     */
    protected $tableBgColor;

    /**
     * @var integer
     */
    protected $tableBorder;

    /**
     * @var integer
     */
    protected $tableCellspacing;

    /**
     * @var integer
     */
    protected $tableCellpadding;

    /**
     * @var integer
     */
    protected $media;

    /**
     * @var integer
     */
    protected $fileCollections;

    /**
     * @var string
     */
    protected $multimedia;

    /**
     * @var boolean
     */
    protected $filelinkSize;

    /**
     * @var string
     */
    protected $filelinkSorting;

    /**
     * @var string
     */
    protected $target;

    /**
     * @var integer
     */
    protected $records;

    /**
     * @SQL\Column(name="spaceBefore", create=false)
     *
     * @var integer
     */
    protected $spaceBefore;

    /**
     * @SQL\Column(name="spaceAfter", create=false)
     *
     * @var integer
     */
    protected $spaceAfter;

    /**
     * @var integer
     */
    protected $sectionFrame;

    /**
     * @SQL\Column(name="sectionIndex", create=false)
     *
     * @var boolean
     */
    protected $sectionIndex;

    /**
     * @SQL\Column(name="linkToTop", create=false)
     *
     * @var boolean
     */
    protected $linkToTop;

    /**
     * @var boolean
     */
    protected $rteEnabled;

    /**
     * @var string
     */
    protected $piFlexform;

    /**
     * @var string
     */
    protected $accessibilityTitle;

    /**
     * @var boolean
     */
    protected $accessibilityBypass;

    /**
     * @var string
     */
    protected $accessibilityBypassText;

    public function getCType()
    {
        return $this->cType;
    }

    public function setCType($cType)
    {
        $this->cType = $cType;

        return $this;
    }

    public function getFeGroup()
    {
        return $this->feGroup;
    }

    public function setFeGroup($feGroup)
    {
        $this->feGroup = $feGroup;

        return $this;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    public function getColPos()
    {
        return $this->colPos;
    }

    public function setColPos($colPos)
    {
        $this->colPos = $colPos;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    public function getHeaderPosition()
    {
        return $this->headerPosition;
    }

    public function setHeaderPosition($headerPosition)
    {
        $this->headerPosition = $headerPosition;

        return $this;
    }

    public function getHeaderLink()
    {
        return $this->headerLink;
    }

    public function setHeaderLink($headerLink)
    {
        $this->headerLink = $headerLink;

        return $this;
    }

    public function getHeaderLayout()
    {
        return $this->headerLayout;
    }

    public function setHeaderLayout($headerLayout)
    {
        $this->headerLayout = $headerLayout;

        return $this;
    }

    public function getSubheader()
    {
        return $this->subheader;
    }

    public function setSubheader($subheader)
    {
        $this->subheader = $subheader;

        return $this;
    }

    public function getBodytext()
    {
        return $this->bodytext;
    }

    public function setBodytext($bodytext)
    {
        $this->bodytext = $bodytext;

        return $this;
    }

    public function getTextAlign()
    {
        return $this->textAlign;
    }

    public function setTextAlign($textAlign)
    {
        $this->textAlign = $textAlign;

        return $this;
    }

    public function getTextFace()
    {
        return $this->textFace;
    }

    public function setTextFace(integet $textFace)
    {
        $this->textFace = $textFace;

        return $this;
    }

    public function getTextSize()
    {
        return $this->textSize;
    }

    public function setTextSize($textSize)
    {
        $this->textSize = $textSize;

        return $this;
    }

    public function getTextColor()
    {
        return $this->textColor;
    }

    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;

        return $this;
    }

    public function getTextProperties()
    {
        return $this->textProperties;
    }

    public function setTextProperties($textProperties)
    {
        $this->textProperties = $textProperties;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImagewidth()
    {
        return $this->imagewidth;
    }

    public function setImagewidth($imagewidth)
    {
        $this->imagewidth = $imagewidth;

        return $this;
    }

    public function getImageheight()
    {
        return $this->imageheight;
    }

    public function setImageheight($imageheight)
    {
        $this->imageheight = $imageheight;

        return $this;
    }

    public function getImageorient()
    {
        return $this->imageorient;
    }

    public function setImageorient($imageorient)
    {
        $this->imageorient = $imageorient;

        return $this;
    }

    public function getImageborder()
    {
        return $this->imageborder;
    }

    public function setImageborder($imageborder)
    {
        $this->imageborder = $imageborder;

        return $this;
    }

    public function getImageNoRows()
    {
        return $this->imageNoRows;
    }

    public function setImageNoRows($imageNoRows)
    {
        $this->imageNoRows = $imageNoRows;

        return $this;
    }

    public function getImageLink()
    {
        return $this->imageLink;
    }

    public function setImageLink($imageLink)
    {
        $this->imageLink = $imageLink;

        return $this;
    }

    public function getImageZoom()
    {
        return $this->imageZoom;
    }

    public function setImageZoom($imageZoom)
    {
        $this->imageZoom = $imageZoom;

        return $this;
    }

    public function getImageEffects()
    {
        return $this->imageEffects;
    }

    public function setImageEffects($imageEffects)
    {
        $this->imageEffects = $imageEffects;

        return $this;
    }

    public function getImageFrames()
    {
        return $this->imageFrames;
    }

    public function setImageFrames($imageFrames)
    {
        $this->imageFrames = $imageFrames;

        return $this;
    }

    public function getImageCompression()
    {
        return $this->imageCompression;
    }

    public function setImageCompression($imageCompression)
    {
        $this->imageCompression = $imageCompression;

        return $this;
    }

    public function getImagecols()
    {
        return $this->imagecols;
    }

    public function setImagecols($imagecols)
    {
        $this->imagecols = $imagecols;

        return $this;
    }

    public function getImagecaption()
    {
        return $this->imagecaption;
    }

    public function setImagecaption($imagecaption)
    {
        $this->imagecaption = $imagecaption;

        return $this;
    }

    public function getImagecaptionPosition()
    {
        return $this->imagecaptionPosition;
    }

    public function setImagecaptionPosition($imagecaptionPosition)
    {
        $this->imagecaptionPosition = $imagecaptionPosition;

        return $this;
    }

    public function getAltText()
    {
        return $this->altText;
    }

    public function setAltText($altText)
    {
        $this->altText = $altText;

        return $this;
    }

    public function getTitleText()
    {
        return $this->titleText;
    }

    public function setTitleText($titleText)
    {
        $this->titleText = $titleText;

        return $this;
    }

    public function getLongdescURL()
    {
        return $this->longdescURL;
    }

    public function setLongdescURL($longdescURL)
    {
        $this->longdescURL = $longdescURL;

        return $this;
    }

    public function getCols()
    {
        return $this->cols;
    }

    public function setCols($cols)
    {
        $this->cols = $cols;

        return $this;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    public function getRecursive()
    {
        return $this->recursive;
    }

    public function setRecursive($recursive)
    {
        $this->recursive = $recursive;

        return $this;
    }

    public function getMenuType()
    {
        return $this->menuType;
    }

    public function setMenuType($menuType)
    {
        $this->menuType = $menuType;

        return $this;
    }

    public function getListType()
    {
        return $this->listType;
    }

    public function setListType($listType)
    {
        $this->listType = $listType;

        return $this;
    }

    public function getSelectKey()
    {
        return $this->selectKey;
    }

    public function setSelectKey($selectKey)
    {
        $this->selectKey = $selectKey;

        return $this;
    }

    public function getTableBgColor()
    {
        return $this->tableBgColor;
    }

    public function setTableBgColor($tableBgColor)
    {
        $this->tableBgColor = $tableBgColor;

        return $this;
    }

    public function getTableBorder()
    {
        return $this->tableBorder;
    }

    public function setTableBorder($tableBorder)
    {
        $this->tableBorder = $tableBorder;

        return $this;
    }

    public function getTableCellspacing()
    {
        return $this->tableCellspacing;
    }

    public function setTableCellspacing($tableCellspacing)
    {
        $this->tableCellspacing = $tableCellspacing;

        return $this;
    }

    public function getTableCellpadding()
    {
        return $this->tableCellpadding;
    }

    public function setTableCellpadding($tableCellpadding)
    {
        $this->tableCellpadding = $tableCellpadding;

        return $this;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    public function getFileCollections()
    {
        return $this->fileCollections;
    }

    public function setFileCollections($fileCollections)
    {
        $this->fileCollections = $fileCollections;

        return $this;
    }

    public function getMultimedia()
    {
        return $this->multimedia;
    }

    public function setMultimedia($multimedia)
    {
        $this->multimedia = $multimedia;

        return $this;
    }

    public function getFilelinkSize()
    {
        return $this->filelinkSize;
    }

    public function setFilelinkSize($filelinkSize)
    {
        $this->filelinkSize = $filelinkSize;

        return $this;
    }

    public function getFilelinkSorting()
    {
        return $this->filelinkSorting;
    }

    public function setFilelinkSorting($filelinkSorting)
    {
        $this->filelinkSorting = $filelinkSorting;

        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    public function getRecords()
    {
        return $this->records;
    }

    public function setRecords($records)
    {
        $this->records = $records;

        return $this;
    }

    public function getSpaceBefore()
    {
        return $this->spaceBefore;
    }

    public function setSpaceBefore($spaceBefore)
    {
        $this->spaceBefore = $spaceBefore;

        return $this;
    }

    public function getSpaceAfter()
    {
        return $this->spaceAfter;
    }

    public function setSpaceAfter($spaceAfter)
    {
        $this->spaceAfter = $spaceAfter;

        return $this;
    }

    public function getSectionFrame()
    {
        return $this->sectionFrame;
    }

    public function setSectionFrame($sectionFrame)
    {
        $this->sectionFrame = $sectionFrame;

        return $this;
    }

    public function getSectionIndex()
    {
        return $this->sectionIndex;
    }

    public function setSectionIndex($sectionIndex)
    {
        $this->sectionIndex = $sectionIndex;

        return $this;
    }

    public function getLinkToTop()
    {
        return $this->linkToTop;
    }

    public function setLinkToTop($linkToTop)
    {
        $this->linkToTop = $linkToTop;

        return $this;
    }

    public function getRteEnabled()
    {
        return $this->rteEnabled;
    }

    public function setRteEnabled($rteEnabled)
    {
        $this->rteEnabled = $rteEnabled;

        return $this;
    }

    public function getPiFlexform()
    {
        return $this->piFlexform;
    }

    public function setPiFlexform($piFlexform)
    {
        $this->piFlexform = $piFlexform;

        return $this;
    }

    public function getAccessibilityTitle()
    {
        return $this->accessibilityTitle;
    }

    public function setAccessibilityTitle($accessibilityTitle)
    {
        $this->accessibilityTitle = $accessibilityTitle;

        return $this;
    }

    public function getAccessibilityBypass()
    {
        return $this->accessibilityBypass;
    }

    public function setAccessibilityBypass($accessibilityBypass)
    {
        $this->accessibilityBypass = $accessibilityBypass;

        return $this;
    }

    public function getAccessibilityBypassText()
    {
        return $this->accessibilityBypassText;
    }

    public function setAccessibilityBypassText($accessibilityBypassText)
    {
        $this->accessibilityBypassText = $accessibilityBypassText;

        return $this;
    }
}