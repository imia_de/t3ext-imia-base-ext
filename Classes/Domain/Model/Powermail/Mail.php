<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model\Powermail;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="tx_powermail_domain_model_mail")
 */
abstract class Mail extends BaseEntity
{
    /**
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderMail;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $recieverMail;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var integer
     */
    protected $feuser;

    /**
     * @var string
     */
    protected $senderIp;

    /**
     * @var string
     */
    protected $userAgent;

    /**
     * @var integer
     */
    protected $time;

    /**
     * @var integer
     */
    protected $form;

    /**
     * @var integer
     */
    protected $answers;

    /**
     * @var string
     */
    protected $spamFactor;

    /**
     * @var string
     */
    protected $marketingSearchterm;

    /**
     * @var string
     */
    protected $marketingReferer;

    /**
     * @var string
     */
    protected $marketingPayedSearchResult;

    /**
     * @var string
     */
    protected $marketingLanguage;

    /**
     * @var string
     */
    protected $marketingBrowserLanguage;

    /**
     * @var string
     */
    protected $marketingFunnel;

    /**
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     * @return $this
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSenderMail()
    {
        return $this->senderMail;
    }

    /**
     * @param string $senderMail
     * @return $this
     */
    public function setSenderMail($senderMail)
    {
        $this->senderMail = $senderMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecieverMail()
    {
        return $this->recieverMail;
    }

    /**
     * @param string $recieverMail
     * @return $this
     */
    public function setRecieverMail($recieverMail)
    {
        $this->recieverMail = $recieverMail;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFeuser()
    {
        return $this->feuser;
    }

    /**
     * @param integer $feuser
     * @return $this
     */
    public function setFeuser($feuser)
    {
        $this->feuser = $feuser;

        return $this;
    }

    /**
     * @return integer
     */
    public function getSenderIp()
    {
        return $this->senderIp;
    }

    /**
     * @param integer $senderIp
     * @return $this
     */
    public function setSenderIp($senderIp)
    {
        $this->senderIp = $senderIp;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     * @return $this
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param integer $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return integer
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param integer $form
     * @return $this
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * @return integer
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param integer $answers
     * @return $this
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpamFactor()
    {
        return $this->spamFactor;
    }

    /**
     * @param string $spamFactor
     * @return $this
     */
    public function setSpamFactor($spamFactor)
    {
        $this->spamFactor = $spamFactor;

        return $this;
    }

    /**
     * @return string $marketingSearchterm
     */
    public function getMarketingSearchterm()
    {
        return $this->marketingSearchterm;
    }

    /**
     * @param string $marketingSearchterm
     * @return $this
     */
    public function setMarketingSearchterm($marketingSearchterm)
    {
        $this->marketingSearchterm = $marketingSearchterm;

        return $this;
    }

    /**
     * @return string $marketingReferer
     */
    public function getMarketingReferer()
    {
        return $this->marketingReferer;
    }

    /**
     * @param string $marketingReferer
     * @return $this
     */
    public function setMarketingReferer($marketingReferer)
    {
        $this->marketingReferer = $marketingReferer;

        return $this;
    }

    /**
     * @return string $marketingPayedSearchResult
     */
    public function getMarketingPayedSearchResult()
    {
        return $this->marketingPayedSearchResult;
    }

    /**
     * @param string $marketingPayedSearchResult
     * @return $this
     */
    public function setMarketingPayedSearchResult($marketingPayedSearchResult)
    {
        $this->marketingPayedSearchResult = $marketingPayedSearchResult;

        return $this;
    }

    /**
     * @return string $marketingLanguage
     */
    public function getMarketingLanguage()
    {
        return $this->marketingLanguage;
    }

    /**
     * @param string $marketingLanguage
     * @return $this
     */
    public function setMarketingLanguage($marketingLanguage)
    {
        $this->marketingLanguage = $marketingLanguage;

        return $this;
    }

    /**
     * @return string $marketingBrowserLanguage
     */
    public function getMarketingBrowserLanguage()
    {
        return $this->marketingBrowserLanguage;
    }

    /**
     * @param string $marketingBrowserLanguage
     * @return $this
     */
    public function setMarketingBrowserLanguage($marketingBrowserLanguage)
    {
        $this->marketingBrowserLanguage = $marketingBrowserLanguage;

        return $this;
    }

    /**
     * @return string $marketingFunnel
     */
    public function getMarketingFunnel()
    {
        return unserialize($this->marketingFunnel);
    }

    /**
     * @param string $marketingFunnel
     * @return $this
     */
    public function setMarketingFunnel($marketingFunnel)
    {
        $this->marketingFunnel = serialize($marketingFunnel);

        return $this;
    }
}