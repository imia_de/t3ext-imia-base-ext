<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model\Powermail;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="tx_powermail_domain_model_field")
 */
abstract class Field extends BaseEntity
{
    /**
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @var integer
     */
    protected $pages;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $settings;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var integer
     */
    protected $contentElement;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $prefillValue;

    /**
     * @var string
     */
    protected $placeholder;

    /**
     * @var string
     */
    protected $createFromTyposcript;

    /**
     * @var integer
     */
    protected $validation;

    /**
     * @var string
     */
    protected $validationConfiguration;

    /**
     * @var string
     */
    protected $css;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $multiselect;

    /**
     * @var string
     */
    protected $datepickerSettings;

    /**
     * @var string
     */
    protected $feuserValue;

    /**
     * @var boolean
     */
    protected $senderEmail;

    /**
     * @var boolean
     */
    protected $senderName;

    /**
     * @var boolean
     */
    protected $mandatory;

    /**
     * @var string
     */
    protected $marker;

    /**
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return int
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getContentElement()
    {
        return $this->contentElement;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getPrefillValue()
    {
        return $this->prefillValue;
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * @return string
     */
    public function getCreateFromTyposcript()
    {
        return $this->createFromTyposcript;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @return string
     */
    public function getValidationConfiguration()
    {
        return $this->validationConfiguration;
    }

    /**
     * @return string
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return boolean
     */
    public function isMultiselect()
    {
        return $this->multiselect;
    }

    /**
     * @return string
     */
    public function getDatepickerSettings()
    {
        return $this->datepickerSettings;
    }

    /**
     * @return string
     */
    public function getFeuserValue()
    {
        return $this->feuserValue;
    }

    /**
     * @return boolean
     */
    public function isSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @return boolean
     */
    public function isSenderName()
    {
        return $this->senderName;
    }

    /**
     * @return boolean
     */
    public function isMandatory()
    {
        return $this->mandatory;
    }

    /**
     * @return string
     */
    public function getMarker()
    {
        return $this->marker;
    }
}