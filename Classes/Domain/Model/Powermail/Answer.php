<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model\Powermail;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="tx_powermail_domain_model_answer")
 */
abstract class Answer extends BaseEntity
{
    /**
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $pid;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var integer
     */
    protected $valueType;

    /**
     * @var integer
     */
    protected $field;

    /**
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * @return int
     */
    public function getField()
    {
        return $this->field;
    }
}