<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="fe_users")
 */
abstract class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * @var string
     */
    protected $plainPassword;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usergroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @param string $password
     * @return \IMIA\ImiaBaseExt\Domain\Model\FrontendUser
     */
    public function setPassword($password, $plain = true)
    {
        $this->plainPassword = $password;

        if ($plain && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            $saltedpasswordsInstance = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance();
            $encryptedPassword = $saltedpasswordsInstance->getHashedPassword($password);
            parent::setPassword($encryptedPassword);
        } else {
            parent::setPassword($password);
        }

        return $this;
    }

    /**
     * @param string $plainPassword
     * @param boolean $updatePassword
     * @return \IMIA\ImiaBaseExt\Domain\Model\FrontendUser
     */
    public function setPlainPassword($plainPassword, $updatePassword = false)
    {
        $this->plainPassword = $plainPassword;

        if ($updatePassword) {
            $this->setPassword($plainPassword);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
}