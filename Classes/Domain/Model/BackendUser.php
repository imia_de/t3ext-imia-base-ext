<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Model;

use IMIA\ImiaBaseExt\Annotation\SQL;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @SQL\Table(name="be_users")
 */
abstract class BackendUser extends \TYPO3\CMS\Extbase\Domain\Model\BackendUser
{
    /**
     * @SQL\Column(name="username", create=false)
     *
     * @var string
     */
    protected $userName = '';

    /**
     * @SQL\Column(name="admin", create=false)
     *
     * @var boolean
     */
    protected $isAdministrator = false;

    /**
     * @SQL\Column(name="disable", create=false)
     *
     * @var boolean
     */
    protected $isDisabled = false;

    /**
     * @SQL\Column(name="starttime", create=false)
     *
     * @var \DateTime|null
     */
    protected $startDateAndTime = null;

    /**
     * @SQL\Column(name="endtime", create=false)
     *
     * @var \DateTime|null
     */
    protected $endDateAndTime = null;

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @SQL\Column(name="realName", create=false)
     *
     * @var string
     */
    protected $realName = '';

    /**
     * @SQL\Column(name="lastlogin", create=false)
     *
     * @var \DateTime|null
     */
    protected $lastLoginDateAndTime;

    /**
     * @SQL\Column(name="disableIPlock", create=false)
     *
     * @var boolean
     */
    protected $ipLockIsDisabled = false;
}