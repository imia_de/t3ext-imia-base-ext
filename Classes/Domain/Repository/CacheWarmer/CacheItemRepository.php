<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Repository\CacheWarmer;

use IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem;
use IMIA\ImiaBaseExt\Domain\Repository\NoStorageRepository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Repository
 * @author      David Frerich <d.frerich@imia.de>
 */
class CacheItemRepository extends NoStorageRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'priority' => QueryInterface::ORDER_DESCENDING,
    ];

    /**
     * @param integer $id
     * @param array $params
     * @return CacheItem
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     */
    public function findOneByParams($id, $params)
    {
        $query = $this->createQuery(1);
        $query->matching($query->logicalAnd([
            $query->equals('pid', $id),
            $query->equals('params', json_encode($params)),
        ]));

        return $query->execute()->current();
    }

    /**
     * @return array
     */
    public function findDistinctRoots($priority = 0)
    {

        $sqlPriority = '';
        if($priority) {
            $sqlPriority = 'and ' . $this->getTableName() .  '.priority = '.intval($priority);
        }

        $result = $this->getDb()->exec_SELECTquery('pages.uid, pages.nav_title, pages.title, COUNT(' . $this->getTableName() .  '.uid) AS items',
            $this->getTableName() . ',pages', 'pages.uid = ' . $this->getTableName() .  '.root '.$sqlPriority, 'pages.uid', 'CONCAT(pages.nav_title, pages.title) ASC');

        $roots = [0 => ''];
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            $roots[$row['uid']] = ($row['nav_title'] ?: $row['title']) . ' (total: ' . (int)$row['items'] . ')';
        }

        return $roots;
    }

    /**
     * @param integer $id
     */
    public function resetWarm($id = null)
    {
        $this->getDb()->exec_UPDATEquery($this->getTableName(), '1=1' . ($id ? ' AND pid = ' . (int)$id : ''), [
            'warm' => 0,
        ]);
    }
}