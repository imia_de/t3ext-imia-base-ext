<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Domain\Repository;

use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;
use IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Storage\Typo3DbBackend;
use IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\Generic\Query;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * @package     imia_base_ext
 * @subpackage  Domain\Repository
 * @author      David Frerich <d.frerich@imia.de>
 */
class Repository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING,
    ];

    /**
     * @var boolean
     *
     * @deprecated
     */
    protected $respectEnableFields = true;

    /**
     * @var boolean
     */
    protected $respectStoragePage = true;

    /**
     * @var boolean
     */
    protected $respectSysLanguage = true;

    /**
     * @var boolean
     */
    protected $ignoreEnableFields = false;

    /**
     * @var boolean
     */
    protected $includeDeleted = false;

    /**
     * @var mixed
     */
    protected $languageOverlayMode = null;

    /**
     * @var string
     */
    protected $languageMode = null;

    /**
     * @var integer
     */
    protected $languageUid = null;

    /**
     * @var array
     */
    protected $enableFieldsToBeIgnored = null;

    /**
     * @var array
     */
    protected $storagePageIds = null;

    /**
     * @var bool
     */
    protected $useQueryCache = true;

    protected function getFormattedSQL($sql_raw) {
        if (empty($sql_raw) || !is_string($sql_raw)) {
            return false;
        }

        $sql_reserved_all = array( 'ACCESSIBLE', 'ACTION', 'ADD', 'AFTER', 'AGAINST', 'AGGREGATE', 'ALGORITHM', 'ALL', 'ALTER', 'ANALYSE', 'ANALYZE', 'AND', 'AS', 'ASC', 'AUTOCOMMIT', 'AUTO_INCREMENT', 'AVG_ROW_LENGTH', 'BACKUP', 'BEGIN', 'BETWEEN', 'BINLOG', 'BOTH', 'BY', 'CASCADE', 'CASE', 'CHANGE', 'CHANGED', 'CHARSET', 'CHECK', 'CHECKSUM', 'COLLATE', 'COLLATION', 'COLUMN', 'COLUMNS', 'COMMENT', 'COMMIT', 'COMMITTED', 'COMPRESSED', 'CONCURRENT', 'CONSTRAINT', 'CONTAINS', 'CONVERT', 'CREATE', 'CROSS', 'CURRENT_TIMESTAMP', 'DATABASE', 'DATABASES', 'DAY', 'DAY_HOUR', 'DAY_MINUTE', 'DAY_SECOND', 'DEFINER', 'DELAYED', 'DELAY_KEY_WRITE', 'DELETE', 'DESC', 'DESCRIBE', 'DETERMINISTIC', 'DISTINCT', 'DISTINCTROW', 'DIV', 'DO', 'DROP', 'DUMPFILE', 'DUPLICATE', 'DYNAMIC', 'ELSE', 'ENCLOSED', 'END', 'ENGINE', 'ENGINES', 'ESCAPE', 'ESCAPED', 'EVENTS', 'EXECUTE', 'EXISTS', 'EXPLAIN', 'EXTENDED', 'FAST', 'FIELDS', 'FILE', 'FIRST', 'FIXED', 'FLUSH', 'FOR', 'FORCE', 'FOREIGN', 'FROM', 'FULL', 'FULLTEXT', 'FUNCTION', 'GEMINI', 'GEMINI_SPIN_RETRIES', 'GLOBAL', 'GRANT', 'GRANTS', 'GROUP', 'HAVING', 'HEAP', 'HIGH_PRIORITY', 'HOSTS', 'HOUR', 'HOUR_MINUTE', 'HOUR_SECOND', 'IDENTIFIED', 'IF', 'IGNORE', 'IN', 'INDEX', 'INDEXES', 'INFILE', 'INNER', 'INSERT', 'INSERT_ID', 'INSERT_METHOD', 'INTERVAL', 'INTO', 'INVOKER', 'IS', 'ISOLATION', 'JOIN', 'KEY', 'KEYS', 'KILL', 'LAST_INSERT_ID', 'LEADING', 'LEFT', 'LEVEL', 'LIKE', 'LIMIT', 'LINEAR', 'LINES', 'LOAD', 'LOCAL', 'LOCK', 'LOCKS', 'LOGS', 'LOW_PRIORITY', 'MARIA', 'MASTER', 'MASTER_CONNECT_RETRY', 'MASTER_HOST', 'MASTER_LOG_FILE', 'MASTER_LOG_POS', 'MASTER_PASSWORD', 'MASTER_PORT', 'MASTER_USER', 'MATCH', 'MAX_CONNECTIONS_PER_HOUR', 'MAX_QUERIES_PER_HOUR', 'MAX_ROWS', 'MAX_UPDATES_PER_HOUR', 'MAX_USER_CONNECTIONS', 'MEDIUM', 'MERGE', 'MINUTE', 'MINUTE_SECOND', 'MIN_ROWS', 'MODE', 'MODIFY', 'MONTH', 'MRG_MYISAM', 'MYISAM', 'NAMES', 'NATURAL', 'NOT', 'NULL', 'OFFSET', 'ON', 'OPEN', 'OPTIMIZE', 'OPTION', 'OPTIONALLY', 'OR', 'ORDER', 'OUTER', 'OUTFILE', 'PACK_KEYS', 'PAGE', 'PARTIAL', 'PARTITION', 'PARTITIONS', 'PASSWORD', 'PRIMARY', 'PRIVILEGES', 'PROCEDURE', 'PROCESS', 'PROCESSLIST', 'PURGE', 'QUICK', 'RAID0', 'RAID_CHUNKS', 'RAID_CHUNKSIZE', 'RAID_TYPE', 'RANGE', 'READ', 'READ_ONLY', 'READ_WRITE', 'REFERENCES', 'REGEXP', 'RELOAD', 'RENAME', 'REPAIR', 'REPEATABLE', 'REPLACE', 'REPLICATION', 'RESET', 'RESTORE', 'RESTRICT', 'RETURN', 'RETURNS', 'REVOKE', 'RIGHT', 'RLIKE', 'ROLLBACK', 'ROW', 'ROWS', 'ROW_FORMAT', 'SECOND', 'SECURITY', 'SELECT', 'SEPARATOR', 'SERIALIZABLE', 'SESSION', 'SET', 'SHARE', 'SHOW', 'SHUTDOWN', 'SLAVE', 'SONAME', 'SOUNDS', 'SQL', 'SQL_AUTO_IS_NULL', 'SQL_BIG_RESULT', 'SQL_BIG_SELECTS', 'SQL_BIG_TABLES', 'SQL_BUFFER_RESULT', 'SQL_CACHE', 'SQL_CALC_FOUND_ROWS', 'SQL_LOG_BIN', 'SQL_LOG_OFF', 'SQL_LOG_UPDATE', 'SQL_LOW_PRIORITY_UPDATES', 'SQL_MAX_JOIN_SIZE', 'SQL_NO_CACHE', 'SQL_QUOTE_SHOW_CREATE', 'SQL_SAFE_UPDATES', 'SQL_SELECT_LIMIT', 'SQL_SLAVE_SKIP_COUNTER', 'SQL_SMALL_RESULT', 'SQL_WARNINGS', 'START', 'STARTING', 'STATUS', 'STOP', 'STORAGE', 'STRAIGHT_JOIN', 'STRING', 'STRIPED', 'SUPER', 'TABLE', 'TABLES', 'TEMPORARY', 'TERMINATED', 'THEN', 'TO', 'TRAILING', 'TRANSACTIONAL', 'TRUNCATE', 'TYPE', 'TYPES', 'UNCOMMITTED', 'UNION', 'UNIQUE', 'UNLOCK', 'UPDATE', 'USAGE', 'USE', 'USING', 'VALUES', 'VARIABLES', 'VIEW', 'WHEN', 'WHERE', 'WITH', 'WORK', 'WRITE', 'XOR', 'YEAR_MONTH' );
        $sql_skip_reserved_words = array('AS', 'ON', 'USING');
        $sql_special_reserved_words = array('(', ')');
        $sql_raw = str_replace("\n", " ", $sql_raw);
        $sql_formatted = "";
        $prev_word = "";
        $word = "";

        for ($i = 0, $j = strlen($sql_raw); $i < $j; $i++) {
            $word .= $sql_raw[$i];
            $word_trimmed = trim($word);

            if ($sql_raw[$i] == " " || in_array($sql_raw[$i], $sql_special_reserved_words)) {
                $word_trimmed = trim($word);
                $trimmed_special = false;

                if (in_array($sql_raw[$i], $sql_special_reserved_words)) {
                    $word_trimmed = substr($word_trimmed, 0, -1);
                    $trimmed_special = true;
                }
                $word_trimmed = strtoupper($word_trimmed);

                if (in_array($word_trimmed, $sql_reserved_all) && !in_array($word_trimmed, $sql_skip_reserved_words)) {
                    if (in_array($prev_word, $sql_reserved_all)) {
                        $sql_formatted .= '<b>' . strtoupper(trim($word)) . '</b>' . '&nbsp;';
                    } else {
                        $sql_formatted .= '<br/>&nbsp;';
                        $sql_formatted .= '<b>' . strtoupper(trim($word)) . '</b>' . '&nbsp;';
                    } $prev_word = $word_trimmed; $word = "";
                } else {
                    $sql_formatted .= trim($word) . '&nbsp;';
                    $prev_word = $word_trimmed; $word = "";
                }
            }
        }
        $sql_formatted .= trim($word);
        return $sql_formatted;
    }
    /**
     * Render the generated SQL of a query in TYPO3 8/9
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param bool $format
     * @param bool $exit
     */
    public function debugQuery($query, $format = true, $exit = true)
    {
        $queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
        $preparedStatement = $queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL();
        $parameters = $queryParser->convertQueryToDoctrineQueryBuilder($query)->getParameters();
        $stringParams = [];

        foreach ($parameters as $key => $parameter) {
            $stringParams[':' . $key] = $parameter;
        }
        $statement = strtr($preparedStatement, $stringParams);

        if ($format) {
            echo '<code>' . $this->getFormattedSQL($statement) . '</code>';
        } else {
            echo $statement;
        }

        if ($exit) {
            exit;
        }
    }

    /**
     * @param array $arguments
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryResultInterface
     */
    public function findBy(array $arguments, $limit = null, $offset = null, $sorting = null)
    {
        $query = $this->createQuery($limit, $offset, $sorting);

        if (count($arguments) > 0) {
            $constraints = [];
            foreach ($arguments as $key => $value) {
                $constraints[] = $query->equals($key, $value);
            }

            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->execute();
    }

    /**
     * @param array $arguments
     * @return integer
     */
    public function countBy(array $arguments)
    {
        $query = $this->createQuery();

        if (count($arguments) > 0) {
            $constraints = [];
            foreach ($arguments as $key => $value) {
                $constraints[] = $query->equals($key, $value);
            }

            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->count();
    }

    /**
     * @param array $arguments
     * @return object
     */
    public function findOneBy(array $arguments)
    {
        $query = $this->createQuery(1);

        $constraints = [];
        foreach ($arguments as $key => $value) {
            $constraints[] = $query->equals($key, $value);
        }

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query->execute()->getFirst();
    }

    /**
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryResultInterface
     */
    public function findAll($limit = null, $offset = null, $sorting = null)
    {
        return $this->createQuery($limit, $offset, $sorting)->execute();
    }

    /**
     * @param integer $fromTime
     * @param integer $toTime
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUpdated($fromTime, $toTime, $limit = null, $offset = null, $sorting = null)
    {
        $query = $this->createQuery($limit, $offset, $sorting);
        $query->matching(
            $query->logicalAnd([
                $query->greaterThan('tstamp', $fromTime),
                $query->lessThanOrEqual('tstamp', $toTime),
            ])
        );

        return $query->execute();
    }

    /**
     * @param array $uids
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @param boolean $keepOrder
     * @return QueryResultInterface|array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     */
    public function findByUids(array $uids, $limit = null, $offset = null, $sorting = null, $keepOrder = false)
    {
        return $this->findByInUid($uids, $keepOrder, $limit, $offset, $sorting);
    }

    /**
     * @param string $property
     * @param array $values
     * @return QueryResultInterface|array
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByIn($property, array $values)
    {
        if (count($values) > 0) {
            $query = $this->createQuery();
            $query->matching($query->in($property, $values));

            return $query->execute();
        } else {
            return [];
        }
    }

    /**
     * @param array $uids
     * @param boolean $keepOrder
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryResultInterface|array
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByInUid(array $uids, $keepOrder = false, $limit = null, $offset = null, $sorting = null)
    {
        if (($key = array_search(0, $uids)) !== false) {
            unset($uids[$key]);
        }

        if (count($uids) > 0) {
            $query = $this->createQuery($limit, $offset, $sorting);
            $constraints = [
                $query->in('uid', $uids),
            ];

            $transPointerField = $GLOBALS['TCA'][$this->getTableName()]['ctrl']['transOrigPointerField'];
            if ($transPointerField) {
                $constraints[] = $query->in($transPointerField, $uids);
            }

            $query->matching($query->logicalOr($constraints));

            if ($keepOrder) {
                $items = [];
                /** @var BaseEntity $item */
                foreach ($query->execute() as $item) {
                    $items[$item->getUid()] = $item;
                    $items[$item->getLocalizedUid()] = $item;
                }

                $results = [];
                foreach ($uids as $uid) {
                    if (isset($items[$uid])) {
                        $results[$items[$uid]->getUid()] = $items[$uid];
                    }
                }

                return $results;
            } else {
                return $query->execute();
            }
        } else {
            return [];
        }
    }

    /**
     * @param integer $pid
     * @return integer
     */
    public function countByPid($pid)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('pid', $pid)
        );

        return $query->count();
    }

    /**
     * @param integer $pid
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryResultInterface
     */
    public function findByPid($pid, $limit = null, $offset = null, $sorting = null)
    {
        $query = $this->createQuery($limit, $offset, $sorting);
        $query->matching(
            $query->equals('pid', $pid)
        );

        return $query->execute();
    }

    /**
     * @return integer
     */
    public function findLastModified()
    {
        $query = $this->createQuery(1, 0, ['tstamp', 'desc']);
        $result = $this->executeArrayQuery($query, ['tstamp']);

        $lastModified = 0;
        foreach ($result as $row) {
            $lastModified = (int)$row['tstamp'];
            break;
        }

        return $lastModified;
    }

    /**
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return QueryInterface
     */
    public function createQuery($limit = null, $offset = null, $sorting = null)
    {
        $this->getTableName(); // fix wrong datamap
        $query = parent::createQuery();

        if (!$this->respectEnableFields || $this->ignoreEnableFields) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
        }
        if (!$this->respectEnableFields || $this->includeDeleted) {
            $query->getQuerySettings()->setIncludeDeleted(true);
        }
        if (!$this->respectStoragePage) {
            $query->getQuerySettings()->setRespectStoragePage(false);
        }
        if (!$this->respectSysLanguage) {
            $query->getQuerySettings()->setRespectSysLanguage(false);
        }
        if ($this->languageOverlayMode !== null) {
            $query->getQuerySettings()->setLanguageOverlayMode($this->languageOverlayMode);
        }
        if ($this->languageMode !== null) {
            $query->getQuerySettings()->setLanguageMode($this->languageMode);
        }
        if ($this->languageUid !== null) {
            $query->getQuerySettings()->setLanguageUid($this->languageUid);
        }
        if ($this->enableFieldsToBeIgnored !== null) {
            $query->getQuerySettings()->setEnableFieldsToBeIgnored($this->enableFieldsToBeIgnored);
        }
        if ($this->storagePageIds !== null) {
            $query->getQuerySettings()->setStoragePageIds($this->storagePageIds);
        }

        /** @var Typo3QuerySettings $querySettings */
        $querySettings = $query->getQuerySettings();
        if ($querySettings instanceof Typo3QuerySettings) {
            $querySettings->useQueryCache($this->useQueryCache);
        }

        if ($limit && $limit > 0) {
            $query->setLimit($limit);
        }
        if ($offset && $offset > 0) {
            $query->setOffset($offset);
        }
        if ($sorting && is_array($sorting) && $sorting[0]) {
            $query->setOrderings([
                $sorting[0] => strtolower($sorting[1]) == 'asc' ?
                    QueryInterface::ORDER_ASCENDING :
                    QueryInterface::ORDER_DESCENDING,
            ]);
        }

        return $query;
    }

    /**
     * @param boolean $respectEnableFields
     * @return \IMIA\ImiaBaseExt\Domain\Repository\Repository
     *
     * @deprecated
     */
    public function setRespectEnableFields($respectEnableFields)
    {
        $this->setIgnoreEnableFields(!$respectEnableFields);
        $this->setIncludeDeleted(!$respectEnableFields);

        return $this;
    }

    /**
     * @param QueryInterface $query
     * @return array
     */
    protected function executeUidArrayQuery($query)
    {
        $uids = [];
        $result = $this->executeArrayQuery($query, ['uid']);
        foreach ($result as $row) {
            $uids[] = $row['uid'];
        }

        return $uids;
    }

    /**
     * @param QueryInterface $query
     * @param array $fields
     * @return array
     */
    protected function executeArrayQuery($query, array $fields = ['uid'])
    {
        /** @var Typo3QuerySettings $querySettings */
        $querySettings = $query->getQuerySettings();
        $querySettings->setOnlyFields($fields);

        return $query->execute(true);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @return \TYPO3\CMS\Core\Database\Query\QueryBuilder
     */
    protected function getQueryBuilder($query)
    {
        /** @var Typo3DbBackend $storageBackend */
        $storageBackend = $this->objectManager->get(Typo3DbBackend::class);
        $queryBuilder = $storageBackend->getQueryBuilder($query);

        return $queryBuilder;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @return string
     */
    protected function getQuerySql($query)
    {
        return $this->getQueryBuilder($query)->getSQL();
    }

    /**
     * @inheritdoc
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function __call($methodName, $arguments)
    {
        if (substr($methodName, 0, 8) === 'findByIn' && strlen($methodName) > 8 && ctype_upper(substr($methodName, 8, 1))) {
            $propertyName = lcfirst(substr($methodName, 8));
            $query = $this->createQuery();
            $query->matching($query->in($propertyName, $arguments[0]));

            return $query->execute();
        } elseif (substr($methodName, 0, 11) === 'findOneByIn' && strlen($methodName) > 11 && ctype_upper(substr($methodName, 11, 1))) {
            $propertyName = lcfirst(substr($methodName, 11));
            $query = $this->createQuery();
            $query->matching($query->in($propertyName, $arguments[0]));

            return $query->execute()->current();
        } elseif (substr($methodName, 0, 12) === 'findUidsByIn' && strlen($methodName) > 12 && ctype_upper(substr($methodName, 12, 1))) {
            $propertyName = lcfirst(substr($methodName, 12));
            $query = $this->createQuery();
            $query->matching($query->in($propertyName, $arguments[0]));

            return $this->executeUidArrayQuery($query);
        } elseif (substr($methodName, 0, 10) === 'findUidsBy' && strlen($methodName) > 10 && ctype_upper(substr($methodName, 10, 1))) {
            $propertyName = lcfirst(substr($methodName, 10));
            $query = $this->createQuery();
            $query->matching($query->in($propertyName, $arguments[0]));

            return $this->executeUidArrayQuery($query);
        } else {
            return parent::__call($methodName, $arguments);
        }
    }

    public function truncate()
    {
        $this->getDb()->exec_TRUNCATEquery($this->getTableName());

        return true;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->getDataMap()->getTableName();
    }

    /**
     * @return string
     */
    public function getColumnName($property)
    {
        return $this->getDataMap()->getColumnMap($property)->getColumnName();
    }

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return $this->getDataMap()->getClassName();
    }

    /**
     * @return DataMap
     */
    public function getDataMap()
    {
        /** @var DataMap $dataMap */
        $dataMap = $this->objectManager
            ->get(DataMapper::class)
            ->getDataMap($this->objectType);

        if ($GLOBALS['TSFE'] && isset($GLOBALS['TSFE']->tmpl->setup['plugin.'])) {
            $className = get_class($this);
            if (strpos($className, '\\') !== false) {
                $classNameParts = explode('\\', $className, 4);
                // Skip vendor and product name for core classes
                if (strpos($className, 'TYPO3\\CMS\\') === 0) {
                    $extensionName = $classNameParts[2];
                } else {
                    $extensionName = $classNameParts[1];
                }
            } else {
                list(, $extensionName) = explode('_', $className);
            }

            $tsExtName = 'tx_' . strtolower($extensionName);
            $tsTableName = $GLOBALS['TSFE']->tmpl->setup['plugin.'][$tsExtName . '.']['persistence.']['classes.'][$dataMap->getClassName() . '.']['mapping.']['tableName'];

            if (isset($tsTableName) && $tsTableName != $dataMap->getTableName()) {
                $dataMap->setTableName($tsTableName);
            }
        }

        return $dataMap;
    }

    /**
     * @return array
     */
    public function getStoragePageIds()
    {
        return $this->storagePageIds;
    }

    /**
     * @param array $storagePageIds
     * @return $this
     */
    public function setStoragePageIds($storagePageIds)
    {
        $this->storagePageIds = $storagePageIds;

        return $this;
    }

    /**
     * @return int
     */
    public function getLanguageUid()
    {
        return $this->languageUid;
    }

    /**
     * @param int $languageUid
     * @return $this
     */
    public function setLanguageUid($languageUid)
    {
        $this->languageUid = $languageUid;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguageMode()
    {
        return $this->languageMode;
    }

    /**
     * @param string $languageMode
     * @return $this
     */
    public function setLanguageMode($languageMode)
    {
        $this->languageMode = $languageMode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguageOverlayMode()
    {
        return $this->languageOverlayMode;
    }

    /**
     * @param mixed $languageOverlayMode
     * @return $this
     */
    public function setLanguageOverlayMode($languageOverlayMode)
    {
        $this->languageOverlayMode = $languageOverlayMode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeDeleted()
    {
        return $this->includeDeleted;
    }

    /**
     * @param boolean $includeDeleted
     * @return $this
     */
    public function setIncludeDeleted($includeDeleted)
    {
        $this->includeDeleted = $includeDeleted;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreEnableFields()
    {
        return $this->ignoreEnableFields;
    }

    /**
     * @param boolean $ignoreEnableFields
     * @return $this
     */
    public function setIgnoreEnableFields($ignoreEnableFields)
    {
        $this->ignoreEnableFields = $ignoreEnableFields;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getRespectSysLanguage()
    {
        return $this->respectSysLanguage;
    }

    /**
     * @param boolean $respectSysLanguage
     * @return $this
     */
    public function setRespectSysLanguage($respectSysLanguage)
    {
        $this->respectSysLanguage = $respectSysLanguage;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getRespectStoragePage()
    {
        return $this->respectStoragePage;
    }

    /**
     * @param boolean $respectStoragePage
     * @return $this
     */
    public function setRespectStoragePage($respectStoragePage)
    {
        $this->respectStoragePage = $respectStoragePage;

        return $this;
    }

    /**
     * @return array
     */
    public function getEnableFieldsToBeIgnored()
    {
        return $this->enableFieldsToBeIgnored;
    }

    /**
     * @param array $enableFieldsToBeIgnored
     * @return $this
     */
    public function setEnableFieldsToBeIgnored($enableFieldsToBeIgnored)
    {
        $this->enableFieldsToBeIgnored = $enableFieldsToBeIgnored;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getUseQueryCache()
    {
        return $this->useQueryCache;
    }

    /**
     * @param boolean $useQueryCache
     * @return Repository
     */
    public function setUseQueryCache($useQueryCache)
    {
        $this->useQueryCache = $useQueryCache;

        return $this;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}