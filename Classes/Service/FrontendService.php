<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Service;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\TimeTracker\TimeTracker;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  Service
 * @author      David Frerich <d.frerich@imia.de>
 */
class FrontendService
{
    /**
     * @var TypoScriptFrontendController
     */
    static protected $defaultTSFE = null;

    /**
     * @var string
     */
    static protected $defaultHost;

    /**
     * @var string
     */
    static protected $defaultRequestUri;

    /**
     * @var string
     */
    static protected $defaultScriptName;

    /**
     * @var ObjectManager
     */
    static protected $objectManager;

    /**
     * @var UriBuilder
     */
    static protected $uriBuilder;

    static public function replace($pageUid, $language)
    {
        self::$defaultTSFE = $GLOBALS['TSFE'];

        self::$defaultHost = $_SERVER['HTTP_HOST'];
        self::$defaultRequestUri = $_SERVER['REQUEST_URI'];
        self::$defaultScriptName = $_SERVER['SCRIPT_NAME'];

        self::init($pageUid, $language);
    }

    static public function reset()
    {
        $GLOBALS['TSFE'] = self::$defaultTSFE;

        $_SERVER['HTTP_HOST'] = self::$defaultHost;
        $_SERVER['REQUEST_URI'] = self::$defaultRequestUri;
        $_SERVER['SCRIPT_NAME'] = self::$defaultScriptName;
        GeneralUtility::flushInternalRuntimeCaches();
    }

    /**
     * @return UriBuilder
     */
    static public function getUriBuilder()
    {
        return self::$uriBuilder;
    }

    /**
     * @param integer $pageUid
     * @param integer $language
     * @return TypoScriptFrontendController
     */
    static protected function init($pageUid = 0, $language = 0)
    {
        if (TYPO3_MODE != 'FE') {
            if (!$GLOBALS['TSFE'] || $GLOBALS['TSFE']->id != $pageUid) {
                if ($pageUid) {
                    $rootline = BackendUtility::BEgetRootLine($pageUid);
                    $host = BackendUtility::firstDomainRecord($rootline);
                    $_SERVER['HTTP_HOST'] = $host;
                }

                $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] = '/typo3/index.php';
                GeneralUtility::flushInternalRuntimeCaches();

                if (!is_object($GLOBALS['TT'])) {
                    $GLOBALS['TT'] = self::getObjectManager()->get(TimeTracker::class);
                    $GLOBALS['TT']->start();
                }

                if (!$GLOBALS['TSFE']) {
                    $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
                        TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], $pageUid, 0, 1);
                }

                $GLOBALS['TSFE']->connectToDB();
                $GLOBALS['TSFE']->initFEuser();
                $GLOBALS['TSFE']->determineId();
                $GLOBALS['TSFE']->initTemplate();
                $GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
                $GLOBALS['TSFE']->forceTemplateParsing = 1;
                $GLOBALS['TSFE']->getConfigArray();
                $GLOBALS['TSFE']->cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);

                /** @var EnviromentServiceForceFrontend $enviromentService */
                $enviromentService = self::getObjectManager()->get(EnviromentServiceForceFrontend::class);

                /** @var Request $request */
                $request = self::getObjectManager()->get(Request::class);
                $request->injectEnvironmentService($enviromentService);
                $request->setRequestUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
                $request->setBaseUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . '/');

                /** @var UriBuilder $uriBuilder */
                $uriBuilder = self::getObjectManager()->get(UriBuilder::class);
                $uriBuilder->setRequest($request);
                $uriBuilder->injectEnvironmentService($enviromentService);

                self::$uriBuilder = $uriBuilder;
            }

            if ($language) {
                $GLOBALS['TSFE']->sys_language_uid = ($GLOBALS['TSFE']->sys_language_content = (int)$language);
                $GLOBALS['TSFE']->linkVars = '&L=' . (int)$language;
            }
        }

        return $GLOBALS['TSFE'];
    }

    /**
     * @return ObjectManager
     */
    static protected function getObjectManager()
    {
        if (!self::$objectManager) {
            self::$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        }

        return self::$objectManager;
    }
}
