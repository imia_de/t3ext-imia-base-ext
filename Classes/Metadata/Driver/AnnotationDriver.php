<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Metadata\Driver;

use Metadata\Driver\DriverInterface;
use Doctrine\Common\Annotations\Reader;
use IMIA\ImiaBaseExt\Metadata\ClassMetadata;
use IMIA\ImiaBaseExt\Metadata\PropertyMetadata;

/**
 * @package     imia_base_ext
 * @subpackage  Metadata\Driver
 * @author      David Frerich <d.frerich@imia.de>
 */
class AnnotationDriver implements DriverInterface
{
    /**
     * @var \Doctrine\Common\Annotations\Reader
     */
    private $reader;

    /**
     * @var string
     */
    private $mergeType;

    /**
     * @var array
     */
    private $annotations = [
        'classes'    => [
            'sqlTable'              => 'IMIA\ImiaBaseExt\Annotation\SQL\Table',
            'sqlIndex'              => 'IMIA\ImiaBaseExt\Annotation\SQL\Index',
            'tcaTable'              => 'IMIA\ImiaBaseExt\Annotation\TCA\Table',
            'tcaAddPaletteToTypes'  => 'IMIA\ImiaBaseExt\Annotation\TCA\AddPaletteToTypes',
            'tcaAddPalettesToTypes' => 'IMIA\ImiaBaseExt\Annotation\TCA\AddPalettesToTypes',
        ],
        'properties' => [
            'sqlColumn'       => 'IMIA\ImiaBaseExt\Annotation\SQL\Column',
            'tcaColumn'       => [
                'IMIA\ImiaBaseExt\Annotation\TCA\Categories',
                'IMIA\ImiaBaseExt\Annotation\TCA\Check',
                'IMIA\ImiaBaseExt\Annotation\TCA\Copy',
                'IMIA\ImiaBaseExt\Annotation\TCA\Column',
                'IMIA\ImiaBaseExt\Annotation\TCA\Files',
                'IMIA\ImiaBaseExt\Annotation\TCA\FilesImage',
                'IMIA\ImiaBaseExt\Annotation\TCA\FilesPDF',
                'IMIA\ImiaBaseExt\Annotation\TCA\FilesVideo',
                'IMIA\ImiaBaseExt\Annotation\TCA\FilesVideoYoutube',
                'IMIA\ImiaBaseExt\Annotation\TCA\Flex',
                'IMIA\ImiaBaseExt\Annotation\TCA\Group',
                'IMIA\ImiaBaseExt\Annotation\TCA\Inline',
                'IMIA\ImiaBaseExt\Annotation\TCA\Input',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputColorpicker',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputDate',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputDateTime',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputImagePosition',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputLink',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputNumber',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputTime',
                'IMIA\ImiaBaseExt\Annotation\TCA\InputSlider',
                'IMIA\ImiaBaseExt\Annotation\TCA\None',
                'IMIA\ImiaBaseExt\Annotation\TCA\Passthrough',
                'IMIA\ImiaBaseExt\Annotation\TCA\Radio',
                'IMIA\ImiaBaseExt\Annotation\TCA\Select',
                'IMIA\ImiaBaseExt\Annotation\TCA\Text',
                'IMIA\ImiaBaseExt\Annotation\TCA\TextRTE',
                'IMIA\ImiaBaseExt\Annotation\TCA\User',

                /* @deprecated since version 1.1.14 */
                'IMIA\ImiaBaseExt\Annotation\TCA\Colorpicker', // -> InputColorpicker
                'IMIA\ImiaBaseExt\Annotation\TCA\DateTime', // -> InputDateTime
                'IMIA\ImiaBaseExt\Annotation\TCA\Number', // -> InputNumber
                'IMIA\ImiaBaseExt\Annotation\TCA\ImageFiles', // -> FilesImages
            ],
            'tcaSearchField'  => 'IMIA\ImiaBaseExt\Annotation\TCA\SearchField',
            'tcaAddToTypes'   => 'IMIA\ImiaBaseExt\Annotation\TCA\AddToTypes',
            'tcaAddToPalette' => 'IMIA\ImiaBaseExt\Annotation\TCA\AddToPalette',
        ],
    ];

    /**
     * @param \Doctrine\Common\Annotations\Reader $reader
     * @param string $mergeType
     */
    public function __construct(Reader $reader, $mergeType = null)
    {
        $this->reader = $reader;
        $this->mergeType = $mergeType;

        $this->loadAnnotations();
    }

    /**
     * @param \ReflectionClass $class
     * @return \IMIA\ImiaBaseExt\Metadata\ClassMetadata
     */
    public function loadMetadataForClass(\ReflectionClass $class)
    {
        try {
            $classMetadata = new ClassMetadata($class->getName());
            $classMetadata->mergeType = $this->mergeType;

            $this->loadClassMetadata($classMetadata, $class);
            $this->loadPropertyMetadata($classMetadata, $class);
        } catch (\Exception $e) {
            echo '<pre>';
            echo '<h2>Annotation Exception</h2>';
            print_r($e->getMessage());
            exit;
        }

        return $classMetadata;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Metadata\ClassMetadata $classMetadata
     * @param \ReflectionClass $reflectionClass
     * @return \IMIA\ImiaBaseExt\Metadata\ClassMetadata
     */
    private function loadClassMetadata(&$classMetadata, $reflectionClass)
    {
        foreach ($this->annotations['classes'] as $field => $annotationClasses) {
            if (is_string($annotationClasses)) {
                $this->setFieldFromAnnotation($classMetadata, $field, $reflectionClass, $annotationClasses);
            } else {
                foreach ($annotationClasses as $annotationClass) {
                    if ($this->setFieldFromAnnotation($classMetadata, $field, $reflectionClass, $annotationClass)) {
                        break;
                    }
                }
            }
        }

        return $classMetadata;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Metadata\ClassMetadata $classMetadata
     * @param \ReflectionClass $reflectionClass
     * @return \IMIA\ImiaBaseExt\Metadata\ClassMetadata
     */
    private function loadPropertyMetadata(&$classMetadata, $reflectionClass)
    {
        foreach ($reflectionClass->getProperties() as $reflectionProperty) {
            $hasOwnProperty = true;
            if ($reflectionClass->getParentClass() && $reflectionClass->getParentClass()->hasProperty($reflectionProperty->getName())) {
                if ($reflectionClass->getParentClass()->getProperty($reflectionProperty->getName())->getDocComment() == $reflectionProperty->getDocComment()) {
                    $hasOwnProperty = false;
                }
            }

            if ($hasOwnProperty) {
                $propertyMetadata = new PropertyMetadata($reflectionClass->getName(), $reflectionProperty->getName());
                foreach ($this->annotations['properties'] as $field => $annotationClasses) {
                    if (is_string($annotationClasses)) {
                        $this->setFieldPropertyFromAnnotation($propertyMetadata, $field, $reflectionProperty, $annotationClasses);
                    } else {
                        foreach ($annotationClasses as $annotationClass) {
                            if ($this->setFieldPropertyFromAnnotation($propertyMetadata, $field, $reflectionProperty, $annotationClass)) {
                                break;
                            }
                        }
                    }
                }

                $classMetadata->addPropertyMetadata($propertyMetadata);
            }
        }

        return $classMetadata;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Metadata\ClassMetadata $classMetadata
     * @param string $field
     * @param \ReflectionClass $reflectionClass
     * @param string|array $annotationClass
     * @return boolean
     */
    private function setFieldFromAnnotation(&$classMetadata, $field, $reflectionClass, $annotationClass)
    {
        $success = false;
        $annotation = $this->reader->getClassAnnotation(
            $reflectionClass,
            $annotationClass
        );

        if (null !== $annotation) {
            $classMetadata->$field = $annotation;
            $success = true;
        }

        return $success;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Metadata\PropertyMetadata $propertyMetadata
     * @param string $field
     * @param \ReflectionClass $reflectionClass
     * @param string|array $annotationClass
     * @return boolean
     */
    private function setFieldPropertyFromAnnotation(&$propertyMetadata, $field, $reflectionProperty, $annotationClass)
    {
        $success = false;
        $annotation = $this->reader->getPropertyAnnotation(
            $reflectionProperty,
            $annotationClass
        );

        if (null !== $annotation) {
            $propertyMetadata->$field = $annotation;
            $success = true;
        }

        return $success;
    }

    /**
     */
    private function loadAnnotations()
    {
        foreach ($this->loadAnnotationClasses() as $annotationFile) {
            require_once($annotationFile);
        }
    }

    /**
     * @return array
     */
    private function loadAnnotationClasses()
    {
        return (array)@include(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_base_ext', 'ext_autoload_annotations.php'));
    }
}