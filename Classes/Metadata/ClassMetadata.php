<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Metadata;

use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration;
use Metadata\MergeableInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Metadata
 * @author      David Frerich <d.frerich@imia.de>
 */
class ClassMetadata extends \Metadata\MergeableClassMetadata
{
    /**
     * @var string
     */
    public $mergeType;

    /**
     * @var SQL\Table
     */
    public $sqlTable;

    /**
     * @var SQL\Index
     */
    public $sqlIndex;

    /**
     * @var TCA\Table
     */
    public $tcaTable;

    /**
     * @var TCA\AddPaletteToTypes
     */
    public $tcaAddPaletteToTypes;

    /**
     * @var TCA\AddPalettesToTypes
     */
    public $tcaAddPalettesToTypes;

    /**
     * @param MergeableInterface $object
     */
    public function merge(MergeableInterface $object)
    {
        /** @var ClassMetadata $object */

        $tcaProperties = ['tcaTable', 'tcaAddPaletteToTypes', 'tcaAddPalettesToTypes'];
        $sqlProperties = ['sqlTable', 'sqlIndex'];

        $currentExtKey = DynamicConfiguration::getExtKeyForClass($this->name);
        $nextExtKey = DynamicConfiguration::getExtKeyForClass($object->name);

        /** @var \ReflectionClass $reflectionClass */
        $reflectionClass = $object->reflection;
        $parentReflectionClass = $reflectionClass->getParentClass();

        $isAbstract = $reflectionClass->isAbstract() || $parentReflectionClass && $parentReflectionClass->isAbstract();
        if (!$isAbstract && $currentExtKey && $nextExtKey && $currentExtKey !== $nextExtKey) {
            // conflicts detected
            switch ($this->mergeType) {
                case 'sql':
                    // merge sql (table name), don't merge tca
                    foreach ($sqlProperties as $property) {
                        $this->mergeObjectProperty($object, $property);
                    }
                    foreach ($tcaProperties as $property) {
                        if (is_object($object->$property)) {
                            $this->$property = clone $object->$property;
                        } else {
                            $this->$property = $object->$property;
                        }
                    }

                    // replace property metadata
                    foreach ($this->propertyMetadata as $field => $propertyMetaData) {
                        if ($propertyMetaData && is_object($propertyMetaData)) {
                            $this->propertyMetadata[$field] = $object->propertyMetadata[$field]
                                ? clone $object->propertyMetadata[$field]
                                : null;

                            if ($this->sqlTable->createAll && $this->propertyMetadata[$field] === null) {
                                $this->propertyMetadata[$field] = $propertyMetaData;
                            } elseif ($this->propertyMetadata[$field] && $this->propertyMetadata[$field]->sqlColumn && $propertyMetaData->sqlColumn) {
                                foreach (get_object_vars($propertyMetaData->sqlColumn) as $propertyName => $propertyData) {
                                    if (isset($propertyData) && $propertyData !== null) {
                                        if (!isset($this->propertyMetadata[$field]->sqlColumn->$propertyName) || $this->propertyMetadata[$field]->sqlColumn->$propertyName === null) {
                                            $this->propertyMetadata[$field]->sqlColumn->$propertyName = $propertyData;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 'tca':
                    // merge sql (table name), don't merge tca
                    foreach ($sqlProperties as $property) {
                        $this->mergeObjectProperty($object, $property);
                    }

                    foreach ($tcaProperties as $property) {
                        if (!$object->tcaTable->createAll || $property != 'tcaTable') {
                            if (is_object($object->$property)) {
                                $this->$property = clone $object->$property;
                            } else {
                                $this->$property = $object->$property;
                            }
                        }
                    }
                    if ($object->tcaTable->createAll) {
                        $this->mergeObjectProperty($object, 'tcaTable');
                    }

                    // replace property metadata
                    foreach ($this->propertyMetadata as $field => $propertyMetaData) {
                        if ($propertyMetaData && is_object($propertyMetaData)) {
                            $sqlColumn = $object->propertyMetadata[$field]->sqlColumn
                                ? clone $object->propertyMetadata[$field]->sqlColumn
                                : null;

                            if ($sqlColumn === null) {
                                $sqlColumn = $propertyMetaData->sqlColumn;
                            } elseif ($propertyMetaData->sqlColumn) {
                                foreach (get_object_vars($propertyMetaData->sqlColumn) as $propertyName => $propertyData) {
                                    if (isset($propertyData) && $propertyData !== null) {
                                        if (!isset($sqlColumn->$propertyName) || $sqlColumn->$propertyName === null) {
                                            $sqlColumn->$propertyName = $propertyData;
                                        }
                                    }
                                }
                            }

                            $this->propertyMetadata[$field] = $object->propertyMetadata[$field]
                                ? clone $object->propertyMetadata[$field]
                                : null;

                            if ($this->tcaTable->createAll && $this->propertyMetadata[$field] === null) {
                                $this->propertyMetadata[$field] = $propertyMetaData;
                            } elseif ($this->propertyMetadata[$field] && $this->tcaTable->createAll && $this->propertyMetadata[$field]->tcaColumn === null) {
                                $this->propertyMetadata[$field]->tcaColumn = $propertyMetaData->tcaColumn;
                            }

                            if ($this->propertyMetadata[$field]) {
                                $this->propertyMetadata[$field]->sqlColumn = $sqlColumn;
                            }
                            if ($this->tcaTable->createAll && !$object->propertyMetadata[$field]) {
                                $object->propertyMetadata[$field] = $this->propertyMetadata[$field];
                            } elseif ($this->propertyMetadata[$field] && $this->tcaTable->createAll && !$object->propertyMetadata[$field]->tcaColumn) {
                                $object->propertyMetadata[$field]->tcaColumn = $this->propertyMetadata[$field]->tcaColumn;
                            }
                        }
                    }
                    break;
                case 'ts':
                    // merge sql and tca
                    foreach ($sqlProperties as $property) {
                        $this->mergeObjectProperty($object, $property);
                    }
                    foreach ($tcaProperties as $property) {
                        $this->mergeObjectProperty($object, $property);
                    }

                    // replace property metadata
                    foreach ($this->propertyMetadata as $field => $propertyMetaData) {
                        if ($propertyMetaData && is_object($propertyMetaData)) {
                            $this->propertyMetadata[$field] = $object->propertyMetadata[$field]
                                ? clone $object->propertyMetadata[$field]
                                : null;

                            if ($this->sqlTable->createAll && $this->propertyMetadata[$field] === null) {
                                $this->propertyMetadata[$field] = $propertyMetaData;
                            } elseif ($this->propertyMetadata[$field] && $this->propertyMetadata[$field]->sqlColumn && $propertyMetaData->sqlColumn) {
                                foreach (get_object_vars($propertyMetaData->sqlColumn) as $propertyName => $propertyData) {
                                    if (isset($propertyData) && $propertyData !== null) {
                                        if (!isset($this->propertyMetadata[$field]->sqlColumn->$propertyName) || $this->propertyMetadata[$field]->sqlColumn->$propertyName === null) {
                                            $this->propertyMetadata[$field]->sqlColumn->$propertyName = $propertyData;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        } else {
            // merge all, no problems
            foreach ($sqlProperties as $property) {
                $this->mergeObjectProperty($object, $property);
            }
            foreach ($tcaProperties as $property) {
                $this->mergeObjectProperty($object, $property);
            }
        }

        parent::merge($object);
    }

    /**
     * @param MergeableInterface $object
     * @param string $property
     */
    private function mergeObjectProperty(MergeableInterface $object, $property)
    {
        if ($this->$property && $object->$property) {
            foreach (get_object_vars($this->$property) as $objectProperty => $value) {
                if (isset($object->$property->$objectProperty)) {
                    if (is_object($object->$property->$objectProperty)) {
                        $this->$property->$objectProperty = clone $object->$property->$objectProperty;
                    } elseif (is_array($this->$property->$objectProperty) && is_array($object->$property->$objectProperty)) {
                        ArrayUtility::mergeRecursiveWithOverrule($this->$property->$objectProperty, $object->$property->$objectProperty);
                    } else {
                        $this->$property->$objectProperty = $object->$property->$objectProperty;
                    }
                }
            }
        } elseif ($object->$property) {
            switch ($property) {
                case 'sqlTable':
                    $this->$property = new SQL\Table;
                    $this->mergeObjectProperty($object, $property);
                    break;
                case 'sqlIndex':
                    $this->$property = new SQL\Index;
                    $this->mergeObjectProperty($object, $property);
                    break;
                case 'tcaTable':
                    $this->$property = new TCA\Table;
                    $this->mergeObjectProperty($object, $property);
                    break;
                case 'tcaAddPaletteToTypes':
                    $this->$property = new TCA\AddPaletteToTypes;
                    $this->mergeObjectProperty($object, $property);
                    break;
                case 'tcaAddPalettesToTypes':
                    $this->$property = new TCA\AddPalettesToTypes;
                    $this->mergeObjectProperty($object, $property);
                    break;
                default:
                    if (is_object($object->$property)) {
                        $this->$property = clone $object->$property;
                    } elseif (is_array($this->$property) && is_array($object->$property)) {
                        ArrayUtility::mergeRecursiveWithOverrule($this->$property, $object->$property);
                    } else {
                        $this->$property = $object->$property;
                    }
                    break;
            }
        }
    }
}
