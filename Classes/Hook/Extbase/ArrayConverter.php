<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Extbase;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface;
use TYPO3\CMS\Extbase\Property\TypeConverter\AbstractTypeConverter;

/**
 * @package     imia_base_ext
 * @subpackage  Property\TypeConverter
 * @author      David Frerich <d.frerich@imia.de>
 */
class ArrayConverter extends AbstractTypeConverter implements SingletonInterface
{
    /**
     * @var array<string>
     */
    protected $sourceTypes = ['array'];

    /**
     * @var string
     */
    protected $targetType = 'string';

    /**
     * @var integer
     */
    protected $priority = 99;

    /**
     * @param string $source
     * @param string $targetType
     *
     * @return boolean
     */
    public function canConvertFrom($source, $targetType)
    {
        return is_array($source);
    }

    /**
     * @param array $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param PropertyMappingConfigurationInterface $configuration
     * @return array
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = [],
                                PropertyMappingConfigurationInterface $configuration = null)
    {
        $target = [];
        foreach ($source as $value) {
            if (trim($value)) {
                $target[] = trim($value);
            }
        }

        return implode(',', $target);
    }
}