<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Frontend;

use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Frontend\Page\PageRepositoryGetRecordOverlayHookInterface;

/**
 * @package     imia_base
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageRepositoryGetRecordOverlayHook implements PageRepositoryGetRecordOverlayHookInterface
{
    /**
     * @param string $table
     * @param array $row
     * @param integer $sys_language_content
     * @param string $OLmode
     * @param PageRepository $parent
     */
    public function getRecordOverlay_preProcess($table, &$row, &$sys_language_content, $OLmode, PageRepository $parent)
    {
    }

    /**
     * @param string $table
     * @param array $row
     * @param integer $sys_language_content
     * @param string $OLmode
     * @param PageRepository $parent
     */
    public function getRecordOverlay_postProcess($table, &$row, &$sys_language_content, $OLmode, PageRepository $parent)
    {
        if ($sys_language_content > 0 && $row && isset($row['_LOCALIZED_UID'])) {
            if ($row['_LOCALIZED_UID'] != $row['uid']) {
                $row['_LOCALIZED_LANGUAGE_UID'] = $sys_language_content;
            }
        }
    }
}
