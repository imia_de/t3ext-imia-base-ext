<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Frontend;

use TYPO3\CMS\Core\SingletonInterface;
use IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem;
use IMIA\ImiaBaseExt\Domain\Repository\CacheWarmer\CacheItemRepository;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * @package     imia_base_ext
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController implements SingletonInterface
{
    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe
     */
    public function checkAlternativeIdMethods($params, &$tsfe)
    {
        if (!$tsfe->isBackendUserLoggedIn()) {
            ExtensionManagementUtility::loadExtTables(true);
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'] as $classReference) {
                    /** @var $hookObject \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface */
                    $hookObject = GeneralUtility::getUserObj($classReference);
                    if (!$hookObject instanceof \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface) {
                        throw new \UnexpectedValueException(
                            '$hookObject "' . $classReference . '" must implement interface TYPO3\\CMS\\Core\\Database\\TableConfigurationPostProcessingHookInterface',
                            1320585902
                        );
                    }
                    $hookObject->processData();
                }
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     * @param integer $timeOutTime
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     */
    public function insertPageIncache($pObj, $timeOutTime)
    {
        if (!$pObj->fe_user->user['uid'] && $pObj->isStaticCacheble()) {
            $cacheItemRepository = $this->getCacheItemRepository();

            $priority = 10;
            switch (count($pObj->rootLine)) {
                case 1:
                    $priority *= 5;
                    break;
                case 2:
                    $priority *= 4;
                    break;
                case 3:
                    $priority *= 3;
                    break;
                case 4:
                    $priority *= 2;
                    break;
            }

            $cacheItem = $cacheItemRepository->findOneByParams($pObj->id, $this->getParams($pObj));
            if (!$cacheItem) {
                $cacheItem = GeneralUtility::makeInstance(CacheItem::class);
                $params = $this->getParams($pObj);

                $cacheItem
                    ->setPid($pObj->id)
                    ->setParams($params)
                    ->setWarm(true)
                    ->setRoot((int)$pObj->domainStartPage);

                $paramString = '';
                foreach ($cacheItem->getParams() as $key => $value) {
                    $paramString .= '&' . $key . '=' . $value;
                }

                $uri = $pObj->cObj->typoLink_URL([
                    'parameter'        => $cacheItem->getPid(),
                    'forceAbsoluteUrl' => true,
                    'additionalParams' => $paramString,
                    'useCacheHash'     => true,
                ]);

                if (count($cacheItem->getSecondaryParams()) >= 4) {
                    $priority = floor($priority / 7);
                } elseif (count($cacheItem->getSecondaryParams()) >= 3) {
                    $priority = floor($priority / 6);
                } elseif (count($cacheItem->getSecondaryParams()) >= 2) {
                    $priority = floor($priority / 5);
                } elseif (count($cacheItem->getSecondaryParams()) >= 1) {
                    $priority = floor($priority / 4);
                }

                $cacheItem
                    ->setUri($uri)
                    ->setPriority($priority);

                $cacheItemRepository->add($cacheItem);
                $this->persistAll();
            } else {
                if (!$cacheItem->getWarm()) {
                    if (count($cacheItem->getSecondaryParams()) >= 4) {
                        $priority = floor($priority / 7);
                    } elseif (count($cacheItem->getSecondaryParams()) >= 3) {
                        $priority = floor($priority / 6);
                    } elseif (count($cacheItem->getSecondaryParams()) >= 2) {
                        $priority = floor($priority / 5);
                    } elseif (count($cacheItem->getSecondaryParams()) >= 1) {
                        $priority = floor($priority / 4);
                    }

                    $cacheItem
                        ->setWarm(true)
                        ->setPriority($priority)
                        ->setRoot((int)$pObj->domainStartPage);

                    if (!$cacheItem->getUri()) {
                        $paramString = '';
                        foreach ($cacheItem->getParams() as $key => $value) {
                            $paramString .= '&' . $key . '=' . $value;
                        }

                        $uri = $pObj->cObj->typoLink_URL([
                            'parameter'        => $cacheItem->getPid(),
                            'forceAbsoluteUrl' => true,
                            'additionalParams' => $paramString,
                            'useCacheHash'     => true,
                        ]);

                        $cacheItem->setUri($uri);
                    }

                    $cacheItemRepository->update($cacheItem);
                    $this->persistAll();
                }
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     * @return array
     */
    protected function getParams($pObj)
    {
        $params = [
            'id'              => (int)$pObj->id,
            'type'            => (int)$pObj->type,
            'gr_list'         => (string)$pObj->gr_list,
            'MP'              => (string)$pObj->MP,
            'domainStartPage' => (int)$pObj->domainStartPage,
        ];

        if (is_array($pObj->TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['createHashBase'])) {
            $_params = [
                'hashParameters'     => &$params,
                'createLockHashBase' => false,
            ];
            foreach ($pObj->TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['createHashBase'] as $_funcRef) {
                GeneralUtility::callUserFunction($_funcRef, $_params, $this);
            }
        }

        unset($params['gr_list']);
        unset($params['domainStartPage']);
        if (isset($params['cHash'])) {
            unset($params['cHash']);
        }
        if (isset($params['id'])) {
            unset($params['id']);
        }
        if (isset($params['encryptionKey'])) {
            unset($params['encryptionKey']);
        }
        if (isset($params['type']) && !$params['type']) {
            unset($params['type']);
        }
        if (isset($params['MP']) && !$params['MP']) {
            unset($params['MP']);
        }

        $params['L'] = $pObj->sys_language_uid;

        $cHashArray = $pObj->cHash_array;
        if (trim($GLOBALS['TYPO3_CONF_VARS']['FE']['cHashExcludedParametersIfEmpty']) === '*') {
            $requiredParams = array_map('trim', explode(',', $GLOBALS['TYPO3_CONF_VARS']['FE']['cHashRequiredParameters']));

            $cHashArrayNew = [];
            foreach ($cHashArray as $key => $value) {
                if (in_array($key, $requiredParams)) {
                    $cHashArrayNew[$key] = $value;
                }
            }
            $cHashArray = $cHashArrayNew;
        }

        $params = array_merge($params, $cHashArray);
        ksort($params);

        return $params;
    }

    /**
     * @return CacheItemRepository
     */
    protected function getCacheItemRepository()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var CacheItemRepository $cacheItemRepository */
        $cacheItemRepository = $objectManager->get(CacheItemRepository::class);

        return $cacheItemRepository;
    }

    protected function persistAll()
    {
        GeneralUtility::makeInstance(ObjectManager::class)
            ->get(PersistenceManager::class)
            ->persistAll();
    }
}