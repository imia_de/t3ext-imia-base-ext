<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Core;

use IMIA\ImiaBaseExt\Utility\DynamicConfiguration;
use TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TableConfigurationPostProcessingCached implements TableConfigurationPostProcessingHookInterface
{
    public function processData()
    {
        global $TCA;

        // Build SQL, TCA and TypoScript for all Extensions
        DynamicConfiguration::build('tables');

        // load dynConfig files
        foreach ($TCA as $tableName => $_) {
            if (!isset($TCA[$tableName]['columns'])) {
                $columnsConfigFile = $TCA[$tableName]['ctrl']['dynamicConfigFile'];
                if ($columnsConfigFile) {
                    if (GeneralUtility::isAbsPath($columnsConfigFile)) {
                        include($columnsConfigFile);
                    }
                }
            }
        }

        if (array_key_exists('tx_powermail_domain_model_answer', $TCA)) {
            $TCA['tx_powermail_domain_model_answer']['ctrl']['excludeOnPageCopy'] = true;
        }

        if (array_key_exists('tx_powermail_domain_model_answers', $TCA)) {
            $TCA['tx_powermail_domain_model_answers']['ctrl']['excludeOnPageCopy'] = true;
        }

        if (array_key_exists('tx_powermail_domain_model_mail', $TCA)) {
            $TCA['tx_powermail_domain_model_mail']['ctrl']['excludeOnPageCopy'] = true;
        }

        if (array_key_exists('tx_powermail_domain_model_mails', $TCA)) {
            $TCA['tx_powermail_domain_model_mails']['ctrl']['excludeOnPageCopy'] = true;
        }

        $TCA['be_groups']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['be_users']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['sys_file']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['sys_file_metadata']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['sys_file_storage']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['sys_language']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['tx_extensionmanager_domain_model_extension']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['tx_extensionmanager_domain_model_repository']['ctrl']['excludeOnPageCopy'] = true;
        $TCA['tx_scheduler_task_group']['ctrl']['excludeOnPageCopy'] = true;
    }
}