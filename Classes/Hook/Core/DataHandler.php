<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Core;

use IMIA\ImiaBaseExt\Domain\Repository\CacheWarmer\CacheItemRepository;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * @package     imia_base_ext
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class DataHandler
{
    /**
     * @param array $params
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
     */
    public function clearCachePostProc($params, $dataHandler)
    {
        if (array_key_exists('cacheCmd', $params) && in_array($params['cacheCmd'], ['temp_cached', 'all', 'system'])) {
            DynamicConfiguration::clearCache();
        } elseif (array_key_exists('cacheCmd', $params) && preg_match('/imia_base_ext-(.+)/ism', $params['cacheCmd'], $match)) {
            DynamicConfiguration::create($match[1]);
        }

        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_base_ext']);
        if ($extConf['cacheWarmer']) {
            if (in_array($params['cacheCmd'], ['pages'])) {
                $this->getCacheItemRepository()->resetWarm();
            } elseif (MathUtility::canBeInterpretedAsInteger($params['cacheCmd'])) {
                $this->getCacheItemRepository()->resetWarm((int)$params['cacheCmd']);
            }
        }
    }

    /**
     * @return CacheItemRepository
     */
    protected function getCacheItemRepository()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var CacheItemRepository $cacheItemRepository */
        $cacheItemRepository = $objectManager->get(CacheItemRepository::class);

        return $cacheItemRepository;
    }
}