<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Core;

use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TableConfigurationPostProcessing implements TableConfigurationPostProcessingHookInterface
{
    /**
     * @throws \TYPO3\CMS\Core\Cache\Exception\InvalidDataException
     */
    public function processData()
    {
        /** @var $codeCache \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend */
        $codeCache = GeneralUtility::makeInstance(CacheManager::class)->getCache('cache_core');

        $processedTCA = $codeCache->get('imiabase_processed_tca');
        $processedPageTypes = $codeCache->get('imiabase_processed_page_types');
        if (!$processedTCA || !$processedPageTypes) {
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'] as $classReference) {
                    /** @var $hookObject \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface */
                    $hookObject = GeneralUtility::getUserObj($classReference);
                    if (!$hookObject instanceof \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface) {
                        throw new \UnexpectedValueException(
                            '$hookObject "' . $classReference . '" must implement interface TYPO3\\CMS\\Core\\Database\\TableConfigurationPostProcessingHookInterface',
                            1320585902
                        );
                    }
                    $hookObject->processData();
                }
            }

            if (TYPO3_MODE == 'BE') {
                $codeCache->set('imiabase_processed_tca', serialize($GLOBALS['TCA']));
                $codeCache->set('imiabase_processed_page_types', serialize($GLOBALS['PAGES_TYPES']));
            }
        } else {
            $GLOBALS['TCA'] = unserialize(substr($processedTCA, 6, -2));
            $GLOBALS['PAGES_TYPES'] = unserialize(substr($processedPageTypes, 6, -2));
        }
    }
}