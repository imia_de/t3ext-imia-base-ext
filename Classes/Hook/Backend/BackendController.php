<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Hook\Backend;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendController
{
    /**
     * @param array $conf
     * @param \TYPO3\CMS\Backend\Controller\BackendController $pObj
     */
    public function constructPostProcess($conf, \TYPO3\CMS\Backend\Controller\BackendController $backendController)
    {
        $resourcePath = '/' . ExtensionManagementUtility::siteRelPath('imia_base_ext') . 'Resources/Public/';

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addRequireJsConfiguration([
            'paths' => [
                'TYPO3/CMS/Backend/Toolbar/ClearCacheMenu' => $resourcePath . 'Javascripts/Override/backend/Toolbar/ClearCacheMenu',
            ],
        ]);
    }
}