<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Components;

/**
 * @package     imia_base_ext
 * @subpackage  Components
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractTree
{
    /**
     * @var string
     */
    protected $treeName = 'pagetree';

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getTreeHtml();
    }

    /**
     *
     * @param array $items
     * @param integer $level
     * @return string
     */
    protected function getTreeItemHtml($items, $level = 0)
    {
        $indent = '';
        for ($j = 0; $j <= $level; $j++) {
            $indent .= '<img class="' . ($j == 0 ? 'x-tree-icon' : 'x-tree-elbow-line') . '" src="/typo3/gfx/clear.gif" alt="">';
        }

        $i = 0;
        $itemCount = count($items);

        $itemHtml = '';
        foreach ($items as $item) {
            $i++;

            $permission = true;
            if (array_key_exists('check_permission', $item) && $item['check_permission']) {
                $permission = $GLOBALS['BE_USER']->isInWebMount($item['uid']);
            }

            if ($permission) {
                $subItemHtml = '';
                if (is_array($item['items']) && count($item['items']) > 0) {
                    $subItemHtml = $this->getTreeItemHtml($item['items'], $level + 1);
                }

                $itemHtml .= '
                    <li class="x-tree-node">
                        <div unselectable="on" class="x-tree-node-el x-unselectable ' . ($subItemHtml ? 'x-tree-node-collapsed' : 'x-tree-node-leaf') . '">' .
                    '<span class="x-tree-node-indent">' . $indent . '</span>' .
                    '<img class="x-tree-ec-icon x-tree-elbow' . ($i >= $itemCount ? '-end' : '') . ($subItemHtml ? '-plus' : '') . '" src="/typo3/gfx/clear.gif" alt="">' .
                    '<span class="t3-icon ' . $item['class'] . '">' . ($item['hidden']
                        ? '<span class="t3-icon t3-icon-status t3-icon-status-overlay t3-icon-overlay-hidden t3-icon-overlay">&nbsp;</span>'
                        : ($item['scheduled']
                            ? '<span class="t3-icon t3-icon-status t3-icon-status-overlay t3-icon-overlay-scheduled t3-icon-overlay">&nbsp;</span>'
                            : ($item['scheduled-future']
                                ? '<span class="t3-icon t3-icon-status t3-icon-status-overlay t3-icon-overlay-scheduled-future-end t3-icon-overlay">&nbsp;</span>'
                                : ($item['restricted']
                                    ? '<span class="t3-icon t3-icon-status t3-icon-status-overlay t3-icon-overlay-access-restricted t3-icon-overlay">&nbsp;</span>'
                                    : '&nbsp;'
                                )
                            )
                        )
                    ) . '</span>' .
                    '<a tabindex="1" ' . ($item['url'] ? 'href="' . $item['url'] . '" target="content" ' : 'href="" onclick="return false" ') . 'class="x-tree-node-anchor" hidefocus="on">' .
                    '<span unselectable="on">' . $item['title'] . '</span>' .
                    '</a>' .
                    '</div>' .
                    '<ul class="x-tree-node-ct" style="display: none">' . $subItemHtml . '</ul>
                    </li>';
            }
        }

        return $itemHtml;
    }

    /**
     * @return string
     */
    protected function getTreeHtml()
    {
        $itemHtml = $this->getTreeItemHtml($this->items);

        return '
            <div class="x-panel-body x-panel-body-noheader x-panel-body-noborder">
                <div class="x-panel">
                    <div class="x-panel-bwrap">
                        <div class="x-panel-body x-panel-body-noheader x-border-layout-ct">
                            <div id="typo3-' . $this->treeName . '-topPanelItems" class="x-panel x-panel-noborder x-border-panel">
                                <div class="x-panel-bwrap">
                                    <div class="x-panel-body x-panel-body-noheader x-panel-body-noborder">
                                        <div id="typo3-' . $this->treeName . '-topPanel" class="x-panel x-panel-noborder">
                                            <div class="x-panel-bwrap">
                                                <div class="x-panel-tbar x-panel-tbar-noheader x-panel-tbar-noborder">
                                                    <div class="x-toolbar x-small-editor x-toolbar-layout-ct">
                                                        <table cellspacing="0" class="x-toolbar-ct">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" class="x-toolbar-left">' . /*
                                                                        <table cellspacing="0">
                                                                            <tbody>
                                                                                <tr class="x-toolbar-left-row">
                                                                                    <td class="x-toolbar-cell">
                                                                                        <div class="x-btn typo3-' . $this->treeName . '-topPanel-button x-btn-icon" id="typo3-' . $this->treeName . '-topPanel-button-newNode">
                                                                                            <button class="x-btn-text t3-icon t3-icon-actions t3-icon-actions-page t3-icon-page-new">&nbsp;</button>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="x-toolbar-cell">
                                                                                        <div class="x-btn typo3-' . $this->treeName . '-topPanel-button x-btn-icon" id="typo3-' . $this->treeName . '-topPanel-button-filter">
                                                                                            <button class="x-btn-text t3-icon t3-icon-actions t3-icon-actions-system t3-icon-system-tree-search-open">&nbsp;</button>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>*/
        '
                                                                    </td>
                                                                    <td align="right" class="x-toolbar-right">
                                                                        <table cellspacing="0" class="x-toolbar-right-ct">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0">
                                                                                            <tbody>
                                                                                                <tr class="x-toolbar-right-row">
                                                                                                    <td class="x-toolbar-cell">
                                                                                                        <div class="x-btn typo3-' . $this->treeName . '-topPanel-button x-btn-icon" id="typo3-' . $this->treeName . '-topPanel-button-refresh">
                                                                                                            <button class="x-btn-text t3-icon t3-icon-actions t3-icon-actions-system t3-icon-system-refresh">&nbsp;</button>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table cellspacing="0">
                                                                                            <tbody>
                                                                                                <tr class="x-toolbar-extras-row"></tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="x-panel-body x-panel-body-noheader x-panel-body-noborder">
                                                    <div id="typo3-' . $this->treeName . '-topPanel-defaultPanel" class=" x-panel typo3-' . $this->treeName . '-topPanel-item">
                                                        <div class="x-panel-bwrap">
                                                            <div class="x-panel-body x-panel-body-noheader"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="typo3-' . $this->treeName . '-treeContainer" class="x-panel x-panel-noborder x-border-panel">
                                <div class="x-panel-bwrap">
                                    <div class="x-panel-body x-panel-body-noheader x-panel-body-noborder">
                                        <div class="x-panel x-panel-noborder x-tree">
                                            <div class="x-panel-bwrap">
                                                <div class="x-panel-body x-panel-body-noheader x-panel-body-noborder">
                                                    <ul class="x-tree-root-ct x-tree-lines">
                                                        <div class="x-tree-root-node">
                                                            <li class="x-tree-node">
                                                                <div unselectable="on" class="x-tree-node-el x-unselectable typo3-' . $this->treeName . '-node-notExpandable x-tree-node-expanded">' .
        '<span class="x-tree-node-indent"></span>' .
        '<img class="x-tree-ec-icon x-tree-elbow-end-minus" src="/typo3/gfx/clear.gif" alt="">' .
        '<span class="t3-icon t3-icon-apps t3-icon-apps-pagetree t3-icon-pagetree-root">&nbsp;</span>' .
        '<a tabindex="1" href="" onclick="return false" class="x-tree-node-anchor" hidefocus="on">' .
        '<span unselectable="on">' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] . '</span>' .
        '</a>' .
        '</div>
                                                                <ul class="x-tree-node-ct">
                                                                    ' . $itemHtml . '
                                                                </ul>
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    }

    /**
     * @return array
     */
    protected function getRootPages()
    {
        return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'pages', 'is_siteroot = 1 AND deleted = 0', '', 'pid asc, sorting asc');
    }
}