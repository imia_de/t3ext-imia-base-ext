<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Property\TypeConverter;

/**
 * @package     imia_base_ext
 * @subpackage  Property\TypeConverter
 * @author      David Frerich <d.frerich@imia.de>
 */
class DateTimeConverter extends \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter
{
    /**
     * @inheritdoc
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = [], \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration = null)
    {
        if (is_string($source)) {
            if ($source) {
                $timestamp = strtotime($source);
            } else {
                return null;
            }
        } elseif (is_int($source)) {
            if ($source) {
                $timestamp = $source;
            } else {
                return null;
            }
        } else {
            if (isset($source['date']) && is_string($source['date'])) {
                if ($source['date']) {
                    $timestamp = strtotime($source['date']);
                } else {
                    return null;
                }
            } elseif (isset($source['date']) && is_int($source['date'])) {
                if ($source['date']) {
                    $timestamp = $source['date'];
                } else {
                    return null;
                }
            } elseif ($this->isDatePartKeysProvided($source)) {
                if ($source['day'] < 1 || $source['month'] < 1 || $source['year'] < 1) {
                    return new \TYPO3\CMS\Extbase\Error\Error('Could not convert the given date parts into a DateTime object because one or more parts were 0.', 1333032779);
                }
                $timestamp = strtotime(sprintf('%d-%d-%d', $source['year'], $source['month'], $source['day']));
            } else {
                throw new \TYPO3\CMS\Extbase\Property\Exception\TypeConverterException('Could not convert the given source into a DateTime object because it was not an array with a valid date as a string', 1308003914);
            }
        }

        if ($timestamp) {
            /** @var \DateTime $date */
            $date = new $targetType();
            $date->setTimestamp($timestamp);

            if (is_array($source)) {
                $this->overrideTimeIfSpecified($date, $source);
            }
        } else {
            return new \TYPO3\CMS\Extbase\Validation\Error('The input "%s" could not be convertet do a Date.', 1307719788, [is_array($source) ? json_encode($source) : $source]);
        }

        return $date;
    }
}