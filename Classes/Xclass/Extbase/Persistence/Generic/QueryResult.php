<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutinos (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic;

use TYPO3\CMS\Extbase\Persistence\Generic\Qom\Statement;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass\Extbase\Persistence\Generic
 * @author      Lars Siemon <l.siemon@imia.de>
 */
class QueryResult extends \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult {

    /**
     * Overwrites the original implementation of Extbase
     *
     * When the query contains a $statement the query is regularly executed and the number of results is counted
     * instead of the original implementation which tries to create a custom COUNT(*) query and delivers wrong results.
     *
     * @return int The number of matching objects
     * @see https://gist.github.com/smichaelsen/2bfdfb872d04a0776f01
     */
    public function count()
    {
        if ($this->numberOfResults === null) {
            if (is_array($this->queryResult)) {
                $this->numberOfResults = count($this->queryResult);
            } else {
                $statement = $this->query->getStatement();
                if ($statement instanceof Statement) {
                    $this->initialize();
                    $this->numberOfResults = count($this->queryResult);
                } else {
                    return parent::count();
                }
            }
        }
        return $this->numberOfResults;
    }
}