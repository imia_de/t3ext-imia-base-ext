<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Storage;

use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Typo3DbBackend extends \TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbBackend
{
    /**
     * @param QueryInterface $query
     * @return QueryBuilder
     */
    public function getQueryBuilder($query)
    {
        $statement = $query->getStatement();
        $queryParser = $this->objectManager->get(Typo3DbQueryParser::class);
        if ($statement instanceof Qom\Statement && $statement->getStatement() instanceof QueryBuilder) {
            $queryBuilder = $statement->getStatement();
        } else {
            $queryBuilder = $queryParser->convertQueryToDoctrineQueryBuilder($query);
        }
        $selectParts = $queryBuilder->getQueryPart('select');
        if ($queryParser->isDistinctQuerySuggested() && !empty($selectParts)) {
            $selectParts[0] = 'DISTINCT ' . $selectParts[0];
            $queryBuilder->selectLiteral(...$selectParts);
        }
        if ($query->getOffset()) {
            $queryBuilder->setFirstResult($query->getOffset());
        }
        if ($query->getLimit()) {
            $queryBuilder->setMaxResults($query->getLimit());
        }

        return $queryBuilder;
    }
}