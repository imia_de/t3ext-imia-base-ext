<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Storage;

use IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\Selector;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Typo3DbQueryParser extends \TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser
{
    /**
     * @inheritdoc
     */
    public function parseQuery(QueryInterface $query)
    {
        $sql = parent::parseQuery($query);

        /** @var Typo3QuerySettings $querySettings */
        $querySettings = $query->getQuerySettings();
        $onlyFields = $querySettings->getOnlyFields();
        if ($onlyFields && is_array($onlyFields) && count($onlyFields) > 0) {
            /** @var Selector $source */
            $source = $query->getSource();
            $className = $source->getNodeTypeName();
            $tableName = $this->dataMapper->getDataMap($className)->getTableName();

            $sql['fields'][$tableName] = $tableName . '.' . implode(',' . $tableName . '.', $querySettings->getOnlyFields());
        }

        return $sql;
    }

    /**
     * @inheritdoc
     */
    protected function getSysLanguageStatement($tableName, $tableAlias, $querySettings)
    {
        $sysLanguageStatement = '';
        if ($querySettings->getLanguageUid() == -1 ) {
            if (is_array($GLOBALS['TCA'][$tableName]['ctrl'])) {
                if (!empty($GLOBALS['TCA'][$tableName]['ctrl']['languageField'])) {
                    $additionalWhereClause = '(' . $tableAlias . '.' . $GLOBALS['TCA'][$tableName]['ctrl']['transOrigPointerField'] . ' = 0 OR ' .
                        $tableAlias . '.' . $GLOBALS['TCA'][$tableName]['ctrl']['transOrigPointerField'] . ' IS NULL)';

                    $sysLanguageStatement = '(' . $additionalWhereClause . ')';
                }
            }
        } else {
            $sysLanguageStatement = parent::getSysLanguageStatement($tableName, $tableAlias, $querySettings);
        }

        return $sysLanguageStatement;
    }
}
