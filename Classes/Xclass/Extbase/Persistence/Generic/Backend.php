<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class Backend extends \TYPO3\CMS\Extbase\Persistence\Generic\Backend
{
    /**
     * Adjustes the common date fields of the given row to the current time
     *
     * @param \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $object
     * @param array &$row
     */
    protected function addCommonDateFieldsToRow(\TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $object, array &$row)
    {
        if (isset($GLOBALS['tstamp_persistence']) && !$GLOBALS['tstamp_persistence']) {
            $dataMap = $this->dataMapper->getDataMap(get_class($object));
            if ($object->_isNew() && $dataMap->getCreationDateColumnName() !== null) {
                $row[$dataMap->getCreationDateColumnName()] = $GLOBALS['EXEC_TIME'];
            }
        } else {
            parent::addCommonDateFieldsToRow($object, $row);
        }
    }

    /**
     * @inheritdoc
     */
    public function getObjectByIdentifier($identifier, $className)
    {
        $object = GeneralUtility::makeInstance($className);
        $className = get_class($object);

        return parent::getObjectByIdentifier($identifier, $className);
    }
}
