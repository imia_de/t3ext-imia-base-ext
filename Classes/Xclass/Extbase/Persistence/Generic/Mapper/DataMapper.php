<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extbase\Persistence\Generic\Mapper;

use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class DataMapper extends \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
{
    /**
     * @inheritdoc
     */
    protected function thawProperties(DomainObjectInterface $object, array $row)
    {
        if (method_exists($object, '_setRow')) {
            $object->_setRow($row);
        }

        if (isset($row['_LOCALIZED_LANGUAGE_UID'])) {
            $object->_setProperty('_localizedLanguageUid', (int)$row['_LOCALIZED_LANGUAGE_UID']);
        }

        parent::thawProperties($object, $row);
    }

    /**
     * @inheritdoc
     */
    public function getType($parentClassName, $propertyName)
    {
        try {
            $type = parent::getType($parentClassName, $propertyName);
        } catch (UnexpectedTypeException $e) {
            throw new UnexpectedTypeException($e->getMessage() . ' (Class: ' . $parentClassName . ', Property: ' . $propertyName . ')', $e->getCode());
        }

        return $type;
    }
}
