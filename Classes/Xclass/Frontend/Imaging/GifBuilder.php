<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Frontend\Imaging;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class GifBuilder extends \TYPO3\CMS\Frontend\Imaging\GifBuilder
{
    /**
     * @inheritdoc
     */
    public function gifBuild()
    {
        $tempPath = $this->tempPath;
        if (php_sapi_name() == 'cli' || TYPO3_MODE != 'FE') {
            $this->tempPath = PATH_site . 'typo3temp/';
        }

        $imageName = parent::gifBuild();
        $this->tempPath = $tempPath;

        return $imageName;
    }
}
