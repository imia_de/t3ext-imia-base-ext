<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Core\DataHandling;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class DataHandler extends \TYPO3\CMS\Core\DataHandling\DataHandler
{
    use HookTrait;

    /**
     * @var array
     */
    protected $deletedRecords = [];

    /**
     * @inheritdoc
     */
    public function recordInfo($table, $id, $fieldList)
    {
        $result = null;
        $this->callHook('recordInfo', [&$result, $table, $id, &$fieldList, &$this]);

        if ($result === null) {
            $result = parent::recordInfo($table, $id, $fieldList);
        }

        return $result ?: null;
    }

    /**
     * @inheritdoc
     */
    public function insertUpdateDB_preprocessBasedOnFieldType($table, $fieldArray)
    {
        $fieldArray = parent::insertUpdateDB_preprocessBasedOnFieldType($table, $fieldArray);
        $this->callHook('insertUpdateDB_preprocessBasedOnFieldType', [&$fieldArray, $table, &$this]);

        return $fieldArray;
    }

    /**
     * @inheritdoc
     */
    public function copySpecificPage($uid, $destPid, $copyTablesArray, $first = false)
    {
        $copyTablesArrayWithoutExcludes = [];
        foreach ($copyTablesArray as $table) {
            if (!isset($GLOBALS['TCA'][$table]['ctrl']['excludeOnPageCopy']) || !$GLOBALS['TCA'][$table]['ctrl']['excludeOnPageCopy']) {
                $copyTablesArrayWithoutExcludes[] = $table;
            }
        }

        return parent::copySpecificPage($uid, $destPid, $copyTablesArrayWithoutExcludes, $first);
    }

    /**
     * @inheritdoc
     */
    protected function copyRecord_processInline($table, $uid, $field, $value, $row, $conf, $realDestPid, $language,
                                                array $workspaceOptions, $localizationMode, $inlineSubType)
    {
        if (isset($GLOBALS['TCA'][$table]['columns'][$field]['config'])) {
            $fieldConfig = $GLOBALS['TCA'][$table]['columns'][$field]['config'];
            if ($fieldConfig['allowed'] == 'pages' || $fieldConfig['foreign_table'] == 'pages') {
                return null;
            }
        }

        return parent::copyRecord_processInline($table, $uid, $field, $value, $row, $conf, $realDestPid, $language,
            $workspaceOptions, $localizationMode, $inlineSubType);
    }

    /**
     * @inheritdoc
     */
    public function deleteRecord($table, $uid, $noRecordCheck = false, $forceHardDelete = false, $undeleteRecord = false)
    {
        if ($table == 'tt_content' && $uid) {
            $record = BackendUtility::getRecord($table, $uid);
            if ($record && $record['sorting'] < 10000000) {
                $this->getDb()->exec_UPDATEquery($table, 'uid = ' . $uid, [
                    'deleted_sorting' => $record['sorting'],
                ]);
            }
        }

        parent::deleteRecord($table, $uid, $noRecordCheck, $forceHardDelete, $undeleteRecord);
    }

    /**
     * @inheritdoc
     */
    public function deleteRecord_procBasedOnFieldType($table, $uid, $field, $value, $conf, $undeleteRecord = false)
    {
        if ($table != 'tt_content' || $field != 'tx_flux_children' || $value !== null) {
            if ($conf['type'] == 'inline') {
                $foreign_table = $conf['foreign_table'];
                if ($foreign_table) {
                    $inlineType = $this->getInlineFieldType($conf);
                    if ($inlineType == 'list' || $inlineType == 'field') {
                        $dbAnalysis = $this->createRelationHandlerInstance();
                        $dbAnalysis->undeleteRecord = true; /** @BUGFIX This needs to be placed BEFORE ->start() */
                        $dbAnalysis->start($value, $conf['foreign_table'], '', $uid, $table, $conf);

                        $enableCascadingDelete = true;
                        // non type save comparison is intended!
                        if (isset($conf['behaviour']['enableCascadingDelete']) && $conf['behaviour']['enableCascadingDelete'] == false) {
                            $enableCascadingDelete = false;
                        }

                        // Walk through the items and remove them
                        foreach ($dbAnalysis->itemArray as $v) {
                            if (!$undeleteRecord) {
                                if ($enableCascadingDelete) {
                                    $this->deleteAction($v['table'], $v['id']);
                                }
                            } else {
                                $this->undeleteRecord($v['table'], $v['id']);
                            }
                        }
                    }
                }
            } elseif ($this->isReferenceField($conf)) {
                $allowedTables = $conf['type'] == 'group' ? $conf['allowed'] : $conf['foreign_table'];
                $dbAnalysis = $this->createRelationHandlerInstance();
                $dbAnalysis->start($value, $allowedTables, $conf['MM'], $uid, $table, $conf);
                foreach ($dbAnalysis->itemArray as $v) {
                    $this->updateRefIndexStack[$table][$uid][] = [$v['table'], $v['id']];
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function moveRecord($table, $uid, $destPid)
    {
        if (in_array($table, ['tx_imiastatistics_pages', 'tx_imiabaseext_cacheitem'])) {
            return;
        }

        parent::moveRecord($table, $uid, $destPid);
    }

    /**
     * @inheritdoc
     */
    public function copyRecord($table, $uid, $destPid, $first = false, $overrideValues = [], $excludeFields = '', $language = 0, $ignoreLocalization = false)
    {
        if (GeneralUtility::_GP('overrideVals') && isset(GeneralUtility::_GP('overrideVals')[$table]) && is_array(GeneralUtility::_GP('overrideVals')[$table])) {
            $overrideValues = array_merge($overrideValues, GeneralUtility::_GP('overrideVals')[$table]);
        }

        $this->callHook('copyRecord_preProcess', [$table, $uid, &$this]);
        $theNewSQLID = parent::copyRecord($table, $uid, $destPid, $first, $overrideValues, $excludeFields, $language, $ignoreLocalization);
        $this->callHook('copyRecord_postProcess', [$table, $uid, $theNewSQLID, &$this]);

        return $theNewSQLID;
    }

    /**
     * @return array
     */
    public function getDeletedRecords()
    {
        return $this->deletedRecords;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
