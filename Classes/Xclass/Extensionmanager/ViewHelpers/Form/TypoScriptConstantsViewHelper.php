<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extensionmanager\ViewHelpers\Form;

use TYPO3\CMS\Extensionmanager\Domain\Model\ConfigurationItem;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptConstantsViewHelper extends \TYPO3\CMS\Extensionmanager\ViewHelpers\Form\TypoScriptConstantsViewHelper
{
    /**
     * Render
     *
     * @return string the rendered tag
     */
    public function render()
    {
        $this->viewHelperMapping['options_multiple'] = 'renderMultipleCheckbox';
        $this->viewHelperMapping['text'] = 'renderTextArea';

        return parent::render();
    }

    /**
     * Render field of type "text"
     *
     * @param ConfigurationItem $configuration
     * @return string
     */
    protected function renderTextArea(ConfigurationItem $configuration)
    {
        $this->tag->setTagName('textarea');
        $this->tag->forceClosingTag(true);
        $this->tag->addAttribute('id', 'em-' . $configuration->getName());
        $this->tag->addAttribute('name', $this->getName($configuration));
        $this->tag->addAttribute('class', 'form-control');
        $this->tag->addAttribute('rows', '8');
        $this->tag->addAttribute('cols', '40');
        if ($configuration->getValue() !== null) {
            $this->tag->setContent($configuration->getValue());
        }

        return $this->tag->render();
    }

    /**
     * Render field of type "options_multiple"
     *
     * @param ConfigurationItem $configuration
     * @return string
     */
    protected function renderMultipleCheckbox(ConfigurationItem $configuration)
    {
        $this->tag->addAttribute('id', 'em-' . $configuration->getName());
        $this->tag->setTagName('div');

        $output = '';
        $checkboxCount = 0;
        $output .= '<input type="hidden" value="" name="' . $this->getName($configuration) . '" />';
        foreach ($configuration->getGeneric() as $label => $value) {
            if ($value) {
                $checkboxId = 'em-' . $configuration->getName() . '-' . ++$checkboxCount;
                $output .= '<div><input id="' . $checkboxId . '" name="' . $this->getName($configuration) . '[]"
                    type="checkbox" value="' . htmlspecialchars($value) . '" style="margin-bottom: 0"';

                if (in_array($value, explode(',', $configuration->getValue()))) {
                    $output .= ' checked="checked"';
                }

                $output .= '> <label for="' . $checkboxId . '" style="padding-bottom: 0; min-height: 20px; ' .
                    'line-height: 20px; display: inline-block">' . $GLOBALS['LANG']->sL($label, true) . '</label></div>';
            }
        }
        $this->tag->setContent($output);

        return $this->tag->render();
    }
}
