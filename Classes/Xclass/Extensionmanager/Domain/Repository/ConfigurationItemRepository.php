<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extensionmanager\Domain\Repository;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConfigurationItemRepository extends \TYPO3\CMS\Extensionmanager\Domain\Repository\ConfigurationItemRepository
{
    /**
     * @inheritdoc
     */
    protected function extractInformationForConfigFieldsOfTypeOptions(array $configurationOption)
    {
        $multiple = false;
        if (preg_match('/options\[(.*)\]multiple/is', $configurationOption['type'])) {
            $multiple = true;
        }

        $configurationOption = parent::extractInformationForConfigFieldsOfTypeOptions($configurationOption);

        if ($multiple) {
            $configurationOption['type'] = 'options_multiple';
            $configurationOption['generic'][''] = '';
        }

        return $configurationOption;
    }
}
