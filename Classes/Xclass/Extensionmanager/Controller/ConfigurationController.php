<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extensionmanager\Controller;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConfigurationController extends \TYPO3\CMS\Extensionmanager\Controller\ConfigurationController
{
    /**
     * @inheritdoc
     */
    protected function saveConfiguration(array $config, $extensionKey)
    {
        foreach ($config as $field => $value) {
            if ($value && $value['value']) {
                if (is_array($value['value'])) {
                    $config[$field]['value'] = implode(',', $value['value']);
                }
            }
        }

        parent::saveConfiguration($config, $extensionKey);
    }
}
