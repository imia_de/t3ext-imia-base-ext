<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Extensionmanager\Utility;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConfigurationUtility extends \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility
{
    /**
     * Return content of an extensions ext_conf_template.txt file if
     * the file exists, empty string if file does not exist.
     *
     * @param string $extensionKey Extension key
     * @return string
     */
    protected function getDefaultConfigurationRawString($extensionKey)
    {
        $rawString = parent::getDefaultConfigurationRawString($extensionKey);
        if ($rawString) {
            preg_match_all('/LLL:EXT:[^:]+:[^\/ \n\r:]+/ism', $rawString, $matches);

            $replaces = [];
            if ($matches && count($matches) && count($matches[0])) {
                foreach ($matches[0] as $match) {
                    $translation = $this->translate($match, $extensionKey);
                    if ($translation) {
                        $replaces[$match] = str_replace(["\r\n", "\n", "\r"], '', $translation);
                    }
                }
            }

            if (count($replaces) > 0) {
                krsort($replaces);
                foreach ($replaces as $search => $replace) {
                    $rawString = str_replace($search, $replace, $rawString);
                }
            }
        }

        return $rawString;
    }

    /**
     * Returns the localized label of the LOCAL_LANG key, $key.
     * Wrapper for the static call.
     *
     * @param string $key The key from the LOCAL_LANG array for which to return the value.
     * @param string $extensionName The name of the extension
     * @return string|NULL The value from LOCAL_LANG or NULL if no translation was found.
     */
    protected function translate($key, $extensionName)
    {
        return LocalizationUtility::translate($key, $extensionName);
    }
}
