<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Form\ViewHelpers;

use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Form\Domain\Model\Renderable\RootRenderableInterface;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Service\TranslationService;
use TYPO3\CMS\Form\ViewHelpers\RenderRenderableViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TranslateElementErrorViewHelper extends \TYPO3\CMS\Form\ViewHelpers\TranslateElementErrorViewHelper
{
    /**
     * Initialize arguments.
     *
     * @internal
     */
    public function initializeArguments()
    {
        $this->registerArgument('element', RootRenderableInterface::class, 'Form Element to translate', true);
        $this->registerArgument('error', Error::class, '', false, '');
        $this->registerArgument('code', 'integer', 'Error code - deprecated', false);
        $this->registerArgument('arguments', 'array', 'Error arguments - deprecated', false, null);
        $this->registerArgument('defaultValue', 'string', 'The default value - deprecated', false, '');
    }

    /**
     * @inheritdoc
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $element = $arguments['element'];
        $error = $arguments['error'];

        $code = $arguments['code'];
        $errorArguments = $arguments['arguments'];
        $defaultValue = $arguments['defaultValue'];

        if ($error instanceof Error) {
            $code = $error->getCode();
            $errorArguments = $error->getArguments();
            $defaultValue = $error->__toString();
        }

        /** @var FormRuntime $formRuntime */
        $formRuntime = $renderingContext
            ->getViewHelperVariableContainer()
            ->get(RenderRenderableViewHelper::class, 'formRuntime');

        return TranslationService::getInstance()->translateFormElementError(
            $element,
            $code,
            $errorArguments,
            $defaultValue,
            $formRuntime
        );
    }
}
