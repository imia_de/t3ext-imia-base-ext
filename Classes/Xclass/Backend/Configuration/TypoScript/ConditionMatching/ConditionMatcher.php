<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Backend\Configuration\TypoScript\ConditionMatching;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ConditionMatcher extends \TYPO3\CMS\Backend\Configuration\TypoScript\ConditionMatching\ConditionMatcher
{
    /**
     * @inheritdoc
     */
    protected function getPage()
    {
        $page = parent::getPage();

        $editconf = GeneralUtility::_GP('edit');
        if ($editconf) {
            $new = false;
            foreach ($editconf as $tableName => $tableCmds) {
                if ($tableName == 'pages') {
                    foreach ($tableCmds as $key => $cmd) {
                        if ($cmd == 'new') {
                            $new = true;
                            break;
                        }
                    }
                }
            }

            if ($new) {
                $defVals = GeneralUtility::_GP('defVals');
                if ($defVals && is_array($defVals) && isset($defVals['pages']) && is_array($defVals['pages'])) {
                    foreach ($defVals['pages'] as $field => $value) {
                        $page[$field] = $value;
                    }
                }
                $overrideVals = GeneralUtility::_GP('overrideVals');
                if ($overrideVals && is_array($overrideVals) && isset($overrideVals['pages']) && is_array($overrideVals['pages'])) {
                    foreach ($overrideVals['pages'] as $field => $value) {
                        $page[$field] = $value;
                    }
                }
            }
        }

        return $page;
    }
}
