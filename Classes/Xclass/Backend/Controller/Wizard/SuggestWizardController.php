<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Backend\Controller\Wizard;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass\Backend
 * @author      David Frerich <d.frerich@imia.de>
 */
class SuggestWizardController extends \TYPO3\CMS\Backend\Controller\Wizard\SuggestWizardController
{
    use HookTrait;

    protected $parsedBody;

    /**
     * @inheritdoc
     */
    public function searchAction(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->parsedBody = $request->getParsedBody();

        return parent::searchAction($request, $response);
    }

    /**
     * @inheritdoc
     */
    protected function getTablesToQueryFromFieldConfiguration(&$fieldConfig)
    {
        $row = BackendUtility::getRecord($this->parsedBody['tableName'], $this->parsedBody['uid']);
        $this->callHook('overrideConfiguration', [$this->parsedBody['tableName'], $this->parsedBody['fieldName'], $row, &$fieldConfig, &$this]);

        if (!$fieldConfig) {
            $fieldConfig = [];
        }

        return parent::getTablesToQueryFromFieldConfiguration($fieldConfig);
    }

    /**
     * @inheritdoc
     */
    protected function isTableHidden(array $tableConfig)
    {
        return false;
    }
}
