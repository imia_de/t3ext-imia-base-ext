<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataProvider;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class EvaluateDisplayConditions extends \TYPO3\CMS\Backend\Form\FormDataProvider\EvaluateDisplayConditions
{
    /**
     * @var string
     */
    protected $recordTable;

    /**
     * @inheritdoc
     */
    public function addData(array $result): array
    {
        $this->recordTable = $result['tableName'];

        if (isset($_REQUEST['recordTypeValue']) && $_REQUEST['recordTypeValue']) {
            if (isset($_REQUEST['command']) && isset($_REQUEST['vanillaUid']) && isset($_REQUEST['tableName'])) {
                if ($_REQUEST['command'] == $result['command'] && $_REQUEST['vanillaUid'] == $result['vanillaUid'] && $_REQUEST['tableName'] == $result['tableName']) {
                    $result['databaseRow']['CType'] = $_REQUEST['recordTypeValue'];
                }
            }
        }

        return parent::addData($result);
    }

    /**
     * @inheritdoc
     */
    protected function findFieldValue(string $givenFieldName, array $databaseRow, array $flexContext = [])
    {
        if (strpos($givenFieldName, '.') !== false && !isset($flexContext['context'])) {
            $fieldValue = null;

            $fieldNameParts = explode('.', $givenFieldName, 2);
            $parentFieldName = $fieldNameParts[0];
            $subFieldName = $fieldNameParts[1];

            if (array_key_exists($parentFieldName, $databaseRow) && $databaseRow[$parentFieldName]) {
                $fieldTCA = $GLOBALS['TCA'][$this->recordTable]['columns'][$parentFieldName];
                $recordFieldValue = $databaseRow[$parentFieldName];
                if (is_array($recordFieldValue)) {
                    $newRecordFieldValues = [];
                    foreach ($recordFieldValue as $arrayValue) {
                        if (is_array($arrayValue) && isset($arrayValue['uid'])) {
                            $newRecordFieldValues[] = ($fieldTCA['config']['type'] == 'group' ? $arrayValue['table'] . '_' : '') . $arrayValue['uid'];
                        }
                    }

                    $recordFieldValue = implode(',', $newRecordFieldValues);
                } elseif (!is_numeric($recordFieldValue)) {
                    $arrValue = explode('|', $recordFieldValue);
                    $recordFieldValue = (int)preg_replace('/^.*?_([0-9]+)$/ism', '$1', $arrValue[0]);
                }

                if ($fieldTCA || $parentFieldName == 'pid') {
                    $subRecordTable = '';
                    $subRecordUid = 0;

                    if ($parentFieldName == 'pid') {
                        $subRecordTable = 'pages';
                        $subRecordUid = (int)$recordFieldValue;
                    } else {
                        switch ($fieldTCA['config']['type']) {
                            case 'select':
                            case 'inline':
                                if ($fieldTCA['config']['foreign_table'] && !$fieldTCA['config']['MM']) {
                                    $recordFieldValues = explode(',', $recordFieldValue);
                                    $subRecordTable = trim($fieldTCA['config']['foreign_table']);
                                    $subRecordUid = (int)array_shift($recordFieldValues);
                                }
                                break;
                            case 'group':
                                if ($fieldTCA['config']['internal_type'] == 'db' && $fieldTCA['config']['allowed']) {
                                    $recordFieldValues = explode(',', $recordFieldValue);
                                    $allowedTables = array_map('trim', explode(',', $fieldTCA['config']['allowed']));

                                    if (count($allowedTables) === 1 || is_numeric($recordFieldValues)) {
                                        $subRecordTable = array_shift($allowedTables);
                                    } else {
                                        foreach ($allowedTables as $table) {
                                            if (strpos($recordFieldValues[0], $table) === 0) {
                                                $subRecordTable = $table;
                                                break;
                                            }
                                        }
                                    }

                                    $subRecordUid = (int)array_shift(explode('|', str_replace($subRecordTable . '_', '', $recordFieldValue)));
                                }
                                break;
                        }
                    }

                    if ($subRecordTable && $subRecordUid) {
                        $res = $this->getDb()->exec_SELECTquery('*', $subRecordTable, 'uid=' . $subRecordUid . BackendUtility::deleteClause($subRecordTable));
                        $subRecord = $this->getDb()->sql_fetch_assoc($res);
                        $this->getDb()->sql_free_result($res);

                        if ($subRecord) {
                            BackendUtility::fixVersioningPid($subRecordTable, $subRecord);
                            $fieldValue = $this->findFieldValue($subFieldName, $subRecord);
                        }
                    }
                }
            }
        } else {
            $fieldValue = parent::findFieldValue($givenFieldName, $databaseRow, $flexContext);
        }

        return $fieldValue;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}