<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Backend\Form\FormDataProvider;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TcaRecordTitle extends \TYPO3\CMS\Backend\Form\FormDataProvider\TcaRecordTitle
{
    /**
     * @inheritdoc
     */
    protected function getRecordTitleForGroupType($value, $fieldConfig)
    {
        $label = null;
        if ($fieldConfig['internal_type'] === 'db') {
            if (!is_array($value)) {
                $value = [];
            }

            $labels = [];
            foreach ($value as $val) {
                $valueParts = GeneralUtility::trimExplode('|', $val);

                if (preg_match('/^(.*?)_([0-9]+)$/ism', $valueParts[0], $match)) {
                    $recordTable = $match[1];
                    $recordUid = (int)$match[2];
                    $recordTcaLabelField = $GLOBALS['TCA'][$recordTable]['ctrl']['label'];

                    if ($recordTcaLabelField) {
                        $record = BackendUtility::getRecord($recordTable, $recordUid, $recordTcaLabelField);
                        $labels[] = $record[$recordTcaLabelField];
                    }
                }
            }

            if (count($labels) > 0) {
                $label = implode(', ', $labels);
            }
        }

        if (!$label) {
            $label = parent::getRecordTitleForGroupType($value, $fieldConfig);
        }

        return $label;
    }
}