<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Backend\Form\Element;

use IMIA\ImiaBaseExt\User\TCA\Wizard\ImagePositionWizard;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass\Backend
 * @author      David Frerich <d.frerich@imia.de>
 */
class InputTextElement extends \TYPO3\CMS\Backend\Form\Element\InputTextElement
{
    /**
     * @inheritdoc
     */
    protected function renderWizards($itemKinds = null, $wizConf = null, $table = null, $row = null, $fieldName = null,
                                     $PA = null, $itemName = null, $specConf = null, $RTE = null)
    {
        if ($wizConf == null && isset($this->data['parameterArray']['fieldConf']['config']['wizards'])) {
            $wizConf = &$this->data['parameterArray']['fieldConf']['config']['wizards'];
        }

        if (is_array($wizConf)) {
            foreach ($wizConf as $wizardIdentifier => &$wizardConfiguration) {
                if (isset($wizardConfiguration['type'])) {
                    switch ($wizardConfiguration['type']) {
                        case 'imageposition':
                            $wizardConfiguration['type'] = 'userFunc';
                            $wizardConfiguration['userFunc'] = ImagePositionWizard::class . '->renderWizard';
                            $wizardConfiguration['params'] = [
                                'field'   => $wizardConfiguration['field'],
                                'measure' => $wizardConfiguration['measure'],
                            ];
                            break;
                    }
                }
            }
        }

        return parent::renderWizards($itemKinds, $wizConf, $table, $row, $fieldName, $PA, $itemName, $specConf, $RTE);
    }
}
