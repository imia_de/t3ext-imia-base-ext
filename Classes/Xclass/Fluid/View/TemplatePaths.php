<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Xclass\Fluid\View;

/**
 * @package     imia_base_ext
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TemplatePaths extends \TYPO3\CMS\Fluid\View\TemplatePaths
{
    /**
     * @inheritdoc
     */
    public function fillDefaultsByPackageName($packageName)
    {
        $configuration = $this->getContextSpecificViewConfiguration($packageName);
        if ($this->getTemplateRootPaths() && count($this->getTemplateRootPaths())) {
            $templateRootPaths = $this->getTemplateRootPaths();
            foreach ($configuration['templateRootPaths'] as $templateRootPath) {
                $templateRootPaths[] = $this->ensureAbsolutePath($templateRootPath);
            }
            $configuration['templateRootPaths'] = array_unique($templateRootPaths);
        }
        if ($this->getLayoutRootPaths() && count($this->getLayoutRootPaths())) {
            $layoutRootPaths = $this->getLayoutRootPaths();
            foreach ($configuration['layoutRootPaths'] as $layoutRootPath) {
                $layoutRootPaths[] = $this->ensureAbsolutePath($layoutRootPath);
            }
            $configuration['layoutRootPaths'] = array_unique($layoutRootPaths);
        }
        if ($this->getPartialRootPaths() && count($this->getPartialRootPaths())) {
            $partialRootPaths = $this->getPartialRootPaths();
            foreach ($configuration['partialRootPaths'] as $partialRootPath) {
                $partialRootPaths[] = $this->ensureAbsolutePath($partialRootPath);
            }
            $configuration['partialRootPaths'] = array_unique($partialRootPaths);
        }

        $this->fillFromConfigurationArray($configuration);
    }
}
