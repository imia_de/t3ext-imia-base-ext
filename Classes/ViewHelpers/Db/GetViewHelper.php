<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Db;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class GetViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    static protected $results = [];

    /**
     * @param integer $uid
     * @param array|string $uids
     * @param string $table
     * @param string $fields
     * @param string $where
     * @param string $sorting
     * @param boolean $first
     * @param string $field
     * @param boolean $forceArray
     * @return array
     */
    public function render($uid = null, $uids = null, $table = 'tt_content', $fields = '*', $where = '', $sorting = '',
                           $first = false, $field = null, $forceArray = false)
    {
        if (!is_array($uids)) {
            if (is_string($uids) && $uids) {
                $uids = explode(',', $uids);
            } else {
                $uids = [];
            }
        }

        if ($uid) {
            $uids[] = $uid;
        } else {
            $uid = $this->renderChildren();
            if ($uid) {
                if (is_array($uid)) {
                    $uids = $uid;
                    $uid = null;
                } else {
                    $uids = array_map('intval', explode(',', $uid));
                    if (count($uids) == 1) {
                        $uid = (int)$uid;
                    } else {
                        $uid = null;
                    }
                }
            }
        }

        if ($fields == '*' && $field) {
            $fields = $field;
        }

        if (count($uids) == 0 && !$where) {
            if ($forceArray) {
                return [];
            } else {
                return null;
            }
        } else {
            $uids = array_map('intval', $uids);

            if ($fields != '*' && !strstr($fields, 'uid')) {
                $fields .= ',uid';
            }

            $key = md5(implode('-', $uids) . '_' . $table . '_' . $fields . '_' . $where . '_' . $sorting);
            if (!array_key_exists($key, self::$results)) {
                $results = $this->getDb()->exec_SELECTgetRows(
                    $fields,
                    $table,
                    '1 = 1' . (count($uids) ? ' AND uid IN (' . implode(',', $uids) . ')' : '') . ($where ? ' AND ' . $where : '') . $this->enableFields($table),
                    '',
                    $sorting
                );

                self::$results[$key] = [];
                foreach ($results as $result) {
                    self::$results[$key][$result['uid']] = $result;
                }

                if (!$sorting && $uids) {
                    $sortedResults = [];
                    foreach ($uids as $sortingUid) {
                        if (array_key_exists($sortingUid, self::$results[$key])) {
                            $sortedResults[$sortingUid] = self::$results[$key][$sortingUid];
                        }
                    }

                    self::$results[$key] = $sortedResults;
                }

                foreach (self::$results[$key] as $resultKey => $result) {
                    if ($table == 'pages') {
                        $result['sys_language_uid'] = 0;
                    }

                    if ($result['uid'] && array_key_exists('sys_language_uid', $result)) {
                        $this->recordOverlay(self::$results[$key][$resultKey], $table);
                    }
                }
            }

            if ($first || $uid) {
                $results = self::$results[$key];
                $result = array_shift($results);

                if ($field && is_array($result) && array_key_exists($field, $result)) {
                    return $result[$field];
                } elseif ($forceArray) {
                    return [$result];
                } else {
                    return $result;
                }
            } else {
                return self::$results[$key];
            }
        }
    }

    /**
     * @param array $record
     * @param string $table
     */
    protected function recordOverlay(&$record, $table)
    {
        if ($this->getTSFE() && $this->getTSFE()->sys_language_uid > 0) {
            if ($table == 'pages') {
                $record = $this->getTSFE()->sys_page->getPageOverlay(
                    $record
                );
            } elseif ($table != 'pages_language_overlay') {
                $record = $this->getTSFE()->sys_page->getRecordOverlay(
                    $table,
                    $record,
                    $this->getTSFE()->sys_language_uid,
                    $this->getTSFE()->config['config']['sys_language_overlay']
                );
            }
        }
    }

    /**
     * @param string $table
     * @return string
     */
    protected function enableFields($table)
    {
        if ($this->getTSFE() && $this->getTSFE()->cObj) {
            $enableFields = $this->getTSFE()->cObj->enableFields($table);
        } else {
            $enableFields = BackendUtility::BEenableFields($table) . BackendUtility::deleteClause($table);
        }

        return $enableFields;
    }

    /**
     * @return TyposcriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}