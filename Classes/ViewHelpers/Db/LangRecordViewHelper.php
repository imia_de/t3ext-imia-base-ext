<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Db;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class LangRecordViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    protected static $langRecord = [];

    /**
     * @param array $record
     * @return array
     */
    public function render($record = null)
    {
        if (!$record) {
            $record = $this->renderChildren();
        }

        $key = md5(serialize($record));
        if (!array_key_exists($key, self::$langRecord)) {
            $record['table'] = 'pages';
            $language = null;
            if ($GLOBALS['SOBE']) {
                $modSettings = $GLOBALS['SOBE']->MOD_SETTINGS;
                $language = (int)$modSettings['language'];
            } elseif ($GLOBALS['TSFE']) {
                $language = (int)$GLOBALS['TSFE']->sys_language_uid;
            }

            if ($language > 0 && $record['uid']) {
                $langRecord = $this->getDb()->exec_SELECTgetSingleRow(
                    '*', 'pages_language_overlay',
                    'pid = ' . $record['uid'] . ' AND sys_language_uid = ' . $language . ' AND deleted = 0 ');

                if ($langRecord) {
                    $record = $langRecord;
                    $record['table'] = 'pages_language_overlay';
                }
            }

            self::$langRecord[$key] = $record;
        }

        return self::$langRecord[$key];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}