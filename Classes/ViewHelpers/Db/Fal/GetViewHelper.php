<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Db\Fal;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class GetViewHelper extends AbstractViewHelper
{
    /**
     * @var array
     */
    static protected $results = [];

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     */
    protected $resourceFactory;

    public function __construct()
    {
        $this->resourceFactory = GeneralUtility::makeInstance('TYPO3\CMS\\Core\Resource\ResourceFactory');
    }

    /**
     * @param string $field
     * @param string $table
     * @param integer $uid
     * @param array|object $record
     * @param boolean $objects
     * @param boolean $first
     * @param boolean $contents
     * @return mixed
     */
    public function render($field, $table = 'tt_content', $uid = null, $record = null, $objects = true, $first = false, $contents = false)
    {
        if (!$uid) {
            if (!$record) {
                $record = $this->renderChildren();
            }

            if (is_array($record)) {
                $uid = $record['uid'];
            } elseif (is_object($record) && method_exists('getUid', $record)) {
                $uid = $record->getUid();
            } else {
                $uid = $record;
            }
        }

        $key = md5($uid . '_' . $table . '_' . $field);
        if (!array_key_exists($key, self::$results)) {
            $res = $this->getDb()->exec_SELECTquery('uid', 'sys_file_reference', 'deleted = 0 AND hidden = 0' .
                ' AND tablenames = ' . $this->getDb()->fullQuoteStr($table, 'sys_file_reference') .
                ' AND fieldname = ' . $this->getDb()->fullQuoteStr($field, 'sys_file_reference') .
                ' AND uid_foreign = ' . (int)$uid, '', 'sorting_foreign');

            self::$results[$key] = [];
            while ($row = $this->getDb()->sql_fetch_assoc($res)) {
                self::$results[$key][] = $row['uid'];
            }
        }

        if ($objects) {
            $resources = [];
            foreach (self::$results[$key] as $uid) {
                try {
                    $resources[] = $this->getResource($uid, $contents);
                } catch (\Exception $e) {
                }
            }

            $results = $resources;
        } else {
            $results = self::$results[$key];
        }

        if ($first) {
            return array_shift($results);
        } else {
            return $results;
        }
    }

    /**
     * @param integer $identity
     * @param boolean $contents
     * @return array
     */
    protected function getResource($identity, $contents = false)
    {
        $fileReference = $this->resourceFactory->getFileReferenceObject(intval($identity));
        $file = $fileReference->getOriginalFile();
        $fileReferenceProperties = $fileReference->getProperties();
        $fileProperties = $this->getFileArray($file);

        ArrayUtility::mergeRecursiveWithOverrule($fileProperties, $fileReferenceProperties, true, false, false);

        switch ($fileProperties['extension']) {
            case 'youtube':
                $fileProperties['contents'] = $fileReference->getContents();
                try {
                    /** @var \TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\YouTubeHelper $helper */
                    $helper = $this->objectManager
                        ->get('TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\YouTubeHelper', $fileProperties['extension']);

                    $fileProperties['previewImage'] = $helper->getPreviewImage($file);
                } catch (\Exception $e) {
                    exit;
                }
                break;
            case 'vimeo':
                $fileProperties['contents'] = $fileReference->getContents();

                try {
                    /** @var \TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\VimeoHelper $helper */
                    $helper = $this->objectManager
                        ->get('TYPO3\CMS\Core\Resource\OnlineMedia\Helpers\VimeoHelper', $fileProperties['extension']);

                    $fileProperties['previewImage'] = $helper->getPreviewImage($file);
                } catch (\Exception $e) {
                }
                break;
            default:
                $fileProperties['contents'] = null;
                break;
        }

        if ($contents && !$fileProperties['contents']) {
            $fileProperties['contents'] = $fileReference->getContents();
        }
        if ($fileProperties['previewImage']) {
            $fileProperties['previewImage'] = str_replace(PATH_site, '/', $fileProperties['previewImage']);
        }

        $fileProperties['file'] = $file;

        return $fileProperties;
    }

    /**
     * Fixes a bug in TYPO3 6.2.0 that the properties metadata is not overlayed on localization.
     *
     * @param File $file
     * @return array
     */
    protected function getFileArray(File $file)
    {
        $properties = $file->getProperties();
        $stat = $file->getStorage()->getFileInfo($file);
        $array = $file->toArray();

        foreach ($properties as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($stat as $key => $value) {
            $array[$key] = $value;
        }

        return $array;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}