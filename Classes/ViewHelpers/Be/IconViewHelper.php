<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Be;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class IconViewHelper extends AbstractViewHelper
{
    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param string $table
     * @param array $row
     * @param object $record
     * @param boolean $clickmenu
     * @return string
     */
    public function render($table = null, $row = null, $record = null, $clickmenu = true)
    {
        if (!$record) {
            $record = $this->renderChildren();
        }
        if (!$table && is_object($record) && method_exists($record, 'getTable')) {
            $table = $record->getTable();
        }
        if (!$row && is_object($record) && method_exists($record, 'getRow')) {
            $row = $record->getRow(false);
        }
        if (is_object($record) && method_exists($record, 'getLocalizedUid')) {
            $row['uid'] = $record->getLocalizedUid();
        }

        $icon = '';
        if ($row && is_array($row) && $table) {
            $icon = $this->getIconFactory()->getIconForRecord($table, $row, Icon::SIZE_SMALL);

            if ($clickmenu && $row['uid']) {
                $icon = BackendUtility::wrapClickMenuOnIcon($icon, $table, $row['uid'], 1, '', '');
            }
        }

        return $icon;
    }

    /**
     * @return IconFactory
     */
    protected function getIconFactory()
    {
        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);

        return $iconFactory;
    }
}