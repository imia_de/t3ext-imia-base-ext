<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Be;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class PermissionViewHelper extends AbstractViewHelper
{
    /**
     * @param string $type
     * @param string|int $value
     * @return mixed
     */
    public function render($type = 'tables_select', $value = null)
    {
        if ($value === null) {
            $value = $this->renderChildren();
        }

        if ($type == 'tsconfig') {
            return $this->getBackendUserAuthentification()->getTSConfigVal($value);
        } elseif ($type == 'tsconfig_properties') {
            return $this->getBackendUserAuthentification()->getTSConfigProp($value);
        } elseif ($type == 'admin') {
            return $this->getBackendUserAuthentification()->isAdmin();
        } elseif ($type == 'language') {
            return $this->getBackendUserAuthentification()->checkLanguageAccess($value);
        } elseif ($type && $value) {
            return $this->getBackendUserAuthentification()->check($type, $value);
        } else {
            return null;
        }
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentification()
    {
        return $GLOBALS['BE_USER'];
    }
}