<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers\Be\Clipboard;

use TYPO3\CMS\Backend\Clipboard\Clipboard;
use TYPO3\CMS\Backend\Routing\UriBuilder;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class GetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $table
     * @param integer $uid
     * @param string $field
     * @return array|null|string
     */
    public function render($table = null, $uid = null, $field = null)
    {
        if (TYPO3_MODE != 'BE') {
            return '';
        }

        if (!$uid) {
            $uid = (int)$this->renderChildren();

            if (!$uid) {
                $uid = null;
            }
        }

        $record = $this->getClipboard()->getSelectedRecord($table, $uid);
        if ($record) {
            $table = preg_replace('/[|][0-9]+$/ism', '', array_keys($this->getClipboard()->elFromTable()));
            if (is_array($table)) {
                $table = array_pop($table);
            }
            $record['_TABLE'] = $table;

            if ($field) {
                if (array_key_exists($field, $record)) {
                    return $record[$field];
                } else {
                    return '';
                }
            } else {
                return $record;
            }
        } else {
            return null;
        }
    }

    /**
     * @return Clipboard
     */
    protected function getClipboard()
    {
        $clipboard = $this->objectManager->get(Clipboard::class);
        $clipboard->initializeClipboard();

        return $clipboard;
    }
}