<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers;

use IMIA\ImiaBaseExt\Utility\BackendTypoScript;
use TYPO3\CMS\Backend\Form\FormDataProvider\TcaSelectItems;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class TcaItemLabelViewHelper extends AbstractViewHelper
{
    /**
     * @param string $table
     * @param string $column
     * @param string $item
     * @param string $class
     * @return string
     */
    public function render($table, $column, $item, $class = null)
    {
        $label = '[' . $item . ']';

        $newLabel = $this->renderItemLabel($item, $table, $column);
        if ($newLabel) {
            $label = $newLabel;
        }

        if (!$newLabel) {
            $typoScript = TYPO3_MODE == 'FE' && isset($GLOBALS['TSFE']->tmpl->setup) ? $GLOBALS['TSFE']->tmpl->setup : BackendTypoScript::get($_REQUEST['id'] ?: null);
            if (isset($typoScript['config.']['tx_extbase.']['persistence.']['classes.'][$class . '.']['mapping.']['tableName'])) {
                $table = $typoScript['config.']['tx_extbase.']['persistence.']['classes.'][$class . '.']['mapping.']['tableName'];
            }

            $newLabel = $this->renderItemLabel($item, $table, $column);
            if ($newLabel) {
                $label = $newLabel;
            }

            if (!$newLabel && isset($typoScript['config.']['tx_extbase.']['persistence.']['classes.'][$class . '.']['mapping.']['columns.'])) {
                foreach ($typoScript['config.']['tx_extbase.']['persistence.']['classes.'][$class . '.']['mapping.']['columns.'] as $columnName => $config) {
                    if ($config['mapOnProperty'] == $column) {
                        $column = substr($columnName, 0, -1);

                        $newLabel = $this->renderItemLabel($item, $table, $column);
                        if ($newLabel) {
                            $label = $newLabel;
                        }
                        break;
                    }
                }
            }
        }

        if ($label ==  '[' . $item . ']' && $column != GeneralUtility::camelCaseToLowerCaseUnderscored($column)) {
            $test = $this->render($table, GeneralUtility::camelCaseToLowerCaseUnderscored($column), $item, $class);
            if ($test != '[' . $item . ']') {
                $label = $test;
            }
        }

        return $label;
    }

    protected function renderItemLabel($item, $table, $column)
    {
        $label = null;
        if (isset($GLOBALS['TCA'][$table]['columns'][$column]['config'])) {
            $config = $GLOBALS['TCA'][$table]['columns'][$column]['config'];

            $tcaItems = [];
            if (isset($config['items'])
                && is_array($config['items'])
            ) {
                $tcaItems = $config['items'];
            }
            if (isset($config['itemsProcFunc'])) {
                /** @var TcaSelectItems $tcaSelectItems */
                $tcaSelectItems = GeneralUtility::makeInstance(TcaSelectItems::class);
                $processorParameters = [
                    'items'    => &$tcaItems,
                    'config'   => $config,
                    'TSconfig' => [],
                    'table'    => $table,
                    'row'      => ['uid' => 0],
                    'field'    => $column,
                ];
                GeneralUtility::callUserFunction($config['itemsProcFunc'], $processorParameters, $tcaSelectItems);
            }
            if (is_array($tcaItems)) {
                foreach ($tcaItems as $tcaItem) {
                    if ($tcaItem[1] == $item) {
                        $label = LocalizationUtility::translate($tcaItem[0], 'ImiaBase');
                        break;
                    }
                }
            }
        }

        return $label;
    }
}