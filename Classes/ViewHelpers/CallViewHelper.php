<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\ViewHelpers;

/**
 * @package     imia_base_ext
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class CallViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $method
     * @param object $object
     * @param array $arguments
     * @throws \RuntimeException
     * @return mixed
     */
    public function render($method, $object = null, array $arguments = [])
    {
        if (null === $object) {
            $object = $this->renderChildren();
            if (false === is_object($object)) {
                throw new \RuntimeException(
                    'Using bx:call requires an object either as "object" attribute, tag content or inline argument',
                    1356849652
                );
            }
        }

        try {
            return call_user_func_array([$object, $method], $arguments);
        } catch(\Exception $e) {
            throw new \RuntimeException(
                'Method "' . $method . '" does not exist on object of type ' . get_class($object),
                1356834755
            );
        }
    }
}