<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\Session\Storage;

use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class FeSessionStorage extends AbstractSessionStorage
{
    /**
     * @param string $key
     * @param string $type
     * @return mixed
     */
    public function get($key, $type = 'ses')
    {
        return $this->getFeUser()->getKey($type, $this->getKey($key));
    }

    /**
     * @param string $key
     * @param string $type
     * @return boolean
     */
    public function has($key, $type = 'ses')
    {
        return $this->get($key) ? true : false;
    }

    /**
     * @param string $key
     * @param string $type
     * @param mixed $data
     */
    public function set($key, $data, $type = 'ses')
    {
        $this->getFeUser()->setKey($type, $this->getKey($key), $data);
        $this->getFeUser()->storeSessionData();
    }

    /**
     * @param string $key
     * @param string $type
     * @return void
     */
    public function remove($key, $type = 'ses')
    {
        if ($this->has($key, $type)) {
            $this->set($key, null, $type);
        }
    }

    /**
     * @param string $type
     */
    protected function setUserDataChanged($type = 'ses')
    {
        switch ($type) {
            case 'ses':
                $this->getFeUser()->sesData_change = 1;
                break;
            case 'user':
                $this->getFeUser()->userData_change = 1;
                break;
            default:
                $this->getFeUser()->sesData_change = 1;
                break;
        }
    }

    /**
     * @return FrontendUserAuthentication
     */
    protected function getFeUser()
    {
        return $GLOBALS['TSFE']->fe_user;
    }

    /**
     *
     * @return object
     */
    public function getUser()
    {
        return $this->getFeUser();
    }
}