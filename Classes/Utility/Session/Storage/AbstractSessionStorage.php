<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\Session\Storage;

use TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentValueException;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractSessionStorage implements StorageInterface
{
    /**
     * @var string
     */
    protected $sessionNamespace = 'imia';

    /**
     * Check whether the data is serializable or not
     *
     * @param mixed $data
     * @return boolean
     */
    public function isSerializable($data)
    {
        if (is_object($data) || is_array($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getKey($key)
    {
        return $this->sessionNamespace . $key;
    }

    /**
     * @param object $object
     * @param string $key
     * @param string $type
     * @throws InvalidArgumentValueException
     */
    public function storeObject($object, $key = null, $type = 'ses')
    {
        if (is_null($key)) {
            $key = get_class($object);
        }
        if ($this->isSerializable($object)) {
            $this->set($key, serialize($object), $type);
        } else {
            throw new InvalidArgumentValueException(sprintf('The object %s is not serializable.', get_class($object)));
        }
    }

    /**
     * @param string $key
     * @param string $type
     * @return object
     */
    public function getObject($key, $type = 'ses')
    {
        return unserialize($this->get($key, $type));
    }

    /**
     * @return string
     */
    public function getSessionNamespace()
    {
        return $this->sessionNamespace;
    }

    /**
     * @param string $sessionNamespace
     */
    public function setSessionNamespace($sessionNamespace)
    {
        $this->sessionNamespace = $sessionNamespace;
    }
}