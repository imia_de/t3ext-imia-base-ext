<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\Session\Storage;

use IMIA\ImiaBaseExt\Utility\Session\Storage\AbstractSessionStorage;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class SessionStorage implements StorageInterface
{
    /**
     * @var AbstractSessionStorage
     */
    protected $concreteSessionManager;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param mixed $data
     * @return true
     */
    public function isSerializable($data)
    {
        return $this->getConcreteSessionManager()->isSerializable($data);
    }

    /**
     * @param string $key
     * @param string $type
     * @return mixed
     */
    public function get($key, $type = 'ses')
    {
        return $this->getConcreteSessionManager()->get($key, $type);
    }

    /**
     * @param string $key
     * @param mixed $data
     * @param string $type
     */
    public function set($key, $data, $type = 'ses')
    {
        $this->getConcreteSessionManager()->set($key, $data, $type);
    }

    /**
     * @param string $key
     * @param string $type
     * @return boolean
     */
    public function has($key, $type = 'ses')
    {
        return $this->getConcreteSessionManager()->has($key, $type);
    }

    /**
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = 'ses')
    {
        $this->getConcreteSessionManager()->remove($key, $type);
    }

    /**
     * @param object $object
     * @param string $key
     * @param string $type
     */
    public function storeObject($object, $key = null, $type = 'ses')
    {
        $this->getConcreteSessionManager()->storeObject($object, $key, $type);
    }

    /**
     * @param string $key
     * @param string $type
     * @return mixed
     */
    public function getObject($key, $type = 'ses')
    {
        return $this->getConcreteSessionManager()->getObject($key, $type);
    }

    /**
     * @return object
     */
    public function getUser()
    {
        return $this->getConcreteSessionManager()->getUser();
    }

    /**
     * @return AbstractSessionStorage
     */
    public function getConcreteSessionManager()
    {
        if (!$this->concreteSessionManager) {
            if (TYPO3_MODE === 'FE') {
                $this->concreteSessionManager = $this->objectManager->get('IMIA\ImiaBaseExt\Utility\Session\Storage\FeSessionStorage');
            } else {
                $this->concreteSessionManager = $this->objectManager->get('IMIA\ImiaBaseExt\Utility\Session\Storage\BeSessionStorage');
            }
        }

        return $this->concreteSessionManager;
    }
}