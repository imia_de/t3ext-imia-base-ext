<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\Session\Storage;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class BeSessionStorage extends AbstractSessionStorage
{
    /**
     * @param string $key
     * @param string $type
     * @return mixed
     */
    public function get($key, $type = '')
    {
        return $this->getBeUser()->getSessionData($this->getKey($key));
    }

    /**
     * @param string $key
     * @param string $type
     * @param mixed $data
     */
    public function set($key, $data, $type = '')
    {
        $this->getBeUser()->setAndSaveSessionData($this->getKey($key), $data);
    }

    /**
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = '')
    {
        if ($this->has($key)) {
            $sesDat = unserialize($this->getBeUser()->user['ses_data']);
            unset($sesDat[$this->getKey($key)]);

            $this->getBeUser()->user['ses_data'] = (!empty($sesDat) ? serialize($sesDat) : '');
            $this->getDb()->exec_UPDATEquery(
                $this->getBeUser()->session_table,
                'ses_id=' . $this->getDb()->fullQuoteStr($this->getBeUser()->user['ses_id'], $this->getBeUser()->session_table),
                ['ses_data' => $this->getBeUser()->user['ses_data']]
            );
        }
    }

    /**
     * @param string $key
     * @param string $type
     * @return boolean
     */
    public function has($key, $type = '')
    {
        $sesDat = unserialize($this->getBeUser()->user['ses_data']);

        return isset($sesDat[$this->getKey($key)]) ? true : false;
    }

    /**
     *
     * @return BackendUserAuthentication
     */
    protected function getBeUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     *
     * @return object
     */
    public function getUser()
    {
        return $this->getBeUser();
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}