<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Cache
{
    /**
     * Datei Berechtigungen
     *
     * @var int
     */
    public $mode = 0775;

    /**
     * Debugmodus
     *
     * @var boolean
     */
    public $debug = false;

    /**
     * Cache Order
     *
     * @var string
     */
    protected $cachePath;

    /**
     * @var string
     */
    protected $ext = '.cache.php';

    /**
     * Konstruktor
     */
    public function __construct($cachePath = null)
    {
        if (!$cachePath) {
            $cachePath = PATH_site . 'typo3temp/var/Cache/Code/cache_core/';
        }

        if (substr($cachePath, strlen($cachePath) - 1) == '/') {
            $cachePath = substr($cachePath, 0, -1);
        }

        $this->ensureDirExists($cachePath);
        $this->cachePath = $cachePath . '/';
    }

    /**
     * Daten gecached?
     *
     * @param string $dataFile
     * @return boolean
     */
    public function hasCache($dataFile)
    {
        if (file_exists($this->cachePath . $dataFile . $this->ext)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $dataFile
     * @return integer
     */
    public function getModified($dataFile)
    {
        if ($this->hasCache($dataFile)) {
            return filemtime($this->cachePath . $dataFile . $this->ext);
        } else {
            return 0;
        }
    }

    /**
     * Cache resetten
     *
     * @param string $dataFile
     */
    public function resetCache($dataFile)
    {
        if ($this->hasCache($dataFile)) {
            @unlink($this->cachePath . $dataFile . $this->ext);
        } elseif (is_dir($this->cachePath . $dataFile)) {
            $this->deleteDir($this->cachePath . $dataFile);
        }
    }

    /**
     * Cache schreiben
     *
     * @param string $dataFile
     * @param mixed $data
     */
    public function writeCache($dataFile, $data)
    {
        $insertData = '<?php' . "\n" . '$_ = ' . $this->convertDataToStringCode($data) . ';' . "\n";
        $this->write($dataFile, $insertData);
    }

    /**
     * Cache in Datei schreiben
     *
     * @param string $dataFile
     * @param string $insertData
     */
    public function write($dataFile, $insertData)
    {
        if ($this->debug) {
            $this->debug('writing ' . $dataFile . $this->ext);
        }

        if (strpos($dataFile, '/') !== false) {
            $cacheDirs = explode('/', $dataFile);
            array_pop($cacheDirs);
            $dirPath = substr($this->cachePath, 0, -1);

            foreach ($cacheDirs as $dir) {
                $dirPath .= '/' . $dir;
                if (!is_dir($dirPath)) {
                    @mkdir($dirPath);
                    @chmod($dirPath, $this->mode);
                }
            }
        }

        @file_put_contents($this->cachePath . $dataFile . $this->ext, $insertData, LOCK_EX);
        @chmod($this->cachePath . $dataFile . $this->ext, $this->mode);
    }

    /**
     * Daten zu einer PHP Zeichenkette (rekursiv) umwandeln
     *
     * @param mixed $data
     * @param int $depth
     * @return string
     */
    public function convertDataToStringCode($data, $depth = 0)
    {
        switch (gettype($data)) {
            // Feldvariable
            case 'array':
                $sT = '';
                for ($i = 0; $i <= $depth; $i++) {
                    $sT .= "\t";
                }

                $phpArray = '';
                foreach ($data AS $key => $subData) {
                    $phpArray .= $sT . '\'' . $key . '\' => ' . $this->convertDataToStringCode($subData, ($depth + 1)) . ', ' . "\n";
                }
                $phpString = '[' . "\n" . $phpArray . substr($sT, 0, -1) . ']';
                break;
            // Fließkommazahl
            case 'double':
                $phpString = $data;
                break;
            // Ganzzahl
            case 'integer':
                $phpString = $data;
                break;
            // Zeichenkette
            case 'string':
                $phpString = "'" . str_replace("'", "\'", $data) . "'";
                break;
            // Boolean
            case 'boolean':
                $phpString = ($data ? 'true' : 'false');
                break;
            default:
                $phpString = 'unserialize(\'' . str_replace("'", "\'", serialize($data)) . '\')';
                break;
        }

        return $phpString;
    }

    /**
     * Cache laden
     *
     * @param string $dataFile
     * @return mixed
     */
    public function loadCache($dataFile)
    {
        if ($this->debug) {
            $this->debug('loading ' . $dataFile . $this->ext);
        }

        $_ = null;
        @include($this->cachePath . $dataFile . $this->ext);

        return $_;
    }

    /**
     * @param string
     * @return Cache
     */
    public function setCachePath($cachePath)
    {
        $this->cachePath = $cachePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @param string $path
     */
    protected function ensureDirExists($path)
    {
        if ($path && !is_dir($path)) {
            $this->ensureDirExists(dirname($path));
            @mkdir($path);
        }
    }

    /**
     * @param string $dirPath
     */
    protected function deleteDir($dirPath)
    {
        if (is_dir($dirPath)) {
            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }

            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::deleteDir($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($dirPath);
        }
    }

    /**
     * Debugausgabe
     *
     * @param string $message
     * @return void
     */
    protected function debug($message)
    {
        echo('<pre>Cache DEBUG: [' . $message . "]\n</pre>");
    }
}
