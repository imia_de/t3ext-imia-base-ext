<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class File
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $contents;

    /**
     * @param string $string
     * @param boolean $isFilename
     * @return string
     */
    static public function sanitize($string = '', $isFilename = true)
    {
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('/[^\w\-' . ($isFilename ? '~_\.' : '') . ']+/u', '-', trim($string));

        return strtolower($string);
    }

    /**
     * @param string $string
     * @param boolean $isFilename
     * @return string
     */
    static public function flatten($string = '', $isFilename = true)
    {
        return str_replace('_', '', self::sanitize($string, $isFilename));
    }

    /**
     * @param string $dir
     */
    static public function ensureDirExists($dir)
    {
        if (!is_dir($dir)) {
            self::ensureDirExists(dirname($dir));
            @mkdir($dir);
        }
    }

    /**
     * @param string $filename
     * @param string $path
     */
    public function __construct($filename, $path = '', $contents = null)
    {
        $this->setfilename($filename);
        $this->setPath($path);

        if ($contents !== null) {
            $this->setContents($contents);
        }
    }

    /**
     * @return boolean
     */
    public function load()
    {
        $success = false;
        if ($this->exists()) {
            $this->contents = GeneralUtility::getUrl($this->getFilePath());
            if ($this->contents) {
                $success = true;
            }
        }

        return $success;
    }

    /**
     * @return boolean
     */
    public function exists()
    {
        return file_exists($this->getFilePath());
    }

    /**
     * @param string $contents
     * @return boolean
     */
    public function write($contents = null)
    {
        $success = false;
        self::ensureDirExists(dirname($this->getFilePath()));

        if ($contents !== null) {
            $this->contents = $contents;
        }

        if (file_put_contents($this->getFilePath(), $this->contents)) {
            $success = true;
        }

        return $success;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->path . $this->filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $contents
     * @return \IMIA\ImiaBaseExt\Utility\File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $contents
     * @return \IMIA\ImiaBaseExt\Utility\File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getContents()
    {
        if ($this->contents === null) {
            $this->load();
        }

        return $this->contents;
    }

    /**
     * @param string $contents
     * @return \IMIA\ImiaBaseExt\Utility\File
     */
    public function setContents($contents)
    {
        $this->contents = $contents;

        return $this;
    }

    /**
     * @param string $contents
     * @return \IMIA\ImiaBaseExt\Utility\File
     */
    public function addContents($contents)
    {
        $this->contents .= $contents;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFilePath();
    }
}