<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Resource;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Helper
{
    /**
     * @param Resource\File|integer $file
     * @param array $fileReferenceData
     * @param BaseEntity $model
     * @param string $fieldname
     * @return Resource\FileReference
     */
    static public function createFileReferenceFromModel($file, $model, $fieldname, array $fileReferenceData = [])
    {
        $insertData = [
            'pid' => (int)$model->getPid(),
            'uid_foreign' => (int)$model->getUid(),
            'tablenames' => $model->getTable(),
            'fieldname' => $fieldname
        ];

        return self::createFileReference($file, $fileReferenceData, $insertData);

    }

    /**
     * @param Resource\File|integer $file
     * @param array $fileReferenceData
     * @param array $insertData
     * @return Resource\FileReference
     */
    static public function createFileReference($file, array $fileReferenceData = [], $insertData = null)
    {
        /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

        /** @var \TYPO3\CMS\Core\Resource\ResourceFactory $factory */
        $factory = $objectManager->get('TYPO3\CMS\Core\Resource\ResourceFactory');
        if (!is_object($file)) {
            try {
                $file = $factory->getFileObject((int)$file);
            } catch (\Exception $e) {
            }
        }

        $fileReference = null;
        if ($file && is_object($file)) {
            $fileReferenceData['uid_local'] = $file->getUid();

            if ($insertData && $insertData['tablenames'] && $insertData['fieldname'] && $insertData['uid_foreign']) {
                $result = self::getDb()->exec_SELECTquery('sorting', 'sys_file_reference',
                    'pid = ' . (int)$insertData['pid'] . ' AND uid_foreign = ' . $insertData['uid_foreign'] .
                    ' AND tablenames = "' . addslashes($insertData['tablenames']) . '" AND fieldname = "' . $insertData['fieldname'] . '"' .
                    ' AND table_local = "sys_file"', '', 'sorting DESC', 1);
                $row = self::getDb()->sql_fetch_assoc($result);

                self::getDb()->exec_INSERTquery('sys_file_reference', array_merge([
                    'pid' => (int)$insertData['pid'],
                    'uid_foreign' => (int)$insertData['uid_foreign'],
                    'tablenames' => $insertData['tablenames'],
                    'fieldname' => $insertData['fieldname'],
                    'table_local' => 'sys_file',
                    'crdate' => time(),
                    'tstamp' => time(),
                    'sorting' => $row && $row['sorting'] ? (int)$row['sorting'] + 1 : 1,
                ], $fileReferenceData));

                $fileReferenceData['uid'] = self::getDb()->sql_insert_id();
            }
            $fileReference = $factory->createFileReferenceObject($fileReferenceData);
        }

        return $fileReference;
    }

    /**
     * @param Resource\FileReference $fileReference
     */
    static public function deleteFileReference(Resource\FileReference $fileReference)
    {
        // delete file
        if (file_exists(PATH_site . $fileReference->getPublicUrl())) {
            $fileReference->getOriginalFile()->delete();
        }

        // delete file reference
        try {
            $fileReference->delete();
        } catch (\Exception $e) {
            self::getDb()->exec_DELETEquery('sys_file_reference', 'uid = ' . $fileReference->getUid());
        }
    }

    /**
     * @param Resource\FileReference $fileReference
     * @param Resource\File $file
     * @param boolean $deleteOldFile
     * @param integer $sorting
     * @return Resource\FileReference
     */
    static public function updateFileReference(Resource\FileReference &$fileReference, Resource\File $file = null, $deleteOldFile = true, $sorting = null)
    {
        $oldFile = $fileReference->getOriginalFile();

        $mergedFileReferenceData = $fileReference->toArray();
        $fileReferenceData = [
            'pid' => $mergedFileReferenceData['pid'],
            'tstamp' => time(),
            'crdate' => $mergedFileReferenceData['crdate'],
            'title' => $mergedFileReferenceData['title'],
            'description' => $mergedFileReferenceData['description'],
            'alternative' => $mergedFileReferenceData['alternative'],
            'link' => $mergedFileReferenceData['link'],
            'tablenames' => $mergedFileReferenceData['tablenames'],
            'fieldname' => $mergedFileReferenceData['fieldname'],
            'table_local' => $mergedFileReferenceData['table_local'],
        ];

        if ($file) {
            $fileReferenceData['uid_local'] = $file->getUid();
        }
        if ($sorting !== null) {
            $fileReferenceData['sorting_foreign'] = (int)$sorting;
        }

        self::getDb()->exec_UPDATEquery(
            'sys_file_reference',
            'uid = ' . (int)$mergedFileReferenceData['uid'],
            $fileReferenceData
        );

        if ($file) {
            $fileReferenceClass = get_class($fileReference);
            $fileReference = new $fileReferenceClass($fileReferenceData);
        }

        if ($deleteOldFile && $file) {
            $oldFile->delete();
        }

        return $fileReference;
    }

    /**
     * @param array|ObjectStorage $objects
     * @param string $property
     * @param string $direction
     * @return array
     */
    static public function sortObjectArrayByProperty($objects, $property, $direction = 'asc')
    {
        return self::sortObjectArrayByProperties($objects, [$property => $direction]);
    }

    /**
     * @param array|ObjectStorage $objects
     * @param array $uids
     * @param string $direction
     * @return array
     */
    static public function sortObjectArrayByUids($objects, $uids, $direction = 'asc')
    {
        $objectOrder = [];
        $objectCount = 0;
        foreach ($uids as $objectUid) {
            $objectOrder[(int)$objectUid] = ++$objectCount;
        }

        $orderedObjects = [];

        /** @var BaseEntity $object */
        foreach ($objects as $object) {
            $orderedObjects[$objectOrder[$object->getUid()]] = $object;
        }

        if (strtolower($direction) === 'asc') {
            ksort($orderedObjects);
        } else {
            krsort($orderedObjects);
        }

        return $orderedObjects;
    }

    /**
     * @param array|ObjectStorage $objects
     * @param array $properties
     * @return array
     */
    static public function sortObjectArrayByProperties($objects, $properties)
    {
        if (!is_array($objects)) {
            $objectArray = [];
            foreach ($objects as $object) {
                $objectArray[] = $object;
            }

            $objects = $objectArray;
        }

        $propertyMethods = [];
        $propertyDirections = [];
        foreach ($properties as $property => $direction) {
            $propertyArr = explode('.', $property);

            $propertyMethod = [];
            foreach ($propertyArr as $subProperty) {
                $propertyMethod[] = 'get' . ucfirst($subProperty);
            }
            $propertyMethods[] = $propertyMethod;
            $propertyDirections[] = strtolower($direction);
        }

        usort($objects, function ($a, $b) use ($propertyMethods, $propertyDirections) {
            $compare = 0;
            foreach ($propertyMethods as $key => $propertyMethod) {
                $propertyValueA = $a;
                $propertyValueB = $b;
                foreach ($propertyMethod as $propertySubMethod) {
                    if ($propertyValueA) {
                        $propertyValueA = $propertyValueA->$propertySubMethod();
                    }
                    if ($propertyValueB) {
                        $propertyValueB = $propertyValueB->$propertySubMethod();
                    }
                }

                $propertyValueA = strtolower(Helper::getOptimizedValue($propertyValueA));
                $propertyValueB = strtolower(Helper::getOptimizedValue($propertyValueB));

                $compare = strcmp($propertyValueA, $propertyValueB);
                if ($propertyDirections[$key] != 'asc') {
                    $compare *= -1;
                }

                if ($compare != 0) {
                    break;
                }
            }

            return $compare;
        });

        return $objects;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    static public function getOptimizedValue($value)
    {
        if (is_object($value)) {
            if (get_class($value) == 'DateTime') {
                $value = $value->getTimestamp();
            } else {
                $value = (string)$value;
            }
        } elseif (is_numeric($value)) {
            $value = str_pad($value, 10, '0', STR_PAD_LEFT);
        }

        return $value;
    }

    /**
     * @param bool $persistence
     */
    static public function setTstampPersistence($persistence = true)
    {
        $GLOBALS['tstamp_persistence'] = $persistence;
    }

    /**
     * @param integer $uid
     * @param string $table
     * @param string $field
     * @param \IMIA\ImiaBaseExt\Domain\Repository\Repository|boolean|null $categoryRepository
     * @param string $categorySelectFields
     * @return array
     */
    static public function getRecordCategories($uid, $table, $field = 'categories', $categoryRepository = null, $categorySelectFields = 'uid, title')
    {
        $fieldNameCondition = ' AND fieldname = ' . self::getDb()->fullQuoteStr($field, 'sys_category_record_mm');
        $tableNameCondition = ' AND tablenames = ' . self::getDb()->fullQuoteStr($table, 'sys_category_record_mm');

        $result = self::getDb()->exec_SELECTquery(
            'uid_local',
            'sys_category_record_mm',
            'uid_foreign = ' . (int)$uid . $tableNameCondition . $fieldNameCondition);

        $categoryUids = [];
        while ($row = self::getDb()->sql_fetch_assoc($result)) {
            $categoryUids[] = (int)$row['uid_local'];
        }

        if (!$categoryRepository) {
            return $categoryUids;
        } elseif (is_bool($categoryRepository) && $categoryRepository) {
            $categoryUids[] = 0;

            return self::getDb()->exec_SELECTgetRows(
                $categorySelectFields,
                'sys_category',
                'uid IN (' . implode(',', $categoryUids) . ')');
        } else {
            if (count($categoryUids) > 0) {
                return $categoryRepository->findByInUid($categoryUids);
            } else {
                return [];
            }
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected static function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}