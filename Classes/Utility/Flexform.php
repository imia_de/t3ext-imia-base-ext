<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class Flexform
{
    /**
     * @var string
     */
    protected $extensionKey = 'tx_imiabaseext';

    /**
     * @var array
     */
    private $pluginSettings;

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    private $contentObjectRenderer;

    /**
     * @param array $params
     * @param array $items
     */
    protected function addItems(&$params, $items)
    {
        if (is_array($items)) {
            foreach ($items as $item) {
                $label = $this->getContentObjectRenderer()->stdWrap($item['label'], $item['label.']);
                $value = $this->getContentObjectRenderer()->stdWrap($item['value'], $item['value.']);
                $icon = $this->getContentObjectRenderer()->stdWrap($item['icon'], $item['icon.']);

                if (preg_match('/^(EXT:)?([^\/]+)\//ism', $value, $match)) {
                    $label = '[' . $match[2] . '] ' . $label;
                }

                $itemConf = [
                    $label,
                    $value,
                ];
                if ($icon) {
                    $itemConf[] = $icon;
                }

                $params['items'][$value] = $itemConf;
            }
        }
    }

    /**
     * @param string $name
     * @param integer $pageUid
     * @return array
     */
    protected function getPluginSettings($name, $pageUid)
    {
        if (!$this->pluginSettings) {
            $typoScript = BackendTypoScript::get($pageUid);
            $this->pluginSettings = $typoScript['plugin.'][$this->extensionKey . '.']['flexforms.'];
        }

        if (is_array($this->pluginSettings) && array_key_exists($name, $this->pluginSettings)) {
            return $this->pluginSettings[$name];
        } else {
            return null;
        }
    }

    /**
     * @param array $params
     * @return integer
     */
    protected function getPid($params)
    {
        $record = $this->getRecord($params);
        if ((int)$record['pid'] > 0) {
            return (int)$record['pid'];
        } else {
            $otherRecord = BackendUtility::getRecordWSOL($params['table'], ((int)$record['pid'] * -1));

            return $otherRecord['pid'];
        }
    }

    /**
     * @param array $params
     * @return array
     */
    protected function getRecord($params)
    {
        if (array_key_exists('flexParentDatabaseRow', $params) && is_array($params['flexParentDatabaseRow'])) {
            $record = $params['flexParentDatabaseRow'];
        } else {
            $record = $params['row'];
        }

        return $record;
    }

    /**
     * @return \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected function getContentObjectRenderer()
    {
        if (!$this->contentObjectRenderer) {
            $objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
            $GLOBALS['TSFE'] = $objectManager->get('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], 0, 0);
            $GLOBALS['TSFE']->config['config']['language'] = $GLOBALS['LANG']->lang;
            $GLOBALS['TSFE']->initLLvars();
            $GLOBALS['TSFE']->initTemplate();

            $this->contentObjectRenderer = $objectManager->get('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        }

        return $this->contentObjectRenderer;
    }
}