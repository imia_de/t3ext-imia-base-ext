<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Core\Bootstrap;
use TYPO3\CMS\Extbase\Mvc\Dispatcher;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Response;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Frontend\Utility\EidUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class EidDispatcher
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $vendorName;

    /**
     * @var string
     */
    protected $extensionName;

    /**
     * @var string
     */
    protected $pluginName;

    /**
     * @var string
     */
    protected $controllerName;

    /**
     * @var string
     */
    protected $actionName;

    /**
     * @var array
     */
    protected $arguments;

    /**
     * @param integer $pageUid
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InfiniteLoopException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InvalidActionNameException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InvalidControllerNameException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InvalidExtensionNameException
     */
    public function dispatch($pageUid = null)
    {
        $this->init($pageUid);

        $configuration = [
            'vendorName'                  => $this->vendorName,
            'extensionName'               => $this->extensionName,
            'pluginName'                  => $this->pluginName,
            'switchableControllerActions' => [
                $this->controllerName => [$this->actionName],
            ],
        ];

        /** @var Bootstrap $bootstrap */
        $bootstrap = $this->getObjectManager()->get(Bootstrap::class);
        $bootstrap->initialize($configuration);
        $bootstrap->cObj = $GLOBALS['TSFE']->cObj;

        /** @var Request $request */
        $request = $this->getObjectManager()->get(Request::class);
        $request->setControllerVendorName($this->vendorName);
        $request->setControllerExtensionName($this->extensionName);
        $request->setPluginName($this->pluginName);
        $request->setControllerName($this->controllerName);
        $request->setControllerActionName($this->actionName);
        $request->setArguments($this->arguments ?: (array)GeneralUtility::_GP('arguments'));
        $request->setFormat($this->arguments && isset($this->arguments['_format']) ? $this->arguments['_format'] : 'html');

        /** @var Response $response */
        $response = $this->getObjectManager()->get(Response::class);
        $response->setHeader('Access-Control-Allow-Origin', '*');

        /** @var Dispatcher $dispatcher */
        $dispatcher = $this->getObjectManager()->get(Dispatcher::class);
        $dispatcher->dispatch($request, $response);

        $this->getTSFE()->sendCacheHeaders();

        $response->send();
    }

    /**
     * @param integer $pageUid
     * @param callable $manpulateTsClosure
     * @return $this
     */
    public function init($pageUid = null, callable $manpulateTsClosure = null)
    {
        /** @var PageRepository $pageRepository */
        $pageRepository = $this->getObjectManager()->get(PageRepository::class);
        if (!is_numeric($pageUid)) {
            $pageUid = $pageRepository->getDomainStartPage(
                GeneralUtility::getIndpEnv('HTTP_HOST'),
                GeneralUtility::getIndpEnv('SCRIPT_NAME'),
                GeneralUtility::getIndpEnv('REQUEST_URI'));
        }

        if (!isset($GLOBALS['TSFE']) || !$GLOBALS['TSFE'] || $GLOBALS['TSFE']->id != $pageUid) {
            /** @var TypoScriptFrontendController $GLOBALS ['TSFE'] */
            $GLOBALS['TSFE'] = $this->getObjectManager()->get(TypoScriptFrontendController::class,
                $GLOBALS['TYPO3_CONF_VARS'], $pageUid, 0, 1);

            $GLOBALS['TSFE']->sys_page = $pageRepository;

            EidUtility::initLanguage();

            ExtensionManagementUtility::loadBaseTca(true);
            ExtensionManagementUtility::loadExtTables(true);
            if (is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'] as $classReference) {
                    /** @var $hookObject \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface */
                    $hookObject = GeneralUtility::getUserObj($classReference);
                    if (!$hookObject instanceof \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface) {
                        throw new \UnexpectedValueException(
                            '$hookObject "' . $classReference . '" must implement interface TYPO3\\CMS\\Core\\Database\\TableConfigurationPostProcessingHookInterface',
                            1320585902
                        );
                    }
                    $hookObject->processData();
                }
            }

            $GLOBALS['TSFE']->initFEuser();
            $GLOBALS['TSFE']->determineId();
            $GLOBALS['TSFE']->initTemplate();
            $GLOBALS['TSFE']->getConfigArray();

            if ($manpulateTsClosure) {
                $manpulateTsClosure($GLOBALS['TSFE']->tmpl->setup);
            }

            $GLOBALS['TSFE']->renderCharset = 'utf-8';

            $GLOBALS['TSFE']->cObj = $this->getObjectManager()->get(ContentObjectRenderer::class);
            $GLOBALS['TSFE']->settingLanguage();
            $GLOBALS['TSFE']->settingLocale();
            $GLOBALS['TSFE']->no_cache = true;
        }

        return $this;
    }

    /**
     * @param string $vendorName
     * @return $this
     */
    public function setVendorName($vendorName)
    {
        $this->vendorName = $vendorName;

        return $this;
    }

    /**
     * @param string $extensionName
     * @return $this
     */
    public function setExtensionName($extensionName)
    {
        $this->extensionName = $extensionName;

        return $this;
    }

    /**
     * @param string $pluginName
     * @return $this
     */
    public function setPluginName($pluginName)
    {
        $this->pluginName = $pluginName;

        return $this;
    }

    /**
     * @param string $controllerName
     * @return $this
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;

        return $this;
    }

    /**
     * @param string $actionName
     * @return $this
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return EidDispatcher
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        if (!$this->objectManager) {
            $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        }

        return $this->objectManager;
    }

    /**
     * @return TypoScriptFrontendController
     */
    public function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}