<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use IMIA\ImiaBaseExt\Metadata\ClassMetadata;
use Symfony\Component\Yaml\Yaml;
use Doctrine\Common\Annotations\AnnotationReader;
use IMIA\ImiaBaseExt\Metadata\Driver\AnnotationDriver;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration\SQL;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration\TCA;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration\TypoScript;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class DynamicConfiguration
{
    /**
     * @var Cache
     */
    static public $cache;

    /**
     * @var string
     */
    static public $cacheKey;

    /**
     * @var boolean
     */
    static private $create = false;

    /**
     * @var array
     */
    static private $settings = [];

    /**
     * @var array
     */
    static private $metadata = [];

    /**
     * @var array
     */
    static private $classNames = [];

    /**
     * @var array
     */
    static private $ignoreAnnotations = ['validate', 'lazy', 'inject', 'fixme', 'todo', 'extensionScannerIgnoreLine'];

    /**
     * @param string $extKey
     * @param string $type
     * @return boolean
     */
    static public function annotationEnabled($extKey, $type = 'tca')
    {
        self::load();

        $enabled = false;
        if (array_key_exists($extKey, self::$settings) && array_key_exists('annotations', self::$settings[$extKey])) {
            if (is_string(self::$settings[$extKey]['annotations'][$type])) {
                $enabled = self::$settings[$extKey]['annotations'][$type] === 'true' ? true : false;
            } else {
                $enabled = (bool)self::$settings[$extKey]['annotations'][$type];
            }
        }

        return $enabled;
    }

    /**
     * @param string $type
     */
    static public function build($type = 'localconf')
    {
        $cacheFile = new File('imia_baseext_' . $type . '.php', PATH_site . 'typo3temp/var/Cache/Code/cache_core/');
        if (!$cacheFile->exists()) {
            self::load();

            $contents = '';
            switch ($type) {
                case 'localconf':
                    foreach (self::$settings as $extKey => $settings) {
                        $file = self::getTyposcript($extKey);
                        if ($file->exists()) {
                            $contents .= "\n" . $file->getContents();
                        }
                    }
                    break;
                case 'tables':
                    foreach (self::$settings as $extKey => $settings) {
                        $file = self::getTCA($extKey);
                        if ($file->exists()) {
                            $contents .= "\n" . str_replace(['<?php', '?>'], '', $file->getContents());
                        }
                    }

                    $contents = '<?php' . "\n" . $contents;
                    break;
            }

            if ($contents) {
                $cacheFile->write($contents);
                self::$create = true;
            }
        }

        if ($cacheFile->exists()) {
            switch ($type) {
                case 'localconf':
                    ExtensionManagementUtility::addTypoScript('imia_base_ext', 'setup', $cacheFile->getContents());
                    break;
                case 'tables':
                    @require_once($cacheFile->getFilePath());
                    break;
            }
        }

        if (self::$create) {
            @chmod(PATH_site . 'typo3temp', 0775);
        }
    }

    /**
     * @param string $extKey
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    static public function create($extKey)
    {
        self::load();
        self::clearCache($extKey);

        self::createSQL($extKey);
        self::createTCA($extKey);
        self::createTypoScript($extKey);

        GeneralUtility::makeInstance(CacheManager::class)->getCache('cache_core')->flush();
    }

    /**
     * @return array
     */
    static public function getCacheCommands()
    {
        self::load();

        $cacheActions = $optionValues = [];
        $settings = self::$settings;
        ksort($settings);
        foreach ($settings as $extKey => $setting) {
            $cmd = 'imia_base_ext-' . $extKey;
            $title = sprintf($GLOBALS['LANG']->sL('LLL:EXT:imia_base_ext/Resources/Private/Language/locallang.xlf:rm.clearCacheMenu_imiabaseext', true), $extKey);
            $description = sprintf($GLOBALS['LANG']->sL('LLL:EXT:imia_base_ext/Resources/Private/Language/locallang.xlf:rm.clearCacheMenu_imiabaseext_desc', true), $extKey);

            $cacheActions[$cmd] = [
                'id'             => $cmd,
                'title'          => $title,
                'description'    => $description,
                'href'           => BackendUtility::getModuleUrl('tce_db', ['vC' => $GLOBALS['BE_USER']->veriCode(), 'cacheCmd' => $cmd, 'ajaxCall' => 1]),
                'iconIdentifier' => 'actions-system-cache-clear-extensions',
            ];
            $optionValues[$cmd] = $cmd;
        }

        return [
            'actions' => $cacheActions,
            'options' => $optionValues,
        ];
    }

    /**
     * @param string $extKey
     */
    static public function clearCache($extKey = null)
    {
        self::init();

        if ($extKey) {
            $extPath = ExtensionManagementUtility::extPath($extKey, 'Configuration');

            if (!is_dir($extPath)) {
                @mkdir($extPath);
            }

            if (is_dir($extPath . '/Cache')) {
                self::rrmdir($extPath . '/Cache');
            }
            mkdir($extPath . '/Cache');
            mkdir($extPath . '/Cache/SQL');
            mkdir($extPath . '/Cache/TCA');
            mkdir($extPath . '/Cache/TypoScript');

            @exec('chmod 777 -R ' . $extPath . '/Cache');
        } else {
            // force complete core_cache clear, even in TYPO3 6.2
            $spriteCacheFile = PATH_site . 'typo3temp/sprites/zextensions.css';
            if (file_exists($spriteCacheFile)) {
                @unlink($spriteCacheFile);
            }

            // reset baseExt setting cache
            self::$cache->resetCache('cache_core/ext_settings_' . self::$cacheKey . '.php');

            self::rrmdir(PATH_site . 'typo3temp/var/Cache/Code/cache_core');
            self::rrmdir(PATH_site . 'typo3temp/autoload');

            if (!is_dir(PATH_site . 'typo3temp')) {
                @mkdir(PATH_site . 'typo3temp');
            }
            if (!is_dir(PATH_site . 'typo3temp/compressor')) {
                @mkdir(PATH_site . 'typo3temp/compressor');
            }
            if (!is_dir(PATH_site . 'typo3temp/GB')) {
                @mkdir(PATH_site . 'typo3temp/GB');
            }
            if (!is_dir(PATH_site . 'typo3temp/locks')) {
                @mkdir(PATH_site . 'typo3temp/locks');
            }
            if (!is_dir(PATH_site . 'typo3temp/cs')) {
                @mkdir(PATH_site . 'typo3temp/cs');
            }
            if (!is_dir(PATH_site . 'typo3temp/pics')) {
                @mkdir(PATH_site . 'typo3temp/pics');
            }
            if (!is_dir(PATH_site . 'typo3temp/llxml')) {
                @mkdir(PATH_site . 'typo3temp/llxml');
            }
            if (!is_dir(PATH_site . 'typo3temp/temp')) {
                @mkdir(PATH_site . 'typo3temp/temp');
            }
            if (!is_dir(PATH_site . 'typo3temp/_processed_')) {
                @mkdir(PATH_site . 'typo3temp/_processed_');
            }
            if (!is_file(PATH_site . 'typo3temp/index.html')) {
                @file_put_contents(PATH_site . 'typo3temp/index.html', '');
            }
        }
    }

    /**
     * @param string $dir
     * @return bool
     */
    static private function rrmdir($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }
        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item != '.' && $item != '..') {
                if (!self::rrmdir($dir . DIRECTORY_SEPARATOR . $item)) {
                    return false;
                }
            }
        }

        return rmdir($dir);
    }

    /**
     * @param string $path
     * @return string
     */
    static public function getExtPath($path)
    {
        if (strpos($path, PATH_site) === 0) {
            $path = str_replace(PATH_site, '', $path);

            if (strpos($path, 'typo3conf/ext/') === 0) {
                $path = str_replace('typo3conf/ext/', '', $path);
                $pathSegments = explode('/', $path, 2);
                $pathCode = '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath(\'' . $pathSegments[0] . '\', \'' . $pathSegments[1] . '\')';
            } else {
                $pathCode = "'" . $path . "'";
            }
        } else {
            $pathCode = "'" . $path . "'";
        }

        return $pathCode;
    }

    /**
     * @param string $searchClassName
     * @return mixed
     */
    static public function getExtKeyForClass($searchClassName)
    {
        if (!self::$classNames) {
            self::$classNames = [];
            foreach (self::$settings as $extKey => $settings) {
                foreach ($settings['classes'] as $className) {
                    self::$classNames[$className] = $extKey;
                }
            }
        }

        if (array_key_exists($searchClassName, self::$classNames)) {
            return self::$classNames[$searchClassName];
        } else {
            return false;
        }
    }

    /**
     * @param string $extKey
     * @return File
     */
    static public function getSQL($extKey)
    {
        return new File('ext_tables.sql', ExtensionManagementUtility::extPath($extKey, 'Configuration/Cache/SQL/'));
    }

    /**
     * @param string $extKey
     * @return File
     */
    static private function getTCA($extKey)
    {
        return new File('ext_tables.php', ExtensionManagementUtility::extPath($extKey, 'Configuration/Cache/TCA/'));
    }

    /**
     * @param string $extKey
     * @return File
     */
    static private function getTyposcript($extKey)
    {
        return new File('setup.ts', ExtensionManagementUtility::extPath($extKey, 'Configuration/Cache/TypoScript/'));
    }

    /**
     * Load the extension settings
     */
    static private function init()
    {
        if (!self::$cacheKey) {
            self::$cacheKey = sha1(TYPO3_version . PATH_site . 'imia_base_ext');
        }
        if (!self::$cache) {
            self::$cache = new Cache(PATH_site . 'typo3temp/var/Cache/Code/');
        }
    }

    /**
     * Load the extension settings
     */
    static private function load()
    {
        self::init();

        if (!self::$settings) {
            if (self::$cache->hasCache('cache_core/ext_settings_' . self::$cacheKey . '.php')) {
                self::$settings = self::$cache->loadCache('cache_core/ext_settings_' . self::$cacheKey . '.php');
            } else {
                foreach ($GLOBALS['TYPO3_LOADED_EXT'] as $extKey => $extInfo) {
                    $extPath = ExtensionManagementUtility::extPath($extKey);
                    $settingsPath = $extPath . 'ext_baseext.yml';

                    // deprecated
                    if (!file_exists($settingsPath)) {
                        $settingsPath = $extPath . 'Configuration/ImiaBaseExt/settings.yml';
                    }

                    if (file_exists($settingsPath)) {
                        self::$settings[$extKey] = Yaml::parse(GeneralUtility::getUrl($settingsPath));
                        self::$settings[$extKey]['path'] = $extPath;

                        self::getClasses($extKey);
                    }
                }

                self::$cache->writeCache('cache_core/ext_settings_' . self::$cacheKey . '.php', self::$settings);
                self::$create = true;
            }
        }
    }

    /**
     * @param string $extKey
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    static private function createSQL($extKey)
    {
        if (self::annotationEnabled($extKey, 'sql')) {
            $cacheFile = self::getSQL($extKey);
            if (!$cacheFile->exists()) {
                $metadata = self::loadMetadata($extKey, 'sql');

                $sqlGen = new SQL($extKey);
                $sql = $sqlGen->build($metadata);

                $cacheFile->write($sql);
            }
        }
    }

    /**
     * @param string $extKey
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    static private function createTCA($extKey)
    {
        if (self::annotationEnabled($extKey, 'tca')) {
            $cacheFile = self::getTCA($extKey);
            if (!$cacheFile->exists()) {
                $cacheFile->setContents('<?php' . LF .
                    'if (!defined (\'TYPO3_MODE\')) {' . LF .
                    '    die (\'Access denied.\');' . LF .
                    '}' . LF .
                    LF
                );

                $metadata = self::loadMetadata($extKey, 'tca');

                $tcaGen = new TCA($extKey);
                $cacheFiles = $tcaGen->build($metadata, $cacheFile);

                /** @var File $file */
                foreach ($cacheFiles as $file) {
                    $file->write();
                }
            }
        }
    }

    /**
     * @param string $extKey
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    static private function createTypoScript($extKey)
    {
        if (self::annotationEnabled($extKey, 'sql') || self::annotationEnabled($extKey, 'tca')) {
            $cacheFile = self::getTyposcript($extKey);
            if (!$cacheFile->exists()) {
                $metadata = self::loadMetadata($extKey, 'ts');

                $tsGen = new TypoScript($extKey);
                $typoScript = $tsGen->build($metadata);

                $cacheFile->setContents($typoScript);
                $cacheFile->write();
            }
        }
    }

    /**
     * @param string $extKey
     * @param string $type
     * @return ClassMetadata
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    static private function loadMetadata($extKey, $type)
    {
        $key = $extKey . '-' . $type;
        if (!array_key_exists($key, self::$metadata)) {
            $reader = new AnnotationReader();
            foreach (self::$ignoreAnnotations as $annotation) {
                $reader::addGlobalIgnoredName(strtolower($annotation));
                $reader::addGlobalIgnoredName(strtoupper($annotation));
                $reader::addGlobalIgnoredName($annotation);
            }
            $reader::addGlobalIgnoredNamespace('TYPO3\CMS\Extbase\Annotation\ORM');

            $annotationDriver = new AnnotationDriver($reader, $type);
            $metadataFactory = new \Metadata\MetadataFactory($annotationDriver);

            self::$metadata[$key] = [];
            foreach (self::getClasses($extKey) as $classPath => $className) {
                if (stripos($className, 'templavoila') !== false) {
                    continue; // problematic xclasses could break the generation process (initialization of the core)
                }

                if (!class_exists($className)) {
                    @require_once($classPath);
                }
                if (class_exists($className) && in_array('TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface', class_implements($className))) {
                    self::$metadata[$key][$className] = $metadataFactory->getMetadataForClass($className);
                }
            }
        }

        return self::$metadata[$key];
    }

    /**
     * @param string $extKey
     * @return array
     */
    static private function getClasses($extKey)
    {
        if (self::$settings[$extKey] && !array_key_exists('classes', self::$settings[$extKey])) {
            $classes = array_merge(
                self::loadClasses(self::$settings[$extKey]['path'] . '/Classes/Domain/Model'),
                self::loadClasses(self::$settings[$extKey]['path'] . '/Classes/Xclass')
            );

            $classNames = [];
            foreach ($classes as $classPath) {
                $classNames[$classPath] = self::getClassFromPath($classPath);
            }

            self::$settings[$extKey]['classes'] = $classNames;
        }

        return self::$settings[$extKey]
            ? self::$settings[$extKey]['classes']
            : [];
    }

    /**
     * @param string $path
     * @return array
     */
    static private function loadClasses($path)
    {
        $classes = [];
        if (is_dir($path)) {
            $dir = dir($path);
            while (false !== ($entry = $dir->read())) {
                if (!in_array($entry, ['.', '..'])) {
                    $entryPath = realpath($path . '/' . $entry);
                    if (is_dir($entryPath)) {
                        $classes = array_merge($classes, self::loadClasses($entryPath));
                    } elseif (preg_match('/\.php$/ism', $entry) && is_file($entryPath)) {
                        $classes[] = $entryPath;
                    }
                }
            }
            $dir->close();
        }
        sort($classes);

        return $classes;
    }

    /**
     * @param string $path
     * @return string
     */
    static private function getClassFromPath($path)
    {
        $className = self::getClassFromPathByToken($path);

        if (!$className) {
            die('No class found in: ' . $path);
        } elseif (substr($className, strlen($className) - 1) == '\\') {
            $className .= preg_replace('/^.*?([^\/]+)\.php$/ism', '$1', $path);
        }

        return $className;
    }

    /**
     * @param string $path
     * @return string
     */
    static private function getClassFromPathByToken($path)
    {
        $class = '';
        $namespace = '';
        $buffer = '';

        $fp = fopen($path, 'r');
        $i = 0;
        while (!feof($fp) && !$class) {
            $buffer .= fread($fp, 512);
            $tokens = @token_get_all($buffer);

            if (strpos($buffer, '{') === false) continue;

            for (; $i < count($tokens); $i++) {
                if ($tokens[$i][0] === T_NAMESPACE) {
                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j][0] === T_STRING) {
                            $namespace .= '\\' . $tokens[$j][1];
                        } else if ($tokens[$j] === '{' || $tokens[$j] === ';') {
                            break;
                        }
                    }
                }

                if ($tokens[$i][0] === T_CLASS) {
                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j] === '{') {
                            $class = $tokens[$i + 2][1];
                        }
                    }
                }
            }
        }

        return substr($namespace, 1) . '\\' . $class;
    }
}
