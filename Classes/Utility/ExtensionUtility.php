<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class ExtensionUtility
{
    /**
     * @param string $extKey
     * @param string $name
     * @param string $path
     * @param string $label
     */
    static public function registerPageIcon($extKey, $name, $path, $label = null)
    {
        $iconPath = ExtensionManagementUtility::siteRelPath($extKey) . $path;

        $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [$label ?: $name, $name, $iconPath];

        /** @var IconRegistry $iconRegistry */
        $iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);
        $iconRegistry->registerIcon(
            'tcarecords-pages-contains-' . $name,
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => $iconPath]
        );
        $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-' . $name] = 'tcarecords-pages-contains-' . $name;
    }

    /**
     * @param string $extKey
     * @param string $pluginName
     * @param string $path
     * @param string $altExtKey
     */
    static public function registerPluginFlexform($extKey, $pluginName, $path, $altExtKey = null)
    {
        $extensionName = str_replace(' ', '', ucwords(str_replace('_', ' ', $extKey)));
        $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);

        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,recursive,pages';

        ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . ($altExtKey ?: $extKey) . '/' . $path);
    }

    /**
     * @param string $extKey
     * @param string $name
     * @param string $path
     * @param string $afterPosition
     * @param array $moduleConfiguration
     */
    static public function registerModuleGroup($extKey, $name, $path = 'Configuration/Module/', $afterPosition = 'file', $moduleConfiguration = [])
    {
        if ($path) {
            $absPath = ExtensionManagementUtility::extPath($extKey, $path);

            $MCONF = null;
            $MLANG = null;
            require_once($absPath . 'conf.php');

            if (is_array($MCONF) && isset($MCONF['name'])) {
                $moduleConfiguration['name'] = $MCONF['name'];
            }
            if (is_array($MLANG) && isset($MLANG['default']['ll_ref'])) {
                $moduleConfiguration['labels'] = $MLANG['default']['ll_ref'];
            }
            if (is_array($MLANG) && isset($MLANG['icon'])) {
                $moduleConfiguration['icon'] = $MLANG['icon'];
            } elseif (isset($MLANG['default']['tabs_images']['tab'])) {
                $moduleConfiguration['icon'] = $MLANG['default']['tabs_images']['tab'];
            }
        }

        ExtensionManagementUtility::addModule($name, '', '', ExtensionManagementUtility::extPath($extKey) . $path, $moduleConfiguration);

        $extModConf = null;
        if (isset($GLOBALS['TBE_MODULES'][$afterPosition])) {
            $extModConf = $GLOBALS['TBE_MODULES'][$name];
            unset($GLOBALS['TBE_MODULES'][$name]);
        }

        $tempBeModules = [];
        foreach ($GLOBALS['TBE_MODULES'] as $key => $value) {
            $tempBeModules[$key] = $value;
            if ($key == $afterPosition) {
                $tempBeModules[$name] = $extModConf;
            }
        }

        $GLOBALS['TBE_MODULES'] = $tempBeModules;
    }

    /**
     * @param string $table
     * @param string|array $fields
     * @param mixed $condition
     * @param string $requestUpdateField
     */
    static public function addDisplayCond($table, $fields, $condition, $requestUpdateField = null)
    {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        foreach ($fields as $field) {
            if ($GLOBALS['TCA'][$table]['columns'][$field]['displayCond']) {
                if (is_array($GLOBALS['TCA'][$table]['columns'][$field]['displayCond'])) {
                    if (isset($GLOBALS['TCA'][$table]['columns'][$field]['displayCond']['AND'])) {
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond']['AND'][] = $condition;
                    } else {
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = ['AND' => [
                            'AND' => $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'],
                            $condition,
                        ]];
                    }
                } else {
                    $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = ['AND' => [
                        $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'],
                        $condition,
                    ]];
                }
            } else {
                $GLOBALS['TCA'][$table]['columns'][$field]['displayCond'] = $condition;
            }

            if ($requestUpdateField) {
                $GLOBALS['TCA'][$table]['columns'][$field]['onChange'] = 'reload';
            }
        }
    }
}