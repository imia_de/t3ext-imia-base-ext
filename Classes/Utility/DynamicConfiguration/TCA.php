<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\DynamicConfiguration;

use IMIA\ImiaBaseExt\Annotation\TCA\Copy;
use IMIA\ImiaBaseExt\Metadata\ClassMetadata;
use IMIA\ImiaBaseExt\Metadata\PropertyMetadata;
use IMIA\ImiaBaseExt\Utility\DynamicConfiguration;
use IMIA\ImiaBaseExt\Utility\File;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_base_ext
 * @subpackage  Utility\DynamicConfiguration
 * @author      David Frerich <d.frerich@imia.de>
 */
class TCA
{
    /**
     * @var array
     */
    static private $tcaColumnNoAutoLabel = [
        \IMIA\ImiaBaseExt\Annotation\TCA\None::class,
        \IMIA\ImiaBaseExt\Annotation\TCA\Passthrough::class,
    ];

    /**
     * @var string
     */
    private $extKey;

    /**
     * @var string
     */
    private $extPath;

    /**
     * @var string
     */
    private $extLangFile;

    /**
     * @var array
     */
    private $extLangFiles = [];

    /**
     * @var array
     */
    private $run = [];

    /**
     * @var array
     */
    private $runBefore = [];

    /**
     * @param string $extKey
     */
    public function __construct($extKey)
    {
        $this->extKey = $extKey;
        $this->extPath = ExtensionManagementUtility::extPath($extKey);
    }

    /**
     * @param ClassMetadata $metadata
     * @param \IMIA\ImiaBaseExt\Utility\File $extTablesCacheFile
     * @return array
     */
    public function build($metadata, $extTablesCacheFile)
    {
        $tcaCacheFiles = ['ext_tables' => &$extTablesCacheFile];
        foreach (['DB.xlf', 'db.xlf', 'DB.xml', 'db.xml', 'locallang_db.xml'] as $langFile) {
            if (file_exists($this->extPath . 'Resources/Private/Language/' . $langFile)) {
                $this->extLangFile = $langFile;
                break;
            }
        }

        foreach ($metadata as $className => $classMetadata) {
            if ($classMetadata->tcaTable && !$classMetadata->tcaTable->ignore && !$classMetadata->reflection->isAbstract()) {
                $tableName = SQL::getTableName($this->extKey, $classMetadata);
                $realClassName = substr($className, strrpos($className, '\\') + 1);
                if (strpos($realClassName, '_') === false) {
                    $dynTCAFile = $realClassName . '.php';
                } else {
                    $dynTCAFile = GeneralUtility::underscoredToUpperCamelCase($realClassName) . '.php';
                }
                $overwriteCtrl = $this->buildOverwriteCtrl($classMetadata->tcaTable, $tableName, $className);

                if ($classMetadata->tcaTable->title == null) {
                    $this->identifyLanguageVariable($className, $tableName, $classMetadata->tcaTable->title, $realClassName, '', true, true);
                }

                if ($classMetadata->tcaTable->iconfile == null) {
                    foreach ([$tableName, $realClassName] as $fileName) {
                        foreach (['svg', 'png', 'gif'] as $fileExt) {
                            if (file_exists($this->extPath . 'Resources/Public/Images/Icons/' . $fileName . '.' . $fileExt)) {
                                $classMetadata->tcaTable->iconfile = 'EXT:' . $this->extKey . '/Resources/Public/Images/Icons/' . $fileName . '.' . $fileExt;
                                break;
                            }
                        }
                    }
                }

                $searchFields = [];
                foreach ($classMetadata->propertyMetadata as $property => $propertyMetadata) {
                    if ($propertyMetadata->tcaSearchField) {
                        $searchFields[] = SQL::getColumnName($propertyMetadata);
                    }
                }
                if (count($searchFields)) {
                    if ($classMetadata->tcaTable->searchFields && is_array($classMetadata->tcaTable->searchFields)) {
                        ArrayUtility::mergeRecursiveWithOverrule($classMetadata->tcaTable->searchFields, $searchFields);
                    } else {
                        $classMetadata->tcaTable->searchFields = $searchFields;
                    }
                }

                $extTablesCacheFile->addContents(LF .
                    '// ******************************************************************' . LF .
                    '// TCA for ' . $className . LF .
                    '// (' . $tableName . ')' . LF .
                    '// ******************************************************************' . LF);

                if (array_key_exists($tableName, $this->runBefore)) {
                    foreach ($this->runBefore[$tableName] as $run) {
                        $extTablesCacheFile->addContents($run . ';' . LF);
                    }
                }

                if (strpos($tableName, 'tx_' . File::flatten($this->extKey)) === 0) {
                    // is a extension table (save!)
                    $extTablesCacheFile->addContents(
                        '$GLOBALS[\'TCA\'][\'' . $tableName . '\'] = [' . LF .
                        '    \'ctrl\' => [' . LF .
                        $this->buildCtrl($classMetadata->tcaTable, 2, $className) .
                        '        \'dynamicConfigFile\' => ' . DynamicConfiguration::getExtPath($extTablesCacheFile->getPath() . $dynTCAFile) . ',' . LF .
                        '    ]' . LF .
                        '];'
                    );
                } else {
                    // could be a table from another extension ore the core (add overwrite if/else)
                    $extTablesCacheFile->addContents(
                        'if (array_key_exists(\'' . $tableName . '\', $GLOBALS[\'TCA\'])) {' . LF .
                        $overwriteCtrl .
                        $this->buildNewOverwriteColumns($classMetadata, $tableName, $realClassName, $className) .
                        '} else {' . LF .
                        '    $GLOBALS[\'TCA\'][\'' . $tableName . '\'] = [' . LF .
                        '        \'ctrl\' => [' . LF .
                        $this->buildCtrl($classMetadata->tcaTable, 3, $className) .
                        '            \'dynamicConfigFile\' => ' . DynamicConfiguration::getExtPath($extTablesCacheFile->getPath() . $dynTCAFile) . ',' . LF .
                        '        ]' . LF .
                        '    ];' . LF .
                        '}'
                    );
                }

                if ($classMetadata->tcaTable->allowTableOnStandardPages) {
                    $extTablesCacheFile->addContents(LF .
                        '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(\'' . $tableName . '\');');
                }

                $addToAllTCATypes = [];
                if ($classMetadata->tcaAddPaletteToTypes) {
                    if ($classMetadata->tcaAddPaletteToTypes->label === null) {
                        $this->identifyLanguageVariable(
                            $className,
                            $tableName,
                            $classMetadata->tcaAddPaletteToTypes->label,
                            $classMetadata->tcaAddPaletteToTypes->palette,
                            $realClassName
                        );
                    }

                    $addToAllTCATypes[str_pad((int)$classMetadata->tcaAddPaletteToTypes->priority, 5, '0', STR_PAD_LEFT) . '_' . $classMetadata->tcaAddPaletteToTypes->label . '_' . spl_object_hash($classMetadata)] =
                        '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(' .
                        '\'' . $tableName . '\', ' .
                        '\'' . ($classMetadata->tcaAddPaletteToTypes->createTab ? '--div--;' . $this->getReplacedString($classMetadata->tcaAddPaletteToTypes->createTab, $className) . ',' : '') .
                        '--palette--;' . $this->getReplacedString($classMetadata->tcaAddPaletteToTypes->label, $className) . ';' . $classMetadata->tcaAddPaletteToTypes->palette . '\', ' .
                        '\'' . $classMetadata->tcaAddPaletteToTypes->types . '\'' .
                        ($classMetadata->tcaAddPaletteToTypes->position ? ', \'' . $classMetadata->tcaAddPaletteToTypes->position . '\'' : '') .
                        ');';
                }
                if ($classMetadata->tcaAddPalettesToTypes) {
                    if ($classMetadata->tcaAddPalettesToTypes->palettes) {
                        foreach ($classMetadata->tcaAddPalettesToTypes->palettes as $key => $palette) {
                            $palette = (object)$palette;
                            if ($palette->label === null) {
                                $this->identifyLanguageVariable(
                                    $className,
                                    $tableName,
                                    $palette->label,
                                    $palette->palette,
                                    $realClassName
                                );
                            }

                            $addToAllTCATypes[str_pad((int)$palette->priority, 5, '0', STR_PAD_LEFT) . '_' . $key . '_' . $this->getReplacedString($palette->label, $className) . '_' . spl_object_hash($classMetadata)] =
                                '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(' .
                                '\'' . $tableName . '\', ' .
                                '\'' . ($palette->createTab ? '--div--;' . $this->getReplacedString($palette->createTab, $className) . ',' : '') .
                                '--palette--;' . $this->getReplacedString($palette->label, $className) . ';' . $palette->palette . '\', ' .
                                '\'' . $palette->types . '\'' .
                                ($palette->position ? ', \'' . $palette->position . '\'' : '') .
                                ');';
                        }
                    }
                }

                /**
                 * @var string $property
                 * @var PropertyMetaData $propertyMetadata
                 */
                foreach ($classMetadata->propertyMetadata as $property => $propertyMetadata) {
                    if ($propertyMetadata->tcaAddToTypes && ($propertyMetadata->tcaAddToTypes->heredity || $classMetadata->name == $propertyMetadata->class)) {
                        $addToAllTCATypes[str_pad((int)$propertyMetadata->tcaAddToTypes->priority, 5, '0', STR_PAD_LEFT) . '_' . SQL::getColumnName($propertyMetadata) . '_' . spl_object_hash($propertyMetadata)] =
                            '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(' .
                            '\'' . $tableName . '\', ' .
                            '\'' . ($propertyMetadata->tcaAddToTypes->createTab ? '--div--;' . $this->getReplacedString($propertyMetadata->tcaAddToTypes->createTab, $className) . ',' : '') .
                            SQL::getColumnName($propertyMetadata) . ($propertyMetadata->tcaAddToTypes->options ?: '') . '\', ' .
                            '\'' . $propertyMetadata->tcaAddToTypes->types . '\'' .
                            ($propertyMetadata->tcaAddToTypes->position ? ', \'' . $propertyMetadata->tcaAddToTypes->position . '\'' : '') .
                            ');';
                    }
                    if ($propertyMetadata->tcaAddToPalette && ($propertyMetadata->tcaAddToPalette->heredity || $classMetadata->name == $propertyMetadata->class)) {
                        if ($propertyMetadata->tcaAddToPalette->palette) {
                            $addToAllTCATypes[str_pad((int)$propertyMetadata->tcaAddToPalette->priority, 5, '0', STR_PAD_LEFT) . '_' . SQL::getColumnName($propertyMetadata) . '_' . spl_object_hash($propertyMetadata)] =
                                '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(' .
                                '\'' . $tableName . '\', ' .
                                '\'' . $propertyMetadata->tcaAddToPalette->palette . '\', ' .
                                '\'' . ($propertyMetadata->tcaAddToPalette->linebreakBefore ? '--linebreak--,' : '') . SQL::getColumnName($propertyMetadata) . ($propertyMetadata->tcaAddToPalette->linebreakAfter ? ',--linebreak--' : '') . '\'' .
                                ($propertyMetadata->tcaAddToPalette->position ? ', \'' . $propertyMetadata->tcaAddToPalette->position . '\'' : '') .
                                ');';
                        }
                        if ($propertyMetadata->tcaAddToPalette->palettes) {
                            foreach ($propertyMetadata->tcaAddToPalette->palettes as $palette) {
                                $addToAllTCATypes[str_pad((int)$propertyMetadata->tcaAddToPalette->priority, 5, '0', STR_PAD_LEFT) . '_' . SQL::getColumnName($propertyMetadata) . '_' . spl_object_hash($propertyMetadata) . '_' . $palette] =
                                    '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(' .
                                    '\'' . $tableName . '\', ' .
                                    '\'' . $palette . '\', ' .
                                    '\'' . ($propertyMetadata->tcaAddToPalette->linebreakBefore ? '--linebreak--,' : '') . SQL::getColumnName($propertyMetadata) . ($propertyMetadata->tcaAddToPalette->linebreakAfter ? ',--linebreak--' : '') . '\'' .
                                    ($propertyMetadata->tcaAddToPalette->position ? ', \'' . $propertyMetadata->tcaAddToPalette->position . '\'' : '') .
                                    ');';
                            }
                        }
                    }
                }

                $addToAllTCATypesStr = '';
                if (count($addToAllTCATypes) > 0) {
                    ksort($addToAllTCATypes);
                    $addToAllTCATypesStr = LF . implode(LF, $addToAllTCATypes);
                    $extTablesCacheFile->addContents($addToAllTCATypesStr);
                }

                $tcaCacheFiles['ext_tca_' . File::flatten($realClassName)] =
                    new File($dynTCAFile, $extTablesCacheFile->getPath(),
                        $this->buildDynFile($classMetadata, $tableName, $realClassName, $className, $addToAllTCATypesStr));

                if (array_key_exists($tableName, $this->run)) {
                    foreach ($this->run[$tableName] as $run) {
                        $extTablesCacheFile->addContents(LF . $run . ';');
                    }
                }
            }
        }

        return $tcaCacheFiles;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Annotation\TCA\Table $tcaTable
     * @param int $indent
     * @param string $className
     * @return string
     */
    private function buildCtrl($tcaTable, $indent = 2, $className)
    {
        $ctrl = '';
        foreach (get_object_vars($tcaTable) as $property => $value) {
            if (!in_array($property, ['types', 'palettes', 'showRecordFieldList', 'allowTableOnStandardPages', 'ignore'])) {
                if ($value !== null || array_key_exists($property, $tcaTable::$defaults)) {
                    if ($value === null) {
                        $value = $tcaTable::$defaults[$property];
                    }
                    if (in_array($property, $tcaTable::$implodes)) {
                        $value = implode(',', $value);
                    }

                    $phpValue = $this->getValueForPhp($value, 4, $className);
                    if ($property && $property == 'iconfile') {
                        $phpValue = $this->getValueForPhp($value, 0, $className);
                    }

                    $ctrl .= str_pad('', $indent * 4) . '\'' . $property . '\' => ' . $phpValue . ',' . LF;
                }
            }
        }

        return $ctrl;
    }

    /**
     * @param \IMIA\ImiaBaseExt\Annotation\TCA\Table $tcaTable
     * @param string $tableName
     * @param string $className
     * @return string
     */
    private function buildOverwriteCtrl($tcaTable, $tableName, $className)
    {
        $ctrl = '';
        foreach (get_object_vars($tcaTable) as $property => $value) {
            if (!in_array($property, ['showRecordFieldList', 'allowTableOnStandardPages', 'ignore'])) {
                if ($value !== null) {
                    if (in_array($property, $tcaTable::$implodes)) {
                        $value = implode(',', $value);
                    }

                    $propertyVariable = '$GLOBALS[\'TCA\'][\'' . $tableName . '\']';
                    if (!in_array($property, ['types', 'palettes', 'showRecordFieldList'])) {
                        $propertyVariable .= '[\'ctrl\']';
                    }

                    if (in_array($property, ['types', 'palettes'])) {
                        $newValue = [];
                        foreach ($value as $key => $realValues) {
                            if ($property == 'palettes') {
                                $newValue[(string)$key] = ['showitem' => $realValues, 'canNotCollapse' => true];
                            } else {
                                $newValue[(string)$key] = ['showitem' => $realValues];
                            }
                        }
                        $value = $newValue;
                    }

                    $ctrl .= $this->buildOverwriteProperty($propertyVariable, $property, $value, 1, $className);
                }
            }
        }

        return $ctrl;
    }

    /**
     * @param string $propertyVariable
     * @param string $property
     * @param mixed $value
     * @param integer $indent
     * @param string $className
     * @return string
     */
    private function buildOverwriteProperty($propertyVariable, $property, $value, $indent = 1, $className)
    {
        $propertyCode = '';
        $phpValue = $this->getValueForPhp($value, 4, $className);

        if ($property && $property == 'iconfile') {
            $phpValue = $this->getValueForPhp($value, 0, $className);
        }

        if (is_array($value)) {
            $setValues = '';
            foreach ($value as $key => $val) {
                $setValues .= $this->buildOverwriteProperty($propertyVariable . '[\'' . $property . '\']', $key, $val, $indent + 1, $className);
            }

            $propertyCode .=
                str_pad('', $indent * 4) . 'if (array_key_exists(\'' . $property . '\', ' . $propertyVariable . ')) {' . LF .
                $setValues .
                str_pad('', $indent * 4) . '} else {' . LF .
                str_pad('', ($indent + 1) * 4) . $propertyVariable . '[\'' . $property . '\'] = ' . $phpValue . ';' . LF .
                str_pad('', $indent * 4) . '}' . LF;
        } else {
            $propertyCode .= str_pad('', $indent * 4) . $propertyVariable . '[\'' . $property . '\'] = ' . $phpValue . ';' . LF;
        }

        return $propertyCode;
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param string $tableName
     * @param string $realClassName
     * @param string $className
     * @return string
     */
    private function buildNewOverwriteColumns($classMetadata, $tableName, $realClassName, $className)
    {
        $columns = '';
        foreach ($classMetadata->propertyMetadata as $property => $propertyMetadata) {
            if ($propertyMetadata->tcaColumn) {
                $name = SQL::getColumnName($propertyMetadata);
                if ($this->checkLanguageVariable($propertyMetadata->tcaColumn->label, $property, $realClassName, $name)) {
                    $propertyMetadata->tcaColumn->label = null;
                }
                $overwriteColumn = $this->buildOverwriteColumn($propertyMetadata, $tableName, $className);

                if ($propertyMetadata->tcaColumn->label === null) {
                    if (!in_array(get_class($propertyMetadata->tcaColumn), self::$tcaColumnNoAutoLabel)) {
                        $this->identifyLanguageVariable(
                            $className,
                            $tableName,
                            $propertyMetadata->tcaColumn->label,
                            $property,
                            $realClassName,
                            false,
                            true
                        );
                    }

                    $langTest = $this->languageValue($propertyMetadata->tcaColumn->label, $property, $realClassName);
                    if (!$langTest) {
                        $reflection = $classMetadata->reflection;
                        while ($reflection->getParentClass() && !$langTest) {
                            $reflection = $reflection->getParentClass();
                            $parentRealClassName = substr($reflection->name, strrpos($reflection->name, '\\') + 1);

                            $this->identifyLanguageVariable(
                                $className,
                                $tableName,
                                $propertyMetadata->tcaColumn->label,
                                $property,
                                $parentRealClassName,
                                false,
                                true
                            );
                        }
                    }
                }

                $columns .=
                    '    if (array_key_exists(\'' . $name . '\', $GLOBALS[\'TCA\'][\'' . $tableName . '\'][\'columns\'])) {' . LF .
                    $overwriteColumn .
                    '    } else {' . LF .
                    '        $tempColumn = [' . LF .
                    $this->buildColumn($property, $propertyMetadata, $tableName, 3, $className) .
                    '        ];' . LF .
                    '        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(\'' . $tableName . '\', $tempColumn);' . LF .
                    '    }' . LF;
            } elseif ($propertyMetadata->sqlColumn) {
                $name = SQL::getColumnName($propertyMetadata);
                if ($name != GeneralUtility::camelCaseToLowerCaseUnderscored($property)) {
                    $columns .=
                        '    $GLOBALS[\'TCA\'][\'' . $tableName . '\'][\'columns\'][\'' . $name . '\'][\'mapOnProperty\'] = "' . $property . '";' . LF;
                }
            }
        }

        return $columns;
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param string $tableName
     * @param string $realClassName
     * @param string $className
     * @param string $addToAllTCATypesStr
     * @return string
     */
    private function buildDynFile($classMetadata, $tableName, $realClassName, $className, $addToAllTCATypesStr = '')
    {
        $tca = '<?php' . LF .
            'if (!defined (\'TYPO3_MODE\')) {' . LF .
            '    die (\'Access denied.\');' . LF .
            '}' . LF .
            LF .
            '$TCA[\'' . $tableName . '\'] = [' . LF .
            '    \'ctrl\' => $TCA[\'' . $tableName . '\'][\'ctrl\'],' . LF .
            '    \'interface\' => [' . LF .
            '        \'showRecordFieldList\' => ' . $this->buildShowRecordFieldList($classMetadata, $className) . ',' . LF .
            '    ],' . LF .
            '    \'types\' => ' . $this->buildTypes($classMetadata, $className) . ',' . LF .
            '    \'palettes\' => ' . $this->buildPalettes($classMetadata, $className) . ',' . LF .
            '    \'columns\' => [' . LF;

        foreach ($classMetadata->propertyMetadata as $property => $propertyMetadata) {
            if ($propertyMetadata->tcaColumn) {
                $name = SQL::getColumnName($propertyMetadata);
                if ($this->checkLanguageVariable($propertyMetadata->tcaColumn->label, $property, $realClassName, $name)) {
                    $propertyMetadata->tcaColumn->label = null;
                }

                if ($propertyMetadata->tcaColumn->label === null) {
                    if (!in_array(get_class($propertyMetadata->tcaColumn), self::$tcaColumnNoAutoLabel)) {
                        $this->identifyLanguageVariable(
                            $className,
                            $tableName,
                            $propertyMetadata->tcaColumn->label,
                            $property,
                            $realClassName
                        );
                        $this->languageValue($propertyMetadata->tcaColumn->label, $property, $realClassName);
                    }
                }

                $tca .= $this->buildColumn($property, $propertyMetadata, $tableName, 2, $className);
            }
        }

        $tca .=
            '   ],' . LF .
            '];' . $addToAllTCATypesStr;

        return $tca;
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param string $className
     * @return string
     */
    private function buildShowRecordFieldList($classMetadata, $className)
    {
        $tcaTable = $classMetadata->tcaTable;
        if ($tcaTable->showRecordFieldList !== null) {
            $showRecordFieldList = $tcaTable->showRecordFieldList;
        } else {
            $columns = $this->getColumnsToShow($classMetadata);
            $showRecordFieldList = implode(', ', $columns);
        }

        return $this->getValueForPhp($showRecordFieldList, 0, $className);
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param string $className
     * @return string
     */
    private function buildTypes($classMetadata, $className)
    {
        $types = [];
        if ($classMetadata->tcaTable->types) {
            foreach ($classMetadata->tcaTable->types as $key => $type) {
                $types[(string)$key] = ['showitem' => $type];
            }
        } else {
            $types[] = ['showitem' => implode(', ', $this->getColumnsToShow($classMetadata))];
        }

        return $this->getValueForPhp($types, 2, $className);
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param string $className
     * @return string
     */
    private function buildPalettes($classMetadata, $className)
    {
        $palettes = [];
        if ($classMetadata->tcaTable->palettes) {
            foreach ($classMetadata->tcaTable->palettes as $key => $palette) {
                $palettes[(string)$key] = ['showitem' => $palette, 'canNotCollapse' => true];
            }
        }

        return $this->getValueForPhp($palettes, 2, $className);
    }

    /**
     * @param ClassMetadata $classMetadata
     * @return array
     */
    private function getColumnsToShow($classMetadata)
    {
        $columns = [];
        foreach ($classMetadata->propertyMetadata as $property => $propertyMetadata) {
            $column = $propertyMetadata->tcaColumn;
            if ($column && get_class($column) !== 'IMIA\ImiaBaseExt\Annotation\TCA\Passthrough') {
                $columns[] = SQL::getColumnName($propertyMetadata);
            }
        }

        return $columns;
    }

    /**
     * @param string $columnProperty
     * @param PropertyMetadata $propertyMetadata
     * @param string $tableName
     * @param int $indent
     * @param string $className
     * @return string
     */
    private function buildColumn($columnProperty, $propertyMetadata, $tableName, $indent = 2, $className)
    {
        $column = $propertyMetadata->tcaColumn;
        $name = SQL::getColumnName($propertyMetadata);

        $tca = str_pad('', $indent * 4) . '\'' . $name . '\' => [' . LF;

        $config = [];
        if (isset($column->eval)) {
            if (!is_array($column->eval)) {
                $eval = GeneralUtility::trimExplode(',', $column->eval);
            } else {
                $eval = $column->eval;
            }

            if (in_array('null', $eval)) {
                if (!isset($column->default) || !$column->default && !isset($column::$defaults['default'])) {
                    $column::$defaults['default'] = null;
                }
            }
        }
        foreach (get_object_vars($column) as $property => $value) {
            if (array_key_exists($property, $column::$fixed)) {
                $value = $column::$fixed[$property];
            }

            if ($value !== null || array_key_exists($property, $column::$defaults)) {
                if ($value === null) {
                    $value = $column::$defaults[$property];
                } elseif (is_array($value) && array_key_exists($property, $column::$defaults) && is_array($column::$defaults[$property])) {
                    $newValue = $column::$defaults[$property];
                    ArrayUtility::mergeRecursiveWithOverrule($newValue, $value);
                    $value = $newValue;
                }

                if (in_array($property, $column::$implodes)) {
                    $value = implode(',', $value);
                }

                if (is_string($value)) {
                    $value = $this->replaceAdditionalMarkers($value, $name, $tableName);
                }

                if ($property == 'items' && !is_array($value)) {
                    $items = null;
                    try {
                        @eval('$items = ' . $value . ';');
                    } catch (\Exception $e) {
                    }

                    $value = [];
                    if ($items && is_array($items)) {
                        foreach ($items as $key => $item) {
                            $icon = null;
                            if (is_array($item)) {
                                $icon = $item[1];
                                $item = $item[0];
                            }

                            if ($icon) {
                                $value[] = [(string)$item, $key, $icon];
                            } else {
                                $value[] = [(string)$item, $key];
                            }
                        }
                    }
                }

                if (in_array($property, $column::$config)) {
                    $config[$property] = $value;
                } else {
                    $tca .= str_pad('', ($indent + 1) * 4) . '\'' . $property . '\' => ' . $this->getValueForPhp($value, $indent + 2, $className) . ',' . LF;
                }
            }
        }

        $configValue = $this->getValueForPhp($config, $indent + 2, $className);
        if (property_exists($column, 'configFunction')) {
            $param = '';
            if (property_exists($column, 'configParameter')) {
                $param = ', \'' . $column::$configParameter . '\'';
            }
            $allowedExtensions = null;
            if (property_exists($column, 'allowedExtensions')) {
                $allowedExtensions = $column->allowedExtensions;
            }
            if ($allowedExtensions) {
                $param = ', \'' . $allowedExtensions . '\'';
            }

            $configValue = '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::' . $column::$configFunction . '(\'' . $name . '\', ' . $configValue . $param . ')';
        } elseif (method_exists($column, 'config')) {
            /** @var Copy $column */
            $config = $column->config($config);
            $configValue = $this->getValueForPhp($config, $indent + 2, $className);
        }

        if (property_exists($column, 'runBefore')) {
            $this->runBefore[$tableName][$name] = $this->replaceAdditionalMarkers($column::$runBefore, $name, $tableName);
        }
        if (property_exists($column, 'run')) {
            $this->run[$tableName][$name] = $this->replaceAdditionalMarkers($column::$run, $name, $tableName);
        }

        $tca .= str_pad('', ($indent + 1) * 4) . '\'config\' => ' . $configValue . ',' . LF;

        $name = SQL::getColumnName($propertyMetadata);
        if ($name != GeneralUtility::camelCaseToLowerCaseUnderscored($columnProperty)) {
            $tca .= str_pad('', ($indent + 1) * 4) . '\'mapOnProperty\' => "' . $columnProperty . '",' . LF;
        }

        $tca .= str_pad('', $indent * 4) . '],' . LF;

        return $tca;
    }

    /**
     * @param PropertyMetadata $propertyMetadata
     * @param string $tableName
     * @param string $className
     * @return string
     */
    private function buildOverwriteColumn($propertyMetadata, $tableName, $className)
    {
        $column = $propertyMetadata->tcaColumn;
        $name = SQL::getColumnName($propertyMetadata);

        $columnVariable = '$GLOBALS[\'TCA\'][\'' . $tableName . '\'][\'columns\'][\'' . $name . '\']';

        $config = [];
        $tca = '';
        if (isset($column->eval)) {
            if (!is_array($column->eval)) {
                $eval = GeneralUtility::trimExplode(',', $column->eval);
            } else {
                $eval = $column->eval;
            }

            if (in_array('null', $eval)) {
                if (!isset($column->default) || !$column->default && !isset($column::$defaults['default'])) {
                    $column::$defaults['default'] = null;
                }
            }
        }
        foreach (get_object_vars($column) as $property => $value) {
            if (array_key_exists($property, $column::$fixed)) {
                $value = $column::$fixed[$property];
            }

            if ($property == 'defaultExtras' && $value === null) {
                $value = $column::$defaults[$property];
            }

            if ($value !== null) {
                if (in_array($property, $column::$implodes)) {
                    $value = implode(',', $value);
                }
                if (is_string($value)) {
                    $value = str_replace(['###CURRENT_TABLE###'], [$tableName], $value);
                }

                if (in_array($property, $column::$config)) {
                    $config[$property] = $value;
                } else {
                    $tca .= $this->buildOverwriteProperty($columnVariable, $property, $value, 2, $className);
                }
            }
        }

        if (property_exists($column, 'configFunction')) {
            $param = '';
            if (property_exists($column, 'configParameter')) {
                $param = ', \'' . $column::$configParameter . '\'';
            }
            $allowedExtensions = null;
            if (property_exists($column, 'allowedExtensions')) {
                $allowedExtensions = $column->allowedExtensions;
            }
            if ($allowedExtensions) {
                $param = ', \'' . $allowedExtensions . '\'';
            }

            $tca .= '        ' . $columnVariable . '[\'config\'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::' .
                $column::$configFunction . '(\'' . $name . '\', ' . $this->getValueForPhp($config, 3, $className) . $param . ');' . LF;
        } elseif (method_exists($column, 'config')) {
            /** @var Copy $column */
            $config = $column->config($config);
            $tca .= $this->buildOverwriteProperty($columnVariable, 'config', $config, 2, $className);
        } else {
            $tca .= $this->buildOverwriteProperty($columnVariable, 'config', $config, 2, $className);
        }

        return $tca;
    }

    /**
     * @param mixed $value
     * @param integer $indent
     * @param string $className
     * @return string
     */
    private function getValueForPhp($value, $indent = 0, $className)
    {
        if ($value === null) {
            $newValue = 'null';
        } elseif (is_string($value)) {
            // enable php code (e.g. for dates range: mktime)
            if (preg_match('/^<\?(php)? (.+)[;]? \?>$/ism', trim($value), $match)) {
                $newValue = $match[2];
            } else {
                $value = $this->getReplacedString($value, $className);
                $newValue = '\'' . str_replace(["'", '\\'], ["\'", '\\'], $value) . '\'';
            }
        } elseif (is_bool($value)) {
            $newValue = $value ? 'true' : 'false';
        } elseif (is_array($value)) {
            $newValue = '[' . LF;
            foreach ($value as $key => $keyVal) {
                $newValue .= str_pad('', $indent * 4) . $this->getValueForPhp($key, 0, $className) . ' => ' .
                    $this->getValueForPhp($keyVal, $indent + 1, $className) . ',' . LF;
            }
            $newValue .= str_pad('', ($indent - 1) * 4) . ']';
        } else {
            $newValue = $value;
        }

        return $newValue;
    }

    /**
     * @param string $string
     * @param string $className
     * @return string
     */
    private function getReplacedString($string, $className)
    {
        // replace "LLL::" with default ' . $this->extLangFile . ' path
        if (preg_match_all('/LLL::([^,;]+)/ism', trim($string), $matches)) {
            foreach ($matches[1] as $key => $match) {
                $value = '[' . $match . ']';

                $langString = '';
                if ($this->extLangFile) {
                    $langString = 'LLL:EXT:' . $this->extKey . '/Resources/Private/Language/' . $this->extLangFile . ':' . $match;
                }

                $parentClassName = get_parent_class($className);
                while (!$this->getLanguageService()->sL($langString) && $parentClassName) {
                    preg_match('/^([^\\\\]+)\\\\([^\\\\]+).*?$/ism', $parentClassName, $extMatch);
                    if ($extMatch[1] && $extMatch[2]) {
                        if ($extMatch[1] == 'TYPO3') {
                            break;
                        } else {
                            $extKey = GeneralUtility::camelCaseToLowerCaseUnderscored($extMatch[2]);
                            if ($extKey == 'imia_base_ext') {
                                break;
                            } else {
                                if (!array_key_exists($extKey, $this->extLangFiles)) {
                                    foreach (['DB.xlf', 'db.xlf', 'DB.xml', 'db.xml', 'locallang_db.xml'] as $langFile) {
                                        if (file_exists(ExtensionManagementUtility::extPath($extKey) . 'Resources/Private/Language/' . $langFile)) {
                                            $this->extLangFiles[$extKey] = $langFile;
                                            break;
                                        }
                                    }
                                }

                                if (isset($this->extLangFiles[$extKey])) {
                                    $langString = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/' . $this->extLangFiles[$extKey] . ':' . $match;
                                }
                            }
                        }
                    }
                    $parentClassName = get_parent_class($parentClassName);
                }

                if ($langString && $this->getLanguageService()->sL($langString)) {
                    $value = $langString;
                }
                $string = str_replace($matches[0][$key], $value, $string);
            }
            // replace "EXTREL::" with extRelPath
        } elseif (preg_match_all('/EXTREL::([^,]+)/ism', trim($string), $matches)) {
            foreach ($matches[1] as $key => $match) {
                $string = str_replace($matches[0][$key], ExtensionManagementUtility::extRelPath($this->extKey) . $match, $string);
            }
        } elseif (preg_match_all('/EXTREL:([^:]+):([^,]+)/ism', trim($string), $matches)) {
            foreach ($matches[2] as $key => $match) {
                $string = str_replace($matches[0][$key], ExtensionManagementUtility::extRelPath($matches[1][$key]) . $match, $string);
            }
            // replace "EXT::" with extPath
        } elseif (preg_match_all('/EXT::([^,]+)/ism', trim($string), $matches)) {
            foreach ($matches[1] as $key => $match) {
                $string = str_replace($matches[0][$key], ExtensionManagementUtility::extPath($this->extKey) . $match, $string);
            }
        }

        return $string;
    }

    /**
     * @param string $value
     * @param string $name
     * @param string $tableName
     * @return string
     */
    private function replaceAdditionalMarkers($value, $name, $tableName)
    {
        return str_replace(['###CURRENT_TABLE###', '###CURRENT_COLUMN###', '###EXTKEY###'], [$tableName, $name, $this->extKey], $value);
    }

    /**
     * @param string $className
     * @param string $tableName
     * @param string $value
     * @param string $name
     * @param string $realClassName
     * @param bool $tableTitle
     * @param bool $languageValueConversion
     */
    private function identifyLanguageVariable($className, $tableName, &$value, $name, $realClassName = '', $tableTitle = false, $languageValueConversion = false)
    {
        if ($this->extLangFile) {
            $value = 'LLL:EXT:' . $this->extKey . '/Resources/Private/Language/' .
                $this->extLangFile . ':' . $this->getLanguageVariable($name, $realClassName);
        } else {
            $value = '';
        }

        if ($languageValueConversion) {
            if ($tableTitle) {
                $this->languageValue($value, $tableName);
            } else {
                $this->languageValue($value, $name);
            }
        }

        $parentClassName = get_parent_class($className);
        while ((!$value || $this->getLabelWithoutTranslation($tableTitle ? $tableName : $name) == $value) && $parentClassName) {
            preg_match('/^([^\\\\]+)\\\\([^\\\\]+).*?$/ism', $parentClassName, $match);
            if ($match[1] && $match[2]) {
                if ($match[1] == 'TYPO3') {
                    break;
                } else {
                    $extKey = GeneralUtility::camelCaseToLowerCaseUnderscored($match[2]);
                    if ($extKey == 'imia_base_ext') {
                        break;
                    } else {
                        if (!array_key_exists($extKey, $this->extLangFiles)) {
                            foreach (['DB.xlf', 'db.xlf', 'DB.xml', 'db.xml', 'locallang_db.xml'] as $langFile) {
                                if (file_exists(ExtensionManagementUtility::extPath($extKey) . 'Resources/Private/Language/' . $langFile)) {
                                    $this->extLangFiles[$extKey] = $langFile;
                                    break;
                                }
                            }
                        }

                        if (isset($this->extLangFiles[$extKey])) {
                            $value = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/' .
                                $this->extLangFiles[$extKey] . ':' . $this->getLanguageVariable($name, $realClassName);

                            if ($languageValueConversion) {
                                if ($tableTitle) {
                                    $this->languageValue($value, $tableName);
                                } else {
                                    $this->languageValue($value, $name);
                                }
                            }
                        }
                    }
                }
            }
            $parentClassName = get_parent_class($parentClassName);
        }
    }

    /**
     * @param string $langString
     * @param string $name
     * @param string $className
     * @return string
     */
    private function languageValue(&$langString, $name, $className = null)
    {
        if ($this->getLanguageService()) {
            if ($langString === null || $this->getLanguageService()->sL($langString) === '') {
                $langString = $this->getLabelWithoutTranslation($name, $className);

                return false;
            }

            if ($this->getLanguageService()->sL($langString) === 'none') {
                $langString = '';
            }
        }

        return true;
    }

    /**
     * @param string $label
     * @param string $property
     * @param string $className
     * @param string $name
     * @return bool
     */
    protected function checkLanguageVariable($label, $property, $className, $name)
    {
        if ($label == $this->getLabelWithoutTranslation($property, $className) || $label == $this->getLabelWithoutTranslation($name)
            || preg_match('/^\[.*_' . $name . '\]$/ism', $label)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    protected function getLabelWithoutTranslation($name, $className = null)
    {
        if ($className && $this->isSysField($name)) {
            return '[' . GeneralUtility::camelCaseToLowerCaseUnderscored($name) . ']';
        } else {
            return '[' . $this->getLanguageVariable($name, $className) . ']';
        }
    }

    /**
     * @param string $name
     * @param string $className
     * @return string
     */
    protected function getLanguageVariable($name, $className = null)
    {
        return ($className ? File::flatten($className) . '_' : '') . File::flatten($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function isSysField($name)
    {
        return in_array(GeneralUtility::camelCaseToLowerCaseUnderscored($name), [
            'uid', 'pid', 'tstamp', 'crdate', 'cruser_id', 'deleted', 'hidden', 'sorting', 'starttime',
            'endtime', 'sys_language_uid', 'l10n_parent', 'l10n_diffsource', 't3_origuid',
        ]);
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}