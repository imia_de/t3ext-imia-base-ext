<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility\DynamicConfiguration;

use IMIA\ImiaBaseExt\Metadata\ClassMetadata;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Utility\DynamicConfiguration
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScript
{
    /**
     * @var string
     */
    private $extKey;

    /**
     * @param string $extKey
     */
    public function __construct($extKey)
    {
        $this->extKey = $extKey;
    }

    /**
     * @param ClassMetadata $metadata
     * @return string
     */
    public function build($metadata)
    {
        $persistence = $this->buildPersistence($metadata);
        $typoScript = '### EXTENSION: ' . $this->extKey . ' ###' . LF . LF;

        foreach ([
                     'config.tx_extbase',
                     'plugin.tx_' . \IMIA\ImiaBaseExt\Utility\File::flatten($this->extKey),
                     'module.tx_' . \IMIA\ImiaBaseExt\Utility\File::flatten($this->extKey),
                 ] as $tsObject) {
            $typoScript .=
                $tsObject . ' {' . LF .
                $persistence . LF .
                '}' . LF . LF;
        }

        return $typoScript;
    }

    /**
     * @param ClassMetadata $metadata
     * @return string
     */
    private function buildPersistence($metadata)
    {
        $typoScript =
            '    persistence {' . LF .
            '        classes {' . LF;

        foreach ($metadata as $className => $classMetadata) {
            if ($classMetadata->sqlTable && !$classMetadata->reflection->isAbstract()) {
                $tableName = SQL::getTableName($this->extKey, $classMetadata);
                $typoScript .=
                    '            ' . $className . ' {' . LF .
                    '                mapping {' . LF .
                    '                    tableName = ' . $tableName . LF .
                    $this->buildColumns($classMetadata) .
                    '                }' . LF .
                    '            }' . LF;
            }
        }
        $typoScript .=
            '        }' . LF .
            '    }' . LF;

        return $typoScript;
    }

    /**
     * @param ClassMetadata $metadata
     * @return string
     */
    private function buildColumns($metadata)
    {
        $columns = [];
        foreach ($metadata->propertyMetadata as $property => $propertyMetadata) {
            if ($propertyMetadata->sqlColumn) {
                $name = SQL::getColumnName($propertyMetadata);
                if ($name != GeneralUtility::camelCaseToLowerCaseUnderscored($property)
                    || in_array($name, ['crdate', 'hidden', 'starttime', 'endtime', 'deleted', 'sorting'])
                ) {
                    $columns[$name] = $property;
                }
            }
        }

        $typoScript = '';
        if (count($columns) > 0) {
            $columnTypoScript = '';
            foreach ($columns as $name => $property) {
                $columnTypoScript .= '                        ' . $name . '.mapOnProperty = ' . $property . LF;
            }

            $typoScript .=
                '                    columns {' . LF .
                $columnTypoScript .
                '                    }' . LF;
        }

        return $typoScript;
    }
}