<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * @package     imia_base_ext
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendTypoScript
{
    /**
     * @var array
     */
    private static $typoScripts = [];

    /**
     * @var ObjectManager
     */
    private static $objectManager;

    /**
     * @var PageRepository
     */
    private static $pageRepository;

    /**
     * @param integer $pageUid
     * @return array
     */
    static public function get($pageUid = null)
    {
        $pageUid = (int)$pageUid;
        if (!$pageUid) {
            $firstRootPage = self::getDb()->exec_SELECTgetSingleRow('uid', 'pages', 'is_siteroot = 1' . BackendUtility::deleteClause('pages'));
            if ($firstRootPage) {
                $pageUid = (int)$firstRootPage['uid'];
            }
        }

        if (!$pageUid) {
            self::$typoScripts[$pageUid] = [];
        } elseif (!array_key_exists($pageUid, self::$typoScripts)) {
            $rootLine = self::getPageRepository()->getRootLine($pageUid);

            if (!$GLOBALS['TSFE']) {
                $GLOBALS['TSFE'] = new \stdClass();
                $GLOBALS['TSFE']->id = $pageUid;
                $GLOBALS['TSFE']->page = ['uid' => $pageUid];
                $GLOBALS['TSFE']->rootLine = $rootLine;
                $GLOBALS['TSFE']->tmpl = (object)['rootLine' => $rootLine];

                $matchObj = self::getObjectManager()
                    ->get('TYPO3\CMS\Frontend\Configuration\TypoScript\ConditionMatching\ConditionMatcher');

                $matchObj->setPageId($pageUid);
                $matchObj->setRootline($rootLine);
            }

            $typoScript = self::getObjectManager()
                ->get('TYPO3\CMS\Core\TypoScript\ExtendedTemplateService');

            $typoScript->tt_track = 0;
            $typoScript->init();
            $typoScript->runThroughTemplates($rootLine);
            $typoScript->generateConfig();

            self::$typoScripts[$pageUid] = $typoScript->setup;

            if (get_class($GLOBALS['TSFE']) == 'stdClass') {
                unset($GLOBALS['TSFE']);
            }
        }

        return self::$typoScripts[$pageUid];
    }

    /**
     * @return DatabaseConnection
     */
    static protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return PageRepository
     */
    static protected function getPageRepository()
    {
        if (!self::$pageRepository) {
            self::$pageRepository = self::getObjectManager()
                ->get(PageRepository::class);
        }

        return self::$pageRepository;
    }

    /**
     * @return ObjectManager
     */
    static protected function getObjectManager()
    {
        if (!self::$objectManager) {
            self::$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        }

        return self::$objectManager;
    }
}