<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Copy extends Column
{
    /**
     * @param array $config
     * @return array
     */
    public function config(array $config)
    {
        $newConfig = $GLOBALS['TCA'][$this->copyTable]['columns'][$this->copyColumn]['config'];
        if (!is_array($newConfig)) {
            $newConfig = [];
        }
        ArrayUtility::mergeRecursiveWithOverrule($newConfig, $config);

        return $newConfig;
    }

    /**
     * @ConfigProperty
     *
     * @var mixed
     */
    public $copyColumn;

    /**
     * @ConfigProperty
     *
     * @var mixed
     */
    public $copyTable;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $items;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $cols;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $showIfRTE;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $itemsProcFunc;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $ds_pointerField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $ds;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $ds_tableField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $ds_pointerField_searchParent;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $ds_pointerField_searchParent_subField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $internal_type;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $allowed;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $disallowed;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $filter;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_opposite_field;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_match_fields;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_insert_fields;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_table_where;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $MM_hasUidField;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $max_size;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $uploadfolder;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $prepend_tname;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $dontRemapTablesOnCopy;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $show_thumbs;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $size;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $autoSizeMax;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $selectedListStyle;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $multiple;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $maxitems;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $minitems;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $disable_controls;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $wizards;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $appearance;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $foreign_record_defaults;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $behaviour;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $customControls;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_field;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_label;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_selector;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $foreign_selector_fieldTcaOverride;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_sortby;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_default_sortby;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table_field;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_unique;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $foreign_match_fields;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $foreign_types;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $symmetric_field;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $symmetric_label;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $symmetric_sortby;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $max;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $eval;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $is_in;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $checkbox;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $range;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $pass_content;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $rows;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $fixedRows;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $wrap;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $renderType;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $format;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $userFunc;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $parameters;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $noTableWrapping;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $itemsProcFunc_config;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $selicon_cols;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $showIconTable;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $suppress_icons;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $iconsInOptionTags;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $noIconsBelowSelect;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table_where;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table_prefix;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $foreign_table_loadIcons;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_where;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_prefix;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $neg_foreign_table_loadIcons;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_imposeValueField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $fileFolder;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $fileFolder_extList;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $fileFolder_recursions;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $allowNonIdValues;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $rootLevel;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $special;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $itemListStyle;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $renderMode;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $treeConfig;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $disableNoMatchingValueElement;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $authMode;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $authMode_enforce;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $exclusiveKeys;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $localizeReferencesAtParentLocalization;
}