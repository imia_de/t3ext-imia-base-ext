<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Categories extends Select
{
    static public $runBefore = '\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable("###EXTKEY###", "###CURRENT_TABLE###", "###CURRENT_COLUMN###")';

    /**
     * @var array
     */
    static public $defaults = [
        'foreign_table'       => 'sys_category',
        'foreign_table_where' => ' ORDER BY sys_category.title ASC',
        'MM'                  => 'sys_category_record_mm',
        'MM_opposite_field'   => 'items',
        'MM_match_fields'     => ['tablenames' => '###CURRENT_TABLE###'],
        'size'                => 10,
        'autoSizeMax'         => 50,
        'maxitems'            => 9999,
        'renderMode'          => 'tree',
        'treeConfig'          => [
            'parentField' => 'parent',
            'appearance'  => [
                'expandAll'  => true,
                'showHeader' => true,
            ],
        ],
        'l10n_mode'           => 'exclude',
    ];

    /**
     * @var array
     */
    static public $fixed = [
        'type' => 'select',
    ];

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_label;
}