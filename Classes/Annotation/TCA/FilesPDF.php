<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class FilesPDF extends Files
{
    /**
     * @var string
     */
    static public $configParameter = 'pdf';

    /**
     * @var array
     */
    static public $defaults = [
        'foreign_types' => [
            \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN     => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT        => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE       => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO       => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO       => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                'showitem' => 'title, --palette--;;filePalette',
            ],
        ],
    ];
}