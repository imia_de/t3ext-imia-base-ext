<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
abstract class Column
{
    /**
     * @var array
     */
    static public $config = [
        'type', 'default', 'readOnly', 'size', 'max', 'eval', 'is_in', 'checkbox', 'range', 'wizards', 'cols', 'rows', 'wrap',
        'items', 'showIfRTE', 'itemsProcFunc', 'showIfRTE', 'selicon_cols', 'showIconTable', 'suppress_icons', 'iconsInOptionTags',
        'foreign_table', 'foreign_table_where', 'foreign_table_prefix', 'foreign_table_loadIcons', 'neg_foreign_table',
        'neg_foreign_table_where', 'neg_foreign_table_prefix', 'neg_foreign_table_loadIcons', 'neg_foreign_table_imposeValueField',
        'fileFolder', 'fileFolder_extList', 'fileFolder_recursions', 'allowNonIdValues', 'dontRemapTablesOnCopy', 'rootLevel',
        'MM', 'MM_opposite_field', 'MM_oppositeUsage', 'MM_match_fields', 'MM_insert_fields', 'MM_table_where', 'MM_hasUidField', 'special',
        'autoSizeMax', 'selectedListStyle', 'itemListStyle', 'renderMode', 'treeConfig', 'multiple', 'maxitems', 'minitems',
        'wizards', 'disableNoMatchingValueElement', 'authMode', 'authMode_enforce', 'exclusiveKeys', 'noIconsBelowSelect',
        'localizeReferencesAtParentLocalization', 'internal_type', 'allowed', 'disallowed', 'filter', 'max_size', 'uploadfolder',
        'prepend_tname', 'dontRemapTablesOnCopy', 'show_thumbs', 'disable_controls', 'appearance', 'pass_content', 'fixedRows',
        'userFunc', 'parameters', 'noTableWrapping', 'ds_pointerField', 'ds', 'ds_tableField', 'ds_pointerField_searchParent',
        'ds_pointerField_searchParent_subField', 'behaviour', 'customControls', 'foreign_field', 'foreign_label', 'foreign_selector',
        'foreign_selector_fieldTcaOverride', 'foreign_sortby', 'foreign_default_sortby', 'foreign_table_field', 'foreign_unique',
        'foreign_match_fields', 'foreign_types', 'symmetric_field', 'symmetric_label', 'symmetric_sortby', 'itemsProcFunc_config',
        'renderType', 'format', 'foreign_record_defaults', 'overrideChildTca', 'enableRichtext', 'enableMultiSelectFilterTextfield',
        'multiSelectFilterItems', 'fieldWizard',
    ];

    /**
     * @var array
     */
    static public $implodes = [
        'eval',
    ];

    /**
     * @var array
     */
    static public $defaults = [];

    /**
     * @var array
     */
    static public $fixed = [];

    /**
     * @var string
     */
    public $onChange;

    /**
     * @var string
     */
    public $label;

    /**
     * @var boolean
     */
    public $exclude;

    /**
     * @var string
     */
    public $l10n_mode;

    /**
     * @var string
     */
    public $l10n_display;

    /**
     * @var string
     */
    public $l10n_cat;

    /**
     * @var mixed
     */
    public $displayCond;

    /**
     * @var string
     */
    public $defaultExtras;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $type;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $default;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $readOnly;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $behaviour;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $fieldWizard;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $configFunc;
}