<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Group extends Column
{
    /**
     * @var array
     */
    static public $defaults = [
        'l10n_mode' => 'exclude',
    ];

    /**
     * @var array
     */
    static public $fixed = [
        'type' => 'group',
    ];

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $internal_type;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $allowed;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $disallowed;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $filter;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_opposite_field;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_match_fields;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_insert_fields;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_table_where;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $MM_hasUidField;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $max_size;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $uploadfolder;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $prepend_tname;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $dontRemapTablesOnCopy;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $show_thumbs;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $size;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $autoSizeMax;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $selectedListStyle;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $multiple;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $maxitems;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $minitems;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $disable_controls;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $wizards;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $appearance;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table;


}