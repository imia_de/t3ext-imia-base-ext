<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Select extends Column
{
    /**
     * @var array
     */
    static public $defaults = [
        'l10n_mode'  => 'exclude',
        'renderType' => 'selectSingle',
    ];

    /**
     * @var array
     */
    static public $fixed = [
        'type' => 'select',
    ];

    /**
     * @ConfigProperty
     *
     * @var mixed
     */
    public $items;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $itemsProcFunc;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $itemsProcFunc_config;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $showIfRTE;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $selicon_cols;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $showIconTable;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $suppress_icons;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $iconsInOptionTags;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $noIconsBelowSelect;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table_where;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $foreign_table_prefix;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $foreign_table_loadIcons;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_where;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_prefix;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $neg_foreign_table_loadIcons;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $neg_foreign_table_imposeValueField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $fileFolder;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $fileFolder_extList;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $fileFolder_recursions;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $allowNonIdValues;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $dontRemapTablesOnCopy;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $rootLevel;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_opposite_field;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_oppositeUsage;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_match_fields;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $MM_insert_fields;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $MM_table_where;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $MM_hasUidField;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $special;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $size;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $autoSizeMax;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $selectedListStyle;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $itemListStyle;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $renderMode;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $renderType;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $treeConfig;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $multiple;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $maxitems;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $minitems;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $wizards;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $disableNoMatchingValueElement;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $authMode;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $authMode_enforce;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $exclusiveKeys;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $localizeReferencesAtParentLocalization;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $enableMultiSelectFilterTextfield;

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $multiSelectFilterItems;
}