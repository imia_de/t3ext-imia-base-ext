<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("CLASS")
 */
class Table
{
    /**
     * @var array
     */
    static public $implodes = [
        'label_alt', 'requestUpdate', 'searchFields',
    ];

    /**
     * @var array
     */
    static public $defaults = [
        'label'         => 'uid',
        'dividers2tabs' => true,
    ];

    /**
     * @var bool
     */
    public $createAll;

    /**
     * @var boolean
     */
    public $ignore = false;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $label;

    /**
     * @var array
     */
    public $label_alt;

    /**
     * @var boolean
     */
    public $label_alt_force;

    /**
     * @var string
     */
    public $label_userFunc;

    /**
     * @var boolean
     */
    public $dividers2tabs;

    /**
     * @var boolean
     */
    public $hideTable;

    /**
     * @var array
     */
    public $requestUpdate;

    /**
     * @var string
     */
    public $thumbnail;

    /**
     * @var string
     */
    public $origUid;

    /**
     * @var string
     */
    public $sortby;

    /**
     * @var string
     */
    public $default_sortby;

    /**
     * @var boolean
     */
    public $canNotCollapse;

    /**
     * @var string
     */
    public $tstamp;

    /**
     * @var string
     */
    public $crdate;

    /**
     * @var string
     */
    public $cruser_id;

    /**
     * @var boolean
     */
    public $readOnly;

    /**
     * @var boolean
     */
    public $adminOnly;

    /**
     * @var string
     */
    public $delete;

    /**
     * @var array
     */
    public $enablecolumns;

    /**
     * @var array
     */
    public $searchFields;

    /**
     * @var boolean
     */
    public $hideAtCopy = true;

    /**
     * @var string
     */
    public $prependAtCopy = '';

    /**
     * @var string
     */
    public $languageField;

    /**
     * @var string
     */
    public $transOrigPointerField;

    /**
     * @var string
     */
    public $transOrigDiffSourceField;

    /**
     * @var boolean
     */
    public $versioningWS;

    /**
     * @var string
     */
    public $iconfile;

    /**
     * @var array
     */
    public $typeicons;

    /**
     * @var string
     */
    public $typeicon_column;

    /**
     * @var integer
     */
    public $rootLevel;

    /**
     * @var string
     */
    public $showRecordFieldList;

    /**
     * @var array
     */
    public $types;

    /**
     * @var array
     */
    public $palettes;

    /**
     * @var boolean
     */
    public $allowTableOnStandardPages;

    /**
     * @var boolean
     */
    public $excludeOnPageCopy;

    /**
     * @var string
     */
    public $copyAfterDuplFields;

    /**
     * @var string
     */
    public $useColumnsForDefaultValues;

    /**
     * @var string
     */
    public $translationSource;

    /**
     * @var string
     */
    public $formattedLabel_userFunc;

    /**
     * @var array
     */
    public $formattedLabel_userFunc_options;

    /**
     * @var array
     */
    public $security;
}