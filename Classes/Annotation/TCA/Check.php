<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_base_ext
 * @subpackage  Annotation\TCA
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Check extends Column
{
    /**
     * @var array
     */
    static public $defaults = [
        'l10n_mode' => 'exclude',
    ];

    /**
     * @var array
     */
    static public $fixed = [
        'type' => 'check',
    ];

    /**
     * @ConfigProperty
     *
     * @var array
     */
    public $items;

    /**
     * @ConfigProperty
     *
     * @var integer
     */
    public $cols;

    /**
     * @ConfigProperty
     *
     * @var boolean
     */
    public $showIfRTE;

    /**
     * @ConfigProperty
     *
     * @var string
     */
    public $itemsProcFunc;
}