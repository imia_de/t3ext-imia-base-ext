<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\User\TCA\Wizard;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Lang\LanguageService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_base_ext
 * @subpackage  User
 * @author      David Frerich <d.frerich@imia.de>
 */
class ImagePositionWizard
{
    /**
     * @var string
     */
    protected $templatePath = 'EXT:imia_base_ext/Resources/Private/Templates/';

    /**
     * Returns the HTML for the wizard inside the modal
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface $response
     * @throws \TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException
     */
    public function getWizardAction(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($this->isValidToken($request)) {
            $queryParams = $request->getQueryParams();
            $fileUid = isset($request->getParsedBody()['file']) ? $request->getParsedBody()['file'] : $queryParams['file'];
            $fileReferenceUid = isset($request->getParsedBody()['filereference']) ? $request->getParsedBody()['filereference'] : $queryParams['filereference'];

            $image = null;
            if ($fileUid && MathUtility::canBeInterpretedAsInteger($fileUid)) {
                try {
                    $image = ResourceFactory::getInstance()->getFileObject($fileUid);
                } catch (FileDoesNotExistException $e) {
                }
            } elseif ($fileReferenceUid && MathUtility::canBeInterpretedAsInteger($fileReferenceUid)) {
                try {
                    $image = ResourceFactory::getInstance()->getFileReferenceObject($fileReferenceUid);
                } catch (FileDoesNotExistException $e) {
                }
            }

            if ($image) {
                $view = $this->getFluidTemplateObject($this->templatePath . 'Wizards/ImagePositionWizard.html');
                $view->assign('image', $image);
                $view->assign('imageTitle', $image->getProperty('title') ?: $image->getNameWithoutExtension());
                $view->assign('imageWidth', $image->getProperty('width'));
                $view->assign('imageHeight', $image->getProperty('height'));
                $view->assign('cropVariant', null);
                $view->assign('measure', (bool)$queryParams['measure']);
                $content = $view->render();
            } else {
                $content = 'Error';
            }

            $response->getBody()->write($content);

            return $response;
        } else {
            return $response->withStatus(403);
        }
    }

    /**
     * @param array $params
     * @param \IMIA\ImiaBaseExt\Xclass\Backend\Form\Element\InputTextElement $pObj
     * @return string
     */
    public function renderWizard($params, $pObj)
    {
        $button = '';
        if ($params['params']['field'] && $params['params']['measure']) {
            $file = $this->getFile($params);

            if ($file) {
                $wizardData = [
                    'measure'                                                 => $params['params']['measure'],
                    $file instanceof FileReference ? 'filereference' : 'file' => $file->getUid(),
                ];
                $wizardData['token'] = GeneralUtility::hmac(implode('|', $wizardData), 'ImagePositionWizard');

                $buttonAttributes = [
                    'data-url'               => BackendUtility::getAjaxUrl('wizard_image_position', $wizardData),
                    'data-severity'          => 'notice',
                    'data-field'             => $params['itemName'],
                    'data-measure'           => $params['params']['measure'],
                    'data-image-name'        => $file->getNameWithoutExtension(),
                    'data-image-uid'         => $file->getUid(),
                    'data-image-width'       => $file->getProperty('width'),
                    'data-image-height'      => $file->getProperty('height'),
                    'data-image-isreference' => $file instanceof FileReference ? '1' : '0',
                ];

                $button .= '<button class="btn btn-default t3js-image-position-trigger"';
                foreach ($buttonAttributes as $key => $value) {
                    $button .= ' ' . $key . '="' . htmlspecialchars($value) . '"';
                }
                $button .= '><span class="t3-icon fa fa-crosshairs"></span>';
                $button .= $this->getLanguageService()->sL('LLL:EXT:imia_base_ext/Resources/Private/Language/locallang.xlf:imwizard.open-positioning', true);
                $button .= '</button>';

                if ($_REQUEST['ajaxID']) {
                    $button .= "\n" . '<script type="text/javascript">require(["' . ExtensionManagementUtility::extRelPath('imia_base_ext') .
                        'Resources/Public/Javascripts/requirejs/ImagePosition.js"], function(ImagePosition){ImagePosition.initializeTrigger()});</script>';
                } else {
                    /** @var PageRenderer $pageRenderer */
                    $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
                    $pageRenderer->loadRequireJsModule(
                        ExtensionManagementUtility::extRelPath('imia_base_ext') . 'Resources/Public/Javascripts/requirejs/ImagePosition.js',
                        'function(ImagePosition){ImagePosition.initializeTrigger()}'
                    );
                }
            }
        }

        return $button;
    }

    /**
     * @param ServerRequestInterface $request the request with the GET parameters
     * @return bool
     */
    protected function isValidToken(ServerRequestInterface $request)
    {
        if ($request->getQueryParams()['file']) {
            $parameters = [
                'measure'       => $request->getQueryParams()['measure'],
                'file'          => $request->getQueryParams()['file'],
            ];
        } else {
            $parameters = [
                'measure'       => $request->getQueryParams()['measure'],
                'filereference' => $request->getQueryParams()['filereference'],
            ];
        }

        $token = GeneralUtility::hmac(implode('|', $parameters), 'ImagePositionWizard');

        return $token === $request->getQueryParams()['token'];
    }

    /**
     * @param string $templatePathAndFileName optional the path to set the template path and filename
     * @return StandaloneView
     */
    protected function getFluidTemplateObject($templatePathAndFileName = null)
    {
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        if ($templatePathAndFileName) {
            $view->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName($templatePathAndFileName));
        }

        return $view;
    }

    /**
     * @param array $params
     * @return \TYPO3\CMS\Core\Resource\File|\TYPO3\CMS\Core\Resource\FileReference
     */
    protected function getFile($params)
    {
        $tcaColumns = $GLOBALS['TCA'][$params['table']]['columns'];

        $previousField = null;
        $fieldValue = $params['row']['uid'];
        $isFile = false;
        $isFileReference = false;

        $table = $params['table'];
        $fieldArr = array_map('trim', explode('.', $params['params']['field']));
        foreach ($fieldArr as $field) {
            if (array_key_exists($field, $tcaColumns)) {
                $subRecordTable = null;
                if ($previousField) {
                } else {
                    if(count($fieldArr) > 1) {
                        $fieldValue = $params['row'][$field];
                        if (is_array($fieldValue)) {
                            $fieldValue = array_first($fieldValue);
                            if (is_array($fieldValue) && isset($fieldValue['uid'])) {
                                $fieldValue = $fieldValue['uid'];
                            }
                        }
                    }
                }

                $newTcaColumns = [];
                switch ($tcaColumns[$field]['config']['type']) {
                    case 'select':
                        if (!isset($tcaColumns[$field]['config']['MM']) && !$tcaColumns[$field]['config']['MM']
                            && $tcaColumns[$field]['config']['foreign_table']
                        ) {
                            $subRecordTable = $tcaColumns[$field]['config']['foreign_table'];
                            $newTcaColumns = $GLOBALS['TCA'][$subRecordTable]['columns'];
                        }
                        break;
                    case 'inline':
                        if (!isset($tcaColumns[$field]['config']['MM']) && !$tcaColumns[$field]['config']['MM']
                            && $tcaColumns[$field]['config']['foreign_table']
                        ) {
                            $subRecordTable = $tcaColumns[$field]['config']['foreign_table'];
                            if ($subRecordTable == 'sys_file_reference') {
                                $row = $this->getDb()->exec_SELECTgetSingleRow('uid, uid_local', $subRecordTable,
                                    'tablenames = "' . $table . '" AND fieldname = "' . $field . '" AND uid_foreign = ' . $fieldValue .
                                    BackendUtility::deleteClause($subRecordTable));

                                if ($row) {
                                    $fieldValue = (int)$row['uid'];
                                    $isFileReference = true;
                                } else {
                                    $fieldValue = '';
                                }
                            } elseif ($tcaColumns[$field]['config']['foreign_field']) {
                                $row = $this->getDb()->exec_SELECTgetSingleRow('uid', $subRecordTable,
                                    $tcaColumns[$field]['config']['foreign_field'] . ' = ' . (int)$fieldValue . BackendUtility::deleteClause($subRecordTable));

                                if ($row) {
                                    $fieldValue = (int)$row['uid'];
                                    $isFile = true;
                                } else {
                                    $fieldValue = '';
                                }
                            }
                            $newTcaColumns = $GLOBALS['TCA'][$subRecordTable]['columns'];
                        }
                        break;
                    case 'group':
                        if ($tcaColumns[$field]['config']['internal_type'] == 'db'
                            && $tcaColumns[$field]['config']['allowed']
                        ) {
                            $allowedTables = array_map('trim', explode(',', $tcaColumns[$field]['config']['allowed']));

                            $subRecordTable = null;
                            if (count($allowedTables) === 1 || is_numeric($fieldValue)) {
                                $subRecordTable = array_shift($allowedTables);
                            } else {
                                foreach ($allowedTables as $table) {
                                    if (strpos($fieldValue[0], $table) === 0) {
                                        $subRecordTable = $table;
                                        break;
                                    }
                                }
                            }

                            if ($subRecordTable) {
                                $fieldValue = (int)array_shift(explode('|', str_replace($subRecordTable . '_', '', $fieldValue)));
                                $newTcaColumns = $GLOBALS['TCA'][$subRecordTable]['columns'];
                            } else {
                                $fieldValue = '';
                            }
                        }
                        break;
                }

                if (!$fieldValue) {
                    break;
                }

                $tcaColumns = $newTcaColumns;
                $previousField = $field;
                $table = $subRecordTable;
            }
        }

        if ($isFile) {
            try {
                return ResourceFactory::getInstance()->getFileObject($fieldValue);
            } catch (\Exception $e) {
            }
        } elseif ($isFileReference) {
            try {
                return ResourceFactory::getInstance()->getFileReferenceObject($fieldValue);
            } catch (\Exception $e) {
            }
        }

        return null;
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
