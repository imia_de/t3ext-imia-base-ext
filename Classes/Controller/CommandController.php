<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Controller;

use IMIA\ImiaBaseExt\Service\EnviromentServiceForceFrontend;
use IMIA\ImiaBaseExt\Traits\HookTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Service\ExtensionService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_base_ext
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class CommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{
    use HookTrait;

    /**
     * @var string
     */
    protected $extensionName = 'ImiaBaseExt';

    /**
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     */
    protected $uriBuilder;

    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var MailMessage
     */
    protected $lastSendMessage;

    /**
     * @var array
     */
    protected $settings;

    /**
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
    }

    /**
     * @param string $actionName
     * @param array $controllerArguments
     * @param string $controllerName
     * @param string $extensionName
     * @param string $pluginName
     * @param string $format
     * @param string $argumentPrefix
     * @return array
     * @throws \TYPO3\CMS\Extbase\Exception
     */
    protected function uriForArguments($actionName = null, $controllerArguments = [], $controllerName = null,
                                       $extensionName = null, $pluginName = null, $format = '', $argumentPrefix = null)
    {
        if ($actionName !== null) {
            $controllerArguments['action'] = $actionName;
        }
        if ($controllerName !== null) {
            $controllerArguments['controller'] = $controllerName;
        }
        /** @var ExtensionService $extensionService */
        $extensionService = GeneralUtility::makeInstance(ExtensionService::class);
        if ($pluginName === null && TYPO3_MODE === 'FE') {
            $pluginName = $extensionService->getPluginNameByAction($extensionName, $controllerArguments['controller'], $controllerArguments['action']);
        }
        if ($format !== '') {
            $controllerArguments['format'] = $format;
        }
        if ($argumentPrefix !== null) {
            $prefixedControllerArguments = [$argumentPrefix => $controllerArguments];
        } else {
            $pluginNamespace = $this->getPluginNamespace($pluginName, $extensionName);
            $prefixedControllerArguments = [$pluginNamespace => $controllerArguments];
        }

        return $prefixedControllerArguments;
    }

    /**
     * @param integer $targetPageUid
     * @param string $actionName
     * @param array $controllerArguments
     * @param string $controllerName
     * @param string $extensionName
     * @param string $pluginName
     * @param boolean $noCache
     * @param boolean $useCacheHash
     * @param boolean $createAbsoluteUri
     * @param string $format
     * @param string $argumentPrefix
     * @param array $arguments
     * @return string
     * @throws \TYPO3\CMS\Extbase\Exception
     */
    public function buildFrontendUri($targetPageUid, $actionName = null, $controllerArguments = [],
                                     $controllerName = null, $extensionName = null, $pluginName = null, $noCache = false, $useCacheHash = true,
                                     $createAbsoluteUri = true, $format = '', $argumentPrefix = null, $arguments = [])
    {
        $frontendPid = $targetPageUid;
        if (isset($arguments['MP']) && $arguments['MP']) {
            $MP = GeneralUtility::trimExplode('-', $arguments['MP']);
            if (isset($MP[1]) && is_numeric($MP[1])) {
                $frontendPid = $MP[1];
            }
        }
        $this->initFrontend($frontendPid);

        if (is_array($controllerArguments) && count($controllerArguments) > 0) {
            $controllerArguments = $this->uriForArguments(
                $actionName,
                $controllerArguments,
                $controllerName,
                $extensionName,
                $pluginName,
                $format,
                $argumentPrefix
            );
        }

        $this->uriBuilder->reset();
        $this->uriBuilder
            ->setNoCache($noCache)
            ->setUseCacheHash($useCacheHash)
            ->setCreateAbsoluteUri($createAbsoluteUri)
            ->setTargetPageUid($targetPageUid)
            ->setArguments(array_merge($arguments, $controllerArguments));

        return str_replace(PATH_site, '/', $this->uriBuilder->buildFrontendUri());
    }

    /**
     * Translate by key
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    public function translate($key, $arguments = [])
    {
        return (string)LocalizationUtility::translate($key, $this->extensionName, $arguments);
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $body
     * @param string $format
     * @param callable $closure
     * @return boolean
     */
    public function sendMail(array $recipient, array $sender, $subject, $body, $format = 'plain', $closure = null)
    {
        /** @var \TYPO3\CMS\Core\Mail\MailMessage $message */
        $message = $this->objectManager->get('TYPO3\CMS\Core\Mail\MailMessage');

        switch ($format) {
            case 'html':
                break;
            case 'plain':
            default:
                $format = 'plain';
                break;
        }

        foreach ($recipient as $key => $value) {
            if (!GeneralUtility::validEmail($key)) {
                if (!GeneralUtility::validEmail($value)) {
                    unset($recipient[$key]);
                }
            }
        }
        foreach ($sender as $key => $value) {
            if (!GeneralUtility::validEmail($key)) {
                if (!GeneralUtility::validEmail($value)) {
                    unset($sender[$key]);
                }
            }
        }

        if (count($recipient) > 0 && count($sender) > 0) {
            $message->setTo($recipient)
                ->setFrom($sender)
                ->setSubject($subject)
                ->setBody($body, 'text/' . $format);

            if ($closure) {
                try {
                    call_user_func($closure, $message);
                } catch (\Exception $e) {
                }
            }

            $message->send();
            $isSend = $message->isSent();

            if ($isSend) {
                $this->setLastSendMessage($message);
            }

            $log = [
                'datetime'  => date('d.m.Y H:i:s'),
                'recipient' => $recipient,
                'sender'    => $sender,
                'subject'   => $subject,
                'body'      => $body,
                'isSend'    => $message->isSent(),
            ];
            if (!is_dir(PATH_site . 'typo3temp/logs')) {
                mkdir(PATH_site . 'typo3temp/logs');
            }
            file_put_contents(PATH_site . 'typo3temp/logs/sendmail.log', print_r($log, true), FILE_APPEND);

            return $isSend;
        } else {
            return false;
        }
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $templateName
     * @param array $arguments
     * @param string $format
     * @param callable $closure
     * @param integer $language
     * @return boolean
     */
    public function sendTemplateMailSpecial(array $recipient, array $sender, $subject, $templateName, array $arguments = [],
                                            $format = 'plain', $closure = null, $language = 0)
    {
        $subject = $this->renderFluidContent($subject, $arguments);

        return $this->sendTemplateMail($recipient, $sender, $subject, $templateName, $arguments, $format, $closure, $language);
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $templateName
     * @param array $arguments
     * @param string $format
     * @param callable $closure
     * @param integer $language
     * @return boolean
     */
    public function sendTemplateMail(array $recipient, array $sender, $subject, $templateName, array $arguments = [],
                                     $format = 'plain', $closure = null, $language = 0)
    {
        switch ($format) {
            case 'html':
                $ext = 'html';
                break;
            case 'text':
            case 'plain':
            default:
                $ext = 'txt';
                break;
        }

        $message = $this->renderView('Email', $templateName, $arguments, $ext, $language);

        return $this->sendMail(
            $recipient,
            $sender,
            $subject,
            $message,
            $format,
            $closure
        );
    }

    /**
     * @param string $content
     * @param array $arguments
     * @return string
     */
    public function renderFluidContent($content, $arguments = [])
    {
        $cryptoStrong = true;
        $tempPath = PATH_site . 'typo3temp/baseext_' . md5($content) . '_' .
            substr(sha1(bin2hex(openssl_random_pseudo_bytes(22, $cryptoStrong))), 0, 10) . '.temp';

        file_put_contents($tempPath, $content);
        $rendered = $this->renderTemplate($tempPath, $arguments);
        unlink($tempPath);

        return $rendered;
    }

    /**
     * @param string $controller
     * @param string $action
     * @param array $arguments
     * @param string $ext
     * @param integer $language
     * @return string
     */
    public function renderView($controller, $action, $arguments, $ext = 'html', $language = 0)
    {
        $templateRootPaths = $this->getPaths('templateRootPath');
        do {
            $templateRootPath = array_pop($templateRootPaths);
            $templatePathAndFilename = $templateRootPath . $controller . '/' . $action . '.' . $ext;
        } while (!file_exists($templatePathAndFilename) && count($templateRootPaths) > 0);

        return $this->renderTemplate($templatePathAndFilename, $arguments, $ext, $language);
    }

    /**
     * @param string $templatePathAndFilename
     * @param array $arguments
     * @param string $ext
     * @param integer $language
     * @return string
     */
    public function renderTemplate($templatePathAndFilename, $arguments, $ext = 'html', $language = 0)
    {
        $this->initFrontend(null, $language);

        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $view */
        $view = $this->objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');

        /** @var EnviromentServiceForceFrontend $enviromentService */
        $enviromentService = $this->objectManager->get(EnviromentServiceForceFrontend::class);

        /** @var Request $request */
        $request = $this->objectManager->get(Request::class);
        $request->injectEnvironmentService($enviromentService);
        $request->setRequestUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        $request->setBaseUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . '/');

        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($request);
        $uriBuilder->injectEnvironmentService($enviromentService);

        /** @var ControllerContext $controllerContext */
        $controllerContext = $this->objectManager->get(ControllerContext::class);
        $controllerContext->setRequest($request);
        $controllerContext->setUriBuilder($uriBuilder);

        $view->setControllerContext($controllerContext);

        $view->setFormat($ext);
        $view->getRequest()->setControllerExtensionName($this->extensionName);

        $partialRootPaths = $this->getPaths('partialRootPath');
        if (method_exists($view, 'setPartialRootPaths')) {
            $view->setPartialRootPaths($partialRootPaths);
        } else {
            $view->setPartialRootPath($partialRootPaths[0]);
        }
        $layoutRootPaths = $this->getPaths('layoutRootPath');
        if (method_exists($view, 'setLayoutRootPaths')) {
            $view->setLayoutRootPaths($layoutRootPaths);
        } else {
            $view->setLayoutRootPath($layoutRootPaths[0]);
        }

        $view->setTemplatePathAndFilename($templatePathAndFilename);
        if ($arguments['settings'] && is_array($arguments['settings'])) {
            if ($this->settings && is_array($this->settings)) {
                ArrayUtility::mergeRecursiveWithOverrule($arguments['settings'], $this->settings);
            }
        } else {
            $arguments['settings'] = $this->settings;
        }

        $view->assignMultiple($arguments);

        return $view->render();
    }

    /**
     * @param $string
     * @param bool $forceLowercase
     * @param bool $anal
     * @return string
     */
    public function sanitize($string, $forceLowercase = true, $anal = false)
    {
        $strip = ["~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            ",", "<", ".", ">", "/", "?"];

        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;

        return ($forceLowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    /**
     * @return MailMessage
     */
    public function getLastSendMessage()
    {
        return $this->lastSendMessage;
    }

    /**
     * @return array
     */
    public function getExtensionConfiguration()
    {
        return $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            $this->extensionName ?: null,
            null
        );
    }

    /**
     * @param string $languageKey
     */
    protected function setLanguage($languageKey)
    {
        if (!$GLOBALS['BE_USER']) {
            $GLOBALS['BE_USER'] = new \stdClass();
            $GLOBALS['BE_USER']->uc = ['lang' => $languageKey];
        }

        $GLOBALS['BE_USER']->uc['lang'] = $languageKey;
    }

    /**
     * Load arguments for a different plugin than the current
     *
     * @param string $pluginName
     */
    protected function loadArguments($pluginName = null)
    {
        if ($pluginName) {
            $arguments = GeneralUtility::_GPmerged($this->getPluginNamespace($pluginName));
        } else {
            $arguments = $_GET;
            ArrayUtility::mergeRecursiveWithOverrule($arguments, $_POST);
        }

        $this->request->setArguments($arguments);
    }

    /**
     *
     * @param string $pluginName
     * @param string $extensionName
     * @return string
     */
    protected function getPluginNamespace($pluginName = null, $extensionName = null)
    {
        $extensionService = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Service\\ExtensionService');
        $pluginNamespace = $extensionService->getPluginNamespace(
            $extensionName ?: $this->extensionName,
            $pluginName
        );

        return $pluginNamespace;
    }

    /**
     * @param string $name
     * @return array
     */
    protected function getPaths($name)
    {
        $extensionConfiguration = $this->getExtensionConfiguration();

        $paths = [];
        if (is_array($extensionConfiguration['view']) && array_key_exists('templateRootPaths', $extensionConfiguration['view'])
            && is_array($extensionConfiguration['view'][$name . 's']) && count($extensionConfiguration['view'][$name . 's']) > 0) {
            ksort($extensionConfiguration['view'][$name . 's']);
            foreach ($extensionConfiguration['view'][$name . 's'] as $path) {
                $paths[] = GeneralUtility::getFileAbsFileName($path);
            }
        } else {
            if ($extensionConfiguration['view'][$name]) {
                $paths[] = GeneralUtility::getFileAbsFileName($extensionConfiguration['view'][$name]);
            } else {
                switch ($name) {
                    case 'partialRootPath':
                        $defaultPath = 'Resources/Private/Partials/';
                        break;
                    case 'layoutRootPath':
                        $defaultPath = 'Resources/Private/Layouts/';
                        break;
                    case 'templateRootPath':
                    default:
                        $defaultPath = 'Resources/Private/Templates/';
                        break;
                }

                $paths[] = ExtensionManagementUtility::extPath(
                    GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName),
                    $defaultPath
                );
            }
        }

        return $paths;
    }

    /**
     * @param MailMessage $message
     * @return $this
     */
    private function setLastSendMessage($message)
    {
        $this->lastSendMessage = $message;

        return $this;
    }

    /**
     * @param integer $pageUid
     * @param integer $language
     * @return TypoScriptFrontendController
     */
    protected function initFrontend($pageUid = null, $language = 0)
    {
        if (!$pageUid) {
            $pageUid = 0;
        }

        if (!$this->configurationManager->getContentObject()) {
            $this->configurationManager->setContentObject(
                $this->objectManager->get(ContentObjectRenderer::class)
            );
        }

        if (!$GLOBALS['TSFE'] || $GLOBALS['TSFE']->id != $pageUid) {
            if ($pageUid) {
                $rootline = BackendUtility::BEgetRootLine($pageUid);
                $host = BackendUtility::firstDomainRecord($rootline);
                $_SERVER['HTTP_HOST'] = $host;
            }

            if (!defined('TYPO3_PATH_WEB')) {
                define('TYPO3_PATH_WEB', true);
            }

            $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] = 'typo3';
            GeneralUtility::flushInternalRuntimeCaches();

            if (!is_object($GLOBALS['TT'])) {
                $GLOBALS['TT'] = $this->objectManager->get('TYPO3\CMS\Core\TimeTracker\TimeTracker');
                $GLOBALS['TT']->start();
            }

            if (!$GLOBALS['TSFE']) {
                $GLOBALS['TSFE'] = GeneralUtility::makeInstance(TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], $pageUid, 0, 1);
            }

            $GLOBALS['TSFE']->connectToDB();
            $GLOBALS['TSFE']->initFEuser();
            $GLOBALS['TSFE']->determineId();
            $GLOBALS['TSFE']->initTemplate();
            $GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
            $GLOBALS['TSFE']->forceTemplateParsing = 1;
            $GLOBALS['TSFE']->getConfigArray();
            $GLOBALS['TSFE']->cObj = $this->configurationManager->getContentObject();

            /** @var Request $request */
            $request = $this->objectManager->get(Request::class);

            $this->uriBuilder = $this->objectManager->get(UriBuilder::class);
            $this->uriBuilder->setRequest($request);
            $this->uriBuilder->initializeObject();
        }

        if ($language) {
            $GLOBALS['TSFE']->sys_language_uid = ($GLOBALS['TSFE']->sys_language_content = (int)$language);
            $GLOBALS['TSFE']->linkVars = '&L=' . (int)$language;
        }

        return $GLOBALS['TSFE'];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}