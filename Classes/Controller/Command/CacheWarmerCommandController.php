<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Controller\Command;

use IMIA\ImiaBaseExt\Service\FrontendService;
use IMIA\ImiaBaseExt\Controller\CommandController;
use IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @package     imia_base_ext
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class CacheWarmerCommandController extends CommandController
{
    /**
     * @var \IMIA\ImiaBaseExt\Domain\Repository\CacheWarmer\CacheItemRepository
     * @inject
     */
    protected $cacheItemRepository;

    /**
     * @param int $itemsPerRun
     * @param int $waitBetweenItemsInMs
     * @param int $minPriority
     * @return bool
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function WarmCacheCommand($itemsPerRun = 100, $waitBetweenItemsInMs = 500, $minPriority = 0)
    {
        for ($i = 0; $i < $itemsPerRun; $i++) {
            /** @var CacheItem $item */
            $item = $this->cacheItemRepository->findOneBy(['warm' => false, 'locked' => false]);
            if (!$item || $item->getPriority() < $minPriority) {
                echo('Cache is fully warmed up' . "\n");

                return true;
            }

            // Always update URI
            FrontendService::replace($item->getPid(), $item->getParams()['L'] ?: 0);

            /** @var ContentObjectRenderer $contentObjectRenderer */
            $contentObjectRenderer = $GLOBALS['TSFE']->cObj;

            $paramString = '';
            foreach ($item->getParams() as $key => $value) {
                $paramString .= '&' . $key . '=' . $value;
            }

            $uri = $contentObjectRenderer->typoLink_URL([
                'parameter'         => $item->getPid(),
                'forceAbsoluteUrl'  => true,
                'forceAbsoluteUrl.' => ['scheme' => 'https'],
                'additionalParams'  => $paramString,
                'useCacheHash'      => true,
            ]);

            $item->setUri($uri);

            $item->setLocked(true);
            $this->cacheItemRepository->update($item);
            $this->objectManager->get(PersistenceManager::class)->persistAll();

            /** @var CacheItem $itemConflict */
            $itemConflict = $this->cacheItemRepository->findOneBy(['warm' => true, 'uri' => $uri]);
            if ($itemConflict) {
                if ($itemConflict->getUid() !== $item->getUid()) {
                    $this->cacheItemRepository->remove($item);
                    $this->objectManager->get(PersistenceManager::class)->persistAll();
                }
            } else {
                echo 'Requesting: ' . $uri;

                $error = false;
                $warmSuccessful = false;
                $responseHeaders = GeneralUtility::getUrl($uri, 2);
                if ($responseHeaders) {
                    $headers = $this->getHeaders($responseHeaders);
                    if (strpos($headers['http_code'], '200') !== false) {
                        $warmSuccessful = true;
                    } elseif (strpos($headers['http_code'], '300') !== false ||
                        strpos($headers['http_code'], '301') !== false ||
                        strpos($headers['http_code'], '302') !== false ||
                        strpos($headers['http_code'], '303') !== false ||
                        strpos($headers['http_code'], '307') !== false ||
                        strpos($headers['http_code'], '308') !== false ||
                        strpos($headers['http_code'], '400') !== false ||
                        strpos($headers['http_code'], '401') !== false ||
                        strpos($headers['http_code'], '402') !== false ||
                        strpos($headers['http_code'], '403') !== false ||
                        strpos($headers['http_code'], '404') !== false ||
                        strpos($headers['http_code'], '405') !== false ||
                        strpos($headers['http_code'], '406') !== false ||
                        strpos($headers['http_code'], '410') !== false) {
                        if (!$warmSuccessful) {
                            $error = true;
                        }
                    }

                    echo ' (' . $headers['http_code'] . ') - ' . ($warmSuccessful ? 'SUCCESS' : ($error ? 'ERROR' : 'INVALID'));
                } else {
                    $error = true;
                    echo ' (no response) - INVALID';
                }
                echo "\n";

                if ($error) {
                    $this->cacheItemRepository->remove($item);
                    $this->objectManager->get(PersistenceManager::class)->persistAll();
                } else {
                    $item
                        ->setLocked(false)
                        ->setWarm(true);

                    $this->cacheItemRepository->update($item);
                    $this->objectManager->get(PersistenceManager::class)->persistAll();
                }

                if ($waitBetweenItemsInMs > 0) {
                    usleep($waitBetweenItemsInMs);
                }
            }
        }

        return true;
    }

    /**
     * @param string $headerContent
     * @return array
     */
    protected function getHeaders($headerContent)
    {
        $headers = [];
        foreach (explode("\r\n", $headerContent) as $i => $line) {
            if ($i === 0)
                $headers['http_code'] = $line;
            else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }

        return $headers;
    }
}