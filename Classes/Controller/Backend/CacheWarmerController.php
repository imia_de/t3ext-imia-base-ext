<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaBaseExt\Controller\Backend;

use IMIA\ImiaBaseExt\Service\FrontendService;
use IMIA\ImiaBaseExt\Controller\ActionController;
use IMIA\ImiaBaseExt\Domain\Model\CacheWarmer\CacheItem;

/**
 * @package     imia_base_ext
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class CacheWarmerController extends ActionController
{
    /**
     * @var \IMIA\ImiaBaseExt\Domain\Repository\CacheWarmer\CacheItemRepository
     * @inject
     */
    protected $cacheItemRepository;

    /**
     * @var \IMIA\ImiaBaseExt\Utility\Session\Storage\SessionStorage
     * @inject
     */
    protected $sessionStorage;

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function indexAction()
    {
        $page = (int)$this->getParam('page') ?: 1;
        if ($page <= 0) {
            $page = 0;
        }

        $perpage = (int)$this->getParam('perpage') ?: 15;
        if ($perpage <= 0) {
            $perpage = 15;
        }
        $offset = $perpage * $page - $perpage;

        $onlyPriority = (int)$this->getParam('onlyPriority') ?: '';
        if ($onlyPriority <= 0) {
            $onlyPriority = '';
        }

        $filter = [];
        $filterUnwarm = [];
        $show = $this->getParam('show') ?: 'unwarm';
        switch ($show) {
            case 'all':
                break;
            case 'warm':
                $filter['warm'] = true;
                break;
            case 'unwarm':
            default:
                $filter['warm'] = false;
                break;
        }

        $root = (int)$this->getParam('root') ?: 0;
        if ($root) {
            $filter['root'] = $root;
            $filterUnwarm['root'] = $root;
        }

        $filterUnwarm['warm'] = false;

        if(intval($onlyPriority)) {
            $filter['priority'] = $onlyPriority;
            $filterUnwarm['priority'] = $onlyPriority;
        }

        $filterTotal = $filterUnwarm;
        unset($filterTotal['warm']);

        $unwarmItemCount = $this->cacheItemRepository->countBy($filterUnwarm);
        $totalItemCount = $this->cacheItemRepository->countBy($filterTotal);
        $itemCount = $this->cacheItemRepository->countBy($filter);
        while ($offset > $itemCount) {
            $offset -= $perpage;
        }

        $items = [];
        /** @var CacheItem $item */
        foreach ($this->cacheItemRepository->findBy($filter, $perpage, $offset) as $item) {
            if (!$item->getUri()) {
                FrontendService::replace($item->getPid(), isset($item->getParams()['L']) ? $item->getParams()['L'] : 0);
                $item->setUri(FrontendService::getUriBuilder()
                    ->setTargetPageUid($item->getPid())
                    ->setArguments($item->getParams())
                    ->setCreateAbsoluteUri(true)->build());

                $this->cacheItemRepository->update($item);
            }
            $items[] = $item;
        }
        $itemPercent = round(100 - (100/($totalItemCount ?: 1) * $unwarmItemCount), 1);

        $this->view->assign('items', $items);
        $this->view->assign('pages', ceil($itemCount / $perpage));
        $this->view->assign('page', $page);
        $this->view->assign('itemCount', $itemCount);
        $this->view->assign('totalItemCount', $totalItemCount);
        $this->view->assign('unwarmItemCount', $unwarmItemCount);
        $this->view->assign('itemPercent', $itemPercent);
        $this->view->assign('itemStatus', $itemPercent < 25 ? 'danger' : ($itemPercent < 50 ? 'warning' : ($itemPercent < 90 ? 'info' : 'success')));

        $rootOptions = $this->cacheItemRepository->findDistinctRoots($onlyPriority);

        $this->view->assign('perpage', $perpage);
        $this->view->assign('perpageOptions', [15 => 15, 30 => 30, 50 => 50, 100 => 100]);
        $this->view->assign('show', $show);
        $this->view->assign('calcOnlyPriority', ['all' => 'all', 50 => 50, 40 => 40, 30 => 30, 20 => 20, 10 => 10]);
        $this->view->assign('onlyPriority', $onlyPriority);
        $this->view->assign('showOptions', ['all' => 'all', 'warm' => 'warm', 'unwarm' => 'unwarm']);
        $this->view->assign('root', $root);
        $this->view->assign('rootOptions', $rootOptions);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getParam($name)
    {
        $param = null;
        if ($this->request->hasArgument($name)) {
            $param = $this->request->getArgument($name);
            $this->sessionStorage->set('param_' . $name, $param);
        } elseif ($this->sessionStorage->has('param_' . $name)) {
            $param = $this->sessionStorage->get('param_' . $name);
        }

        return $param;
    }
}