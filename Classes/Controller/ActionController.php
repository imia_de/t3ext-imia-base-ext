<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2024+ IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

declare(strict_types=1);

namespace IMIA\ImiaBaseExt\Controller;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController as TYPO3ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;
use TYPO3\CMS\Extbase\Mvc\ResponseInterface;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Security\Exception\InvalidHashException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractCompositeValidator;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

abstract class ActionController extends TYPO3ActionController
{
    use HookTrait;

    /**
     * @var array
     */
    protected $resolvedSettings = null;

    /**
     * @var array
     */
    protected $fileStorage = [];

    /**
     * @var string
     */
    protected $controllerName = null;

    /**
     * @var array
     */
    protected $overrideValidation = [];

    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);

        $view->assign('settings', $this->getSettings());
        $view->assign('action', substr($this->actionMethodName, 0, strlen('Action') * -1));
    }

    protected function initializeActionMethodValidators()
    {
        parent::initializeActionMethodValidators();

        if (isset($this->overrideValidation[$this->actionMethodName])) {
            $overrideValidation = $this->overrideValidation[$this->actionMethodName];
            $actionMethodParameters = static::getActionMethodParameters($this->objectManager);

            if (isset($actionMethodParameters[$this->actionMethodName])) {
                $methodParameters = $actionMethodParameters[$this->actionMethodName];
            } else {
                $methodParameters = [];
            }

            $parameterValidators = $this->validatorResolver->buildMethodArgumentsValidatorConjunctions(get_class($this), $this->actionMethodName, $methodParameters);

            /** @var \TYPO3\CMS\Extbase\Mvc\Controller\Argument $argument */
            foreach ($this->arguments as $argument) {
                if (isset($overrideValidation[$argument->getName()])) {
                    $validator = $parameterValidators[$argument->getName()];
                    $baseValidatorConjunction = $this->validatorResolver->getBaseValidatorConjunction($overrideValidation[$argument->getName()]);

                    if (!empty($baseValidatorConjunction) && $validator instanceof AbstractCompositeValidator) {
                        $validator->addValidator($baseValidatorConjunction);
                    }

                    $argument->setValidator($validator);
                }
            }
        }
    }

    protected function callActionMethod()
    {
        parent::callActionMethod();

        $response = &$this->response;

        if ($this->view instanceof JsonView && $response instanceof \TYPO3\CMS\Extbase\Mvc\Web\Response) {
            $response->setHeader('Content-Type', 'text/json');
            $response->setHeader('Content-Length', strlen($response->getContent()));
        }
    }

    protected function addErrorFlashMessage()
    {
    }

    public function processRequest(RequestInterface $request, ResponseInterface $response)
    {
        if ($GLOBALS['TYPO3_CONF_VARS']['FE']['debug']) {
            parent::processRequest($request, $response);
        } else {
            try {
                parent::processRequest($request, $response);
            } catch (\Exception $exception) {
                if ($exception instanceof StopActionException) {
                    throw ($exception);
                } else {
                    if ($exception->getMessage()) {
                        $flashMessage = $this->getFlashMessage($exception->getMessage(), '', FlashMessage::ERROR, false);

                        /** @var FlashMessageService $flashMessageService */
                        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
                        $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);

                        $this->response->appendContent(
                            $flashMessageService->getMessageQueueByIdentifier()->renderFlashMessages()
                        );

                        if (getenv('TYPO3_CONTEXT') != 'Development') {
                            $errorMail = getenv('ERROR_MAIL') ?: ($_ENV['ERROR_MAIL'] ?: ($_SERVER['ERROR_MAIL'] ?: $_SERVER['REDIRECT_ERROR_MAIL']));
                            if ($errorMail && !($exception instanceof NoSuchArgumentException) && !($exception instanceof InvalidHashException)) {
                                if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
                                    $protocol = 'https://';
                                } else {
                                    $protocol = 'http://';
                                }
                                $errorMessage = 'URL: ' . $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "\n";
                                $errorMessage .= 'Output: ' . $exception->getMessage() . "\n\n";
                                $errorMessage .= @sprintf('Error in file "%s" at line %d: %s' . "\n", $exception->getFile(), $exception->getLine(), $exception->getMessage());

                                file_put_contents(PATH_site . 'typo3temp/logs/error.log', date('d.m.Y H:i:s') . ' - ' . $errorMessage . "\n", FILE_APPEND | LOCK_EX);
                                $errorMsgFile = PATH_site . 'typo3temp/var/Cache/Code/cache_core/error_' . md5($errorMessage) . '.lock';

                                if (!file_exists($errorMsgFile) || filemtime($errorMsgFile) < (time() - 60 * 60)) {
                                    file_put_contents($errorMsgFile, $errorMessage, LOCK_EX);

                                    if (
                                        \strpos($errorMessage, 'MySQL server has gone away') === false &&
                                        \strpos($errorMessage, 'Allowed memory size') === false &&
                                        \strpos($errorMessage, 'The HMAC of the form could not be validated') &&
                                        \strpos($errorMessage, 'The current host header value does not match the configured trusted hosts pattern') === false &&
                                        \strpos($errorMessage, 'powermail') === false &&
                                        \strpos($errorMessage, '(controller "Form") is not allowed by this plugin') === false
                                    ) {
                                        if (\getenv('StagingEnv') === 'active') {
                                            // We are on Staging: Don't send any mails - because of pen testing
                                            \error_log($errorMessage); // ~> goes to ./logs/
                                        } else {
                                            // We are on Production: Send the mail
                                            $domain = $_SERVER['HTTP_HOST'];
                                            \mail(
                                                $errorMail,
                                                'TYPO3 Extbase Error' . ($domain ? ' | ' . $domain : ''),
                                                $errorMessage,
                                                'From: dev@imia.de' . "\r\n" .
                                                'Reply-To: dev@imia.de' . "\r\n" .
                                                'X-Mailer: PHP/' . \phpversion()
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($GLOBALS['TSFE']->type == 0) {
                        $this->response->appendContent('<!--DISABLE_CONTENT_CACHE-->');
                    }
                    if ($GLOBALS['TSFE']) {
                        $GLOBALS['TSFE']->no_cache = true;
                    }
                }
            }
        }
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return ObjectManagerInterface
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return UriBuilder
     */
    public function getUriBuilder()
    {
        return $this->uriBuilder;
    }

    /**
     * @param string $key
     * @param boolean $resolved
     * @return mixed
     */
    public function getSetting($key, $resolved = true)
    {
        $settings = $this->getSettings($resolved);

        if (strpos($key, '.') === false) {
            $value = $settings[$key];
        } else {
            $keys = explode('.', $key);
            $value = $settings[array_shift($keys)];

            foreach ($keys as $subKey) {
                if ($value && is_array($value)) {
                    $value = $value[$subKey];
                } else {
                    $value = null;
                    break;
                }
            }
        }

        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function setSetting($key, $value)
    {
        $this->settings[$key] = $value;
        $this->resolvedSettings[$key] = $value;

        return $this;
    }

    /**
     * @param array $config
     * @return mixed
     * @throws \Exception
     */
    public function renderTSObj(array $config)
    {
        if (!$this->view) {
            throw new \Exception('Error: calling renderTSObj before view is initialized');
        } elseif (array_key_exists('_typoScriptNodeValue', $config) && is_object($this->getContentObject())) {
            $contentType = $config['_typoScriptNodeValue'];

            if (
                in_array($contentType, [
                    'CASE',
                    'CLEARGIF',
                    'COA',
                    'COA_INT',
                    'COLUMNS',
                    'CONTENT',
                    'CTABLE',
                    'EDITPANEL',
                    'FILE',
                    'FILES',
                    'FLUIDTEMPLATE',
                    'FORM',
                    'HMENU',
                    'HRULER',
                    'IMAGE',
                    'IMG_RESOURCE',
                    'IMGTEXT',
                    'LOAD_REGISTER',
                    'OTABLE',
                    'RECORDS',
                    'RESTORE_REGISTER',
                    'SEARCHRESULT',
                    'SVG',
                    'TEMPLATE',
                    'TEXT',
                    'USER',
                    'USER_INT'
                ])
            ) {

                unset($config['_typoScriptNodeValue']);
                $contentConfig = $this->objectManager->get('TYPO3\CMS\Extbase\Service\TypoScriptService')
                    ->convertPlainArrayToTypoScriptArray($config);

                $split = null;
                if ($contentConfig['split.']) {
                    $split = $contentConfig['split.'];
                    unset($contentConfig['split.']);
                }

                $value = $this->getContentObject()->cObjGetSingle($contentType, $contentConfig);

                if ($split) {
                    $value = explode($split['token'] ?: ',', $value);
                }

                return $value;
            } else {
                return $config;
            }
        } else {
            return $config;
        }
    }

    /**
     * @return ContentObjectRenderer
     */
    public function getContentObject()
    {
        return $this->configurationManager->getContentObject();
    }

    /**
     * Translate by key
     *
     * @param string $key
     * @param array $arguments
     * @return string
     */
    public function translate($key, $arguments = [])
    {
        return (string) LocalizationUtility::translate($key, $this->extensionName, $arguments);
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $body
     * @param string $format
     * @param callable $closure
     * @return boolean
     */
    public function sendMail(array $recipient, array $sender, $subject, $body, $format = 'plain', $closure = null)
    {
        $message = $this->objectManager->get('TYPO3\CMS\Core\Mail\MailMessage');

        switch ($format) {
            case 'html':
                break;
            case 'plain':
            default:
                $format = 'plain';
                break;
        }

        foreach ($recipient as $key => $value) {
            if (!GeneralUtility::validEmail($key)) {
                if (!GeneralUtility::validEmail($value)) {
                    unset($recipient[$key]);
                }
            }
        }
        foreach ($sender as $key => $value) {
            if (!GeneralUtility::validEmail($key)) {
                if (!GeneralUtility::validEmail($value)) {
                    unset($sender[$key]);
                }
            }
        }

        if (count($recipient) > 0 && count($sender) > 0) {
            $subject = trim(str_replace(["\r\n", "\r", "\n"], ' ', $subject));
            $subject = utf8_encode(utf8_decode($subject)); // remove problematic chars

            $message->setTo($recipient)
                ->setFrom($sender)
                ->setSubject($subject)
                ->setBody($body, 'text/' . $format);

            if ($closure) {
                try {
                    call_user_func($closure, $message);
                } catch (\Exception $e) {
                }
            }

            $message->send();

            $log = [
                'datetime' => date('d.m.Y H:i:s'),
                'recipient' => $recipient,
                'sender' => $sender,
                'subject' => $subject,
                'body' => $body,
                'isSend' => $message->isSent(),
            ];
            if (!is_dir(PATH_site . 'typo3temp/logs')) {
                mkdir(PATH_site . 'typo3temp/logs');
            }
            file_put_contents(PATH_site . 'typo3temp/logs/sendmail.log', print_r($log, true), FILE_APPEND);

            return $message->isSent();
        } else {
            return false;
        }
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $templateName
     * @param array $arguments
     * @param string $format
     * @param callable $closure
     * @return boolean
     */
    public function sendTemplateMailSpecial(
        array $recipient,
        array $sender,
        $subject,
        $templateName,
        array $arguments = [],
        $format = 'plain',
        $closure = null
    ) {
        $subject = $this->renderFluidContent($subject, $arguments);

        return $this->sendTemplateMail($recipient, $sender, $subject, $templateName, $arguments, $format, $closure);
    }

    /**
     * @param array $recipient
     * @param array $sender
     * @param string $subject
     * @param string $templateName
     * @param array $arguments
     * @param string $format
     * @param callable $closure
     * @return boolean
     */
    public function sendTemplateMail(
        array $recipient,
        array $sender,
        $subject,
        $templateName,
        array $arguments = [],
        $format = 'plain',
        $closure = null
    ) {
        switch ($format) {
            case 'html':
                $ext = 'html';
                break;
            case 'text':
            case 'plain':
            default:
                $ext = 'txt';
                break;
        }

        $message = $this->renderView('Email', $templateName, $arguments, $ext);

        return $this->sendMail(
            $recipient,
            $sender,
            $subject,
            $message,
            $format,
            $closure
        );
    }

    /**
     * @param string $name
     * @return array
     */
    public function getFileArgument($name)
    {
        $fileArgument = null;

        $argumentName = explode('.', $name);
        $name = array_shift($argumentName);

        if (
            $this->request->hasArgument($name) && array_key_exists($this->getPluginNamespace(), $_FILES)
            && $_FILES[$this->getPluginNamespace()]['name'][$name]
        ) {
            $fileArgument = [
                'name' => $_FILES[$this->getPluginNamespace()]['name'][$name],
                'type' => $_FILES[$this->getPluginNamespace()]['type'][$name],
                'tmp_name' => $_FILES[$this->getPluginNamespace()]['tmp_name'][$name],
                'error' => $_FILES[$this->getPluginNamespace()]['error'][$name],
                'size' => $_FILES[$this->getPluginNamespace()]['size'][$name],
            ];

            if (count($argumentName) > 0) {
                foreach ($argumentName as $subname) {
                    foreach ($fileArgument as $key => $argument) {
                        $fileArgument[$key] = $argument[$subname];
                    }
                }
            }
        }

        return $fileArgument;
    }

    /**
     * @param array $fileData
     * @param integer $storageUid
     * @param string $folder
     * @return File
     */
    public function uploadFile(array $fileData, $storageUid = 0, $folder = '')
    {
        $file = null;
        $storage = $this->getFileStorage($storageUid);

        if ($storage->isWritable() && file_exists($fileData['tmp_name'])) {
            $targetFolder = $storage->getRootLevelFolder();
            foreach (explode('/', $folder) as $folderName) {
                if ($folderName) {
                    if ($storage->hasFolderInFolder($folderName, $targetFolder)) {
                        $targetFolder = $targetFolder->getSubfolder($folderName);
                    } else {
                        $targetFolder = $targetFolder->createFolder($folderName);
                    }
                }
            }

            $file = $targetFolder->addUploadedFile($fileData, 'changeName');
            if ($file) {
                // indexes file
                $file->updateProperties([]);
            }
        }

        return $file;
    }

    /**
     * @param string $filePath
     * @param string $fileName
     * @param integer $storageUid
     * @param string $folder
     * @return File
     */
    public function addFile($filePath, $fileName, $storageUid = 0, $folder = '')
    {
        $file = null;
        $storage = $this->getFileStorage($storageUid);

        if ($storage->isWritable() && file_exists($filePath)) {
            $targetFolder = $storage->getRootLevelFolder();
            foreach (explode('/', $folder) as $folderName) {
                if ($folderName) {
                    if ($storage->hasFolderInFolder($folderName, $targetFolder)) {
                        $targetFolder = $targetFolder->getSubfolder($folderName);
                    } else {
                        $targetFolder = $targetFolder->createFolder($folderName);
                    }
                }
            }

            $file = $targetFolder->addFile($filePath, $fileName, 'changeName');
            if ($file) {
                // indexes file
                $file->updateProperties([]);
            }
        }

        return $file;
    }

    /**
     * @param File $file
     * @param integer $storageUid
     * @param string $folder
     * @return File
     */
    public function moveFile(File $file, $storageUid = 0, $folder = '')
    {
        $storage = $this->getFileStorage($storageUid);
        if ($storage->isWritable()) {
            $targetFolder = $storage->getRootLevelFolder();
            foreach (explode('/', $folder) as $folderName) {
                if ($folderName) {
                    if ($storage->hasFolderInFolder($folderName, $targetFolder)) {
                        $targetFolder = $targetFolder->getSubfolder($folderName);
                    } else {
                        $targetFolder = $targetFolder->createFolder($folderName);
                    }
                }
            }

            $file->moveTo($targetFolder);
        }

        return $file;
    }

    /**
     * @param string $fileName
     * @param integer $storageUid
     * @param string $folder
     * @return File
     */
    public function getFile($fileName, $storageUid = 0, $folder = '')
    {
        $file = null;
        $storage = $this->getFileStorage($storageUid);
        if ($storage->isWritable()) {
            $fileFolder = $storage->getRootLevelFolder();
            foreach (explode('/', $folder) as $folderName) {
                if ($folderName) {
                    if ($storage->hasFolderInFolder($folderName, $fileFolder)) {
                        $fileFolder = $fileFolder->getSubfolder($folderName);
                    } else {
                        $fileFolder = $fileFolder->createFolder($folderName);
                    }
                }
            }

            if ($storage->hasFile($fileFolder->getIdentifier() . $fileName)) {
                /** @var File $file */
                $file = $storage->getFile($fileFolder->getIdentifier() . $fileName);
            }
        }

        return $file;
    }

    /**
     * @param integer $storageUid
     * @return \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    public function getFileStorage($storageUid = 0)
    {
        if (!array_key_exists($storageUid, $this->fileStorage)) {
            $factory = $this->objectManager->get('TYPO3\CMS\Core\Resource\ResourceFactory');
            $this->fileStorage[$storageUid] = $factory->getStorageObject($storageUid);
        }

        return $this->fileStorage[$storageUid];
    }

    /**
     * @param string $content
     * @param array $arguments
     * @return string
     */
    public function renderFluidContent($content, $arguments = [])
    {
        $cryptoStrong = true;
        $tempPath = PATH_site . 'typo3temp/baseext_' . md5($content) . '_' .
            substr(sha1(bin2hex(openssl_random_pseudo_bytes(22, $cryptoStrong))), 0, 10) . '.temp';

        file_put_contents($tempPath, $content);
        $rendered = $this->renderTemplate($tempPath, $arguments);
        unlink($tempPath);

        return $rendered;
    }

    /**
     * @param string $controller
     * @param string $action
     * @param array $arguments
     * @param string $ext
     * @return string
     */
    public function renderView($controller, $action, $arguments, $ext = 'html')
    {
        $templateRootPaths = $this->getPaths('templateRootPath');
        do {
            $templateRootPath = array_pop($templateRootPaths);
            $templatePathAndFilename = $templateRootPath . $controller . '/' . $action . '.' . $ext;
        } while (!file_exists($templatePathAndFilename) && count($templateRootPaths) > 0);

        return $this->renderTemplate($templatePathAndFilename, $arguments);
    }

    /**
     * @param string $templatePathAndFilename
     * @param array $arguments
     * @param string $ext
     * @return string
     */
    public function renderTemplate($templatePathAndFilename, $arguments, $ext = 'html')
    {
        $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $cObj = $this->configurationManager->getContentObject();

        $view = $this->objectManager->get(StandaloneView::class);
        $view->setFormat($ext);
        $view->getRequest()->setControllerExtensionName($this->extensionName);
        $view->getRequest()->setPluginName($this->getRequest()->getPluginName());
        $view->getRequest()->setControllerName($this->getRequest()->getControllerName());
        $view->getRequest()->setControllerActionName($this->getRequest()->getControllerActionName());

        $partialRootPaths = $this->getPaths('partialRootPath');
        if (method_exists($view, 'setPartialRootPaths')) {
            $view->setPartialRootPaths($partialRootPaths);
        } else {
            $view->setPartialRootPath($partialRootPaths[0]);
        }
        $layoutRootPaths = $this->getPaths('layoutRootPath');
        if (method_exists($view, 'setLayoutRootPaths')) {
            $view->setLayoutRootPaths($layoutRootPaths);
        } else {
            $view->setLayoutRootPath($layoutRootPaths[0]);
        }

        $view->setTemplatePathAndFilename($templatePathAndFilename);
        $settings = $this->getSettings();
        if ($arguments['settings'] && is_array($arguments['settings'])) {
            if ($settings && is_array($settings)) {
                ArrayUtility::mergeRecursiveWithOverrule($arguments['settings'], $settings);
            }
        } else {
            $arguments['settings'] = $settings;
        }

        $view->assignMultiple($arguments);
        $content = $view->render();

        $this->configurationManager->setContentObject($cObj);
        $this->configurationManager->setConfiguration($configuration);

        return $content;
    }

    /**
     * @param string $string
     * @param bool $forceLowercase
     * @param bool $anal
     * @return string
     */
    public function sanitize($string, $forceLowercase = true, $anal = false)
    {
        $strip = [
            "~",
            "`",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "*",
            "(",
            ")",
            "_",
            "=",
            "+",
            "[",
            "{",
            "]",
            "}",
            "\\",
            "|",
            ";",
            ":",
            "\"",
            "'",
            "&#8216;",
            "&#8217;",
            "&#8220;",
            "&#8221;",
            "&#8211;",
            "&#8212;",
            ",",
            "<",
            ".",
            ">",
            "/",
            "?"
        ];

        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;

        return ($forceLowercase) ?
            (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
            $clean;
    }

    /**
     * @return array
     */
    public function getExtensionConfiguration()
    {
        return $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            $this->extensionName ?: null,
            $this->request && $this->request->getPluginName() ? $this->request->getPluginName() : null
        );
    }

    /**
     * @param boolean $resolved
     * @return array
     */
    public function getSettings($resolved = true)
    {
        if (!is_array($this->settings)) {
            $this->settings = [];
        }

        if ($resolved && $this->view && !$this->resolvedSettings) {
            $this->resolvedSettings = $this->settings;
            if ($this->resolvedSettings && is_array($this->resolvedSettings)) {
                foreach ($this->resolvedSettings as $key => $setting) {
                    $this->resolvedSettings[$key] = $this->resolveSetting($setting);
                }
            }
        }

        if ($resolved && $this->resolvedSettings) {
            return $this->resolvedSettings;
        } else {
            return $this->settings;
        }
    }

    /**
     * @param mixed $setting
     * @return mixed
     */
    protected function resolveSetting($setting)
    {
        if (is_array($setting)) {
            if (array_key_exists('_typoScriptNodeValue', $setting)) {
                $setting = $this->renderTSObj($setting);
            } else {
                foreach ($setting as $key => $value) {
                    $setting[$key] = $this->resolveSetting($value);
                }

                if (isset($setting['override'])) {
                    $setting = $setting['override'];
                }
            }
        }

        return $setting;
    }

    /**
     * @param string $name
     * @return array
     */
    protected function getPaths($name)
    {
        $extensionConfiguration = $this->getExtensionConfiguration();

        $paths = [];
        if (
            is_array($extensionConfiguration['view']) && array_key_exists('templateRootPaths', $extensionConfiguration['view'])
            && is_array($extensionConfiguration['view'][$name . 's']) && count($extensionConfiguration['view'][$name . 's']) > 0
        ) {
            ksort($extensionConfiguration['view'][$name . 's']);
            foreach ($extensionConfiguration['view'][$name . 's'] as $path) {
                $paths[] = GeneralUtility::getFileAbsFileName($path);
            }
        } else {
            if ($extensionConfiguration['view'][$name]) {
                $paths[] = GeneralUtility::getFileAbsFileName($extensionConfiguration['view'][$name]);
            } else {
                switch ($name) {
                    case 'partialRootPath':
                        $defaultPath = 'Resources/Private/Partials/';
                        break;
                    case 'layoutRootPath':
                        $defaultPath = 'Resources/Private/Layouts/';
                        break;
                    case 'templateRootPath':
                    default:
                        $defaultPath = 'Resources/Private/Templates/';
                        break;
                }

                $paths[] = ExtensionManagementUtility::extPath(
                    GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName),
                    $defaultPath
                );
            }
        }

        return $paths;
    }

    /**
     * @param string $extensionName
     * @return $this
     */
    public function setExtensionName($extensionName)
    {
        $this->extensionName = $extensionName;

        return $this;
    }

    /**
     * Load arguments for a different plugin than the current
     *
     * @param string $pluginName
     */
    protected function loadArguments($pluginName = null)
    {
        if ($pluginName) {
            $arguments = GeneralUtility::_GPmerged($this->getPluginNamespace($pluginName));
        } else {
            $arguments = $_GET;
            ArrayUtility::mergeRecursiveWithOverrule($arguments, $_POST);
        }

        $this->request->setArguments($arguments);
    }

    /**
     *
     * @param string $pluginName
     * @param string $extensionName
     * @return string
     */
    protected function getPluginNamespace($pluginName = null, $extensionName = null)
    {
        $extensionService = $this->objectManager->get('TYPO3\CMS\Extbase\Service\ExtensionService');
        $pluginNamespace = $extensionService->getPluginNamespace(
            $extensionName ? $extensionName : $this->extensionName,
            $pluginName ? $pluginName : $this->request->getPluginName()
        );

        return $pluginNamespace;
    }

    /**
     * @inheritdoc
     */
    public function getFlashMessage($messageBody, $messageTitle = '', $severity = FlashMessage::OK, $storeInSession = true)
    {
        /** @var FlashMessage $flashMessage */
        $flashMessage = GeneralUtility::makeInstance(
            FlashMessage::class,
            $messageBody,
            $messageTitle,
            $severity,
            $storeInSession
        );

        return $flashMessage;
    }

    /**
     * @inheritdoc
     */
    public function addFlashMessage($messageBody, $messageTitle = '', $severity = FlashMessage::OK, $storeInSession = true)
    {
        if (!$messageBody) {
            return;
        }

        if ($this->controllerContext && is_object($this->controllerContext)) {
            parent::addFlashMessage($messageBody, $messageTitle, $severity, $storeInSession);
        } else {
            if (!is_string($messageBody)) {
                throw new \InvalidArgumentException('The message body must be of type string, "' . gettype($messageBody) . '" given.', 1243258395);
            }

            /** @var FlashMessage $flashMessage */
            $flashMessage = $this->getFlashMessage($messageBody, $messageTitle, $severity, $storeInSession);

            /** @var FlashMessageService $flashMessageService */
            $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
            $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
        }
    }

    /**
     * @return bool
     */
    public function initializeJSONView()
    {
        return $this->initializeViewByClass(JsonView::class);
    }

    /**
     * @param string $class
     * @return bool
     */
    public function initializeViewByClass($class)
    {
        /** @var ViewInterface $view */
        $view = $this->objectManager->get($class);
        $this->setViewConfiguration($view);

        if ($view->canRender($this->controllerContext) !== false) {
            $view->setControllerContext($this->controllerContext);
            if (method_exists($view, 'injectSettings')) {
                $view->injectSettings($this->getSettings());
            }
            $view->initializeView();
            $view->assign('settings', $this->getSettings());

            $this->view = $view;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param integer $pageUid
     */
    protected function initFrontend($pageUid = null)
    {
        if (!$pageUid) {
            $pageUid = 0;
        }

        if (!$this->configurationManager->getContentObject()) {
            $this->configurationManager->setContentObject(
                $this->objectManager->get('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer')
            );
        }

        if (!$GLOBALS['TSFE'] || $GLOBALS['TSFE']->id != $pageUid) {
            if ($pageUid) {
                $rootline = BackendUtility::BEgetRootLine($pageUid);
                $host = BackendUtility::firstDomainRecord($rootline);
                $_SERVER['HTTP_HOST'] = $host;
            }

            $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] = 'typo3';

            if (!is_object($GLOBALS['TT'])) {
                $GLOBALS['TT'] = $this->objectManager->get('TYPO3\CMS\Core\TimeTracker\TimeTracker');
                $GLOBALS['TT']->start();
            }

            if (!$GLOBALS['TSFE']) {
                $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
                    'TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController',
                    $GLOBALS['TYPO3_CONF_VARS'],
                    $pageUid,
                    0,
                    1
                );
            }

            $GLOBALS['TSFE']->connectToDB();
            $GLOBALS['TSFE']->initFEuser();
            $GLOBALS['TSFE']->determineId();
            $GLOBALS['TSFE']->initTemplate();
            $GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
            $GLOBALS['TSFE']->forceTemplateParsing = 1;
            $GLOBALS['TSFE']->getConfigArray();
            $GLOBALS['TSFE']->cObj = $this->configurationManager->getContentObject();

            $this->uriBuilder = $this->objectManager->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);
            /** @var \TYPO3\CMS\Extbase\Mvc\Web\Request $request */
            $request = $this->objectManager->get(\TYPO3\CMS\Extbase\Mvc\Web\Request::class);

            $this->uriBuilder->setRequest($request);
            $this->uriBuilder->initializeObject();
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}