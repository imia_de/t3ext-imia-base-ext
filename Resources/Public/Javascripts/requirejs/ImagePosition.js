/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Module: TYPO3/CMS/Backend/ImagePosition
 * Contains all logic for the image position GUI
 */
define(['jquery', 'TYPO3/CMS/Backend/Modal', 'TYPO3/CMS/Backend/Severity'], function ($, Modal, Severity) {

    /**
     *
     * @type {{margin: number, currentModal: null, positionerSelector: string, $trigger: null}}
     * @exports TYPO3/CMS/Backend/ImagePosition
     */
    var ImagePosition = {
        margin: 20,
        currentModal: null,
        positionerSelector: '.t3js-positioner-image-container > img',
        $trigger: null
    };

    /**
     * Initialize triggers
     */
    ImagePosition.initializeTrigger = function() {
        var $triggers = $('.t3js-image-position-trigger');
        $triggers.each(function(){
            if ($(this).closest('.form-wizards-items-bottom').length > 0) {
                $(this).closest('.form-wizards-wrap').find('.form-wizards-items-aside').append($(this));
            }
        });

        // Remove existing bind function
        $triggers.off('click', ImagePosition.buttonClick);
        // Bind new function
        $triggers.on('click', ImagePosition.buttonClick);
    };

    /**
     * Functions that should be bind to the trigger button
     *
     * @param {Event} e click event
     */
    ImagePosition.buttonClick = function(e) {
        e.preventDefault();
        // Prevent double trigger
        if (ImagePosition.$trigger !== $(this)) {
            ImagePosition.$trigger = $(this);
            ImagePosition.show();
        }
    };

    /**
     * Open modal with image to position
     */
    ImagePosition.show = function() {
        ImagePosition.currentModal = Modal.loadUrl(
            ImagePosition.$trigger.data('image-name'),
            Severity.notice,
            [],
            ImagePosition.$trigger.data('url'),
            ImagePosition.initializePositionerModal,
            '.modal-content'
        );

        ImagePosition.currentModal.addClass('modal-dark modal-style-dark')
            .find('.modal-dialog').css({'width': '300px', marginLeft: 'auto', marginRight: 'auto'});
    };

    /**
     * Initialize the positioner modal
     */
    ImagePosition.initializePositionerModal = function() {
        top.require(['imagesloaded'], function(imagesLoaded) {
            var $image = ImagePosition.getPositioner();

            // wait until image is loaded
            imagesLoaded($image, function() {
                var $modal = ImagePosition.currentModal.find('.modal-dialog').css('width', '');
                $modal.find('img').show();
                var $modalContent = $modal.find('.modal-content').css('width', '100%');
                var $modalPanelSidebar = $modal.find('.modal-panel-sidebar');
                var $modalPanelBody = $modal.find('.modal-panel-body').css('width', $image.width());

                // Let modal auto-fill width
                $modal.css({width: $image.width() + 300, marginLeft: 'auto', marginRight: 'auto'})
                    .addClass('modal-image-position modal-resize');
                Modal.center();

                // Wait a few microseconds before calculating available width (DOM isn't always updated direct)
                setTimeout(function() {
                    $modalPanelBody.css('width', $image.width());
                    $modal.css({width: $image.width() + 300, marginLeft: 'auto', marginRight: 'auto'})
                        .addClass('modal-image-position modal-resize');
                    Modal.center();

                    // Wait a few microseconds to let the modal resize
                    setTimeout(ImagePosition.initializePositioner, 100);
                }, 100);
            });
        });
    };

    /**
     * Initialize positioner
     */
    ImagePosition.initializePositioner = function() {
        var $image = ImagePosition.getPositioner(), positionData;

        // Give img-container same dimensions as the image
        var positionContainer = ImagePosition.currentModal.find('.t3js-positioner-image-container');
        positionContainer.css({width: $image.width(), height: $image.height()});

        var $trigger = ImagePosition.$trigger;
        var positionValue = $trigger.closest('.form-control-wrap').find('input[name="' + $trigger.data('field') + '"]').val();

        var $infoX = ImagePosition.currentModal.find('.t3js-image-position-info-pos-x');
        var $infoY = ImagePosition.currentModal.find('.t3js-image-position-info-pos-y');
        var $infoWidth = ImagePosition.currentModal.find('.t3js-image-position-info-pos-width');
        var $infoHeight = ImagePosition.currentModal.find('.t3js-image-position-info-pos-height');

        var container = $('.t3js-positioner-image-container');
        var positionInfoX = ImagePosition.currentModal.find('.t3js-image-position-info-pos-x');
        var positionInfoY = ImagePosition.currentModal.find('.t3js-image-position-info-pos-y');

        if (positionValue) {
            var position = positionValue.split(':');
            positionContainer.append('<div class="position-marker" style="top: ' + position[0] + '; left: ' + position[1] + '"><i class="fa fa-2x fa-crosshairs"></i></div>');
            positionInfoY.text(position[0]);
            positionInfoX.text(position[1]);
        } else {
            positionContainer.append('<div class="position-marker" style="top: 0; left: 0; display: none"><i class="fa fa-2x fa-crosshairs"></i></div>');
        }

        var positionMarker = positionContainer.find('.position-marker');

        positionContainer.click(function(e){
            e.preventDefault();

            positionMarker.show();

            var top = 100 / positionContainer.height() * (e.pageY - $(this).offset().top);
            var left = 100 / positionContainer.width() * (e.pageX - $(this).offset().left);

            if ($trigger.data('measure') == '%') {
                positionInfoY.text(top.toFixed(2) + '%');
                positionInfoX.text(left.toFixed(2) + '%');
            } else {
                positionInfoY.text((top * $trigger.data('image-height') / 100).toFixed(2) + 'px');
                positionInfoX.text((left * $trigger.data('image-width') / 100).toFixed(2) + 'px');
            }

            positionValue = positionInfoY.text() + ':' + positionInfoX.text();
            positionMarker.css('top', top + '%');
            positionMarker.css('left', left + '%');

            return false;
        });

        ImagePosition.initializePositioningActions();
    };

    /**
     * Get image to be positioned
     *
     * @returns {Object} jQuery object
     */
    ImagePosition.getPositioner = function() {
        return ImagePosition.currentModal.find(ImagePosition.positionerSelector);
    };

    /**
     * Bind buttons from positioner tool panel
     */
    ImagePosition.initializePositioningActions = function() {
        ImagePosition.currentModal.find('[data-method]').click(function(e) {
            e.preventDefault();
            var method = $(this).data('method');
            var options = $(this).data('option') || {};
            if (typeof ImagePosition[method] === 'function') {
                ImagePosition[method](options);
            }
        });
    };

    /**
     * Save position values in form and close modal
     */
    ImagePosition.save = function() {
        var $image = ImagePosition.getPositioner();
        var $trigger = ImagePosition.$trigger;
        var formFieldName = $trigger.data('field');
        var $formField = $trigger.closest('.form-control-wrap').find('input[name="' + formFieldName + '"]');
        var $formFieldUsable = $trigger.closest('.form-control-wrap').find('input[type="text"]');
        var $formGroup = $formField.closest('.form-group');

        var positionInfoX = ImagePosition.currentModal.find('.t3js-image-position-info-pos-x');
        var positionInfoY = ImagePosition.currentModal.find('.t3js-image-position-info-pos-y');
        var newValue = positionInfoY.text() + ':' + positionInfoX.text();
        if (newValue == ':') {
            newValue = '';
        }

        $formGroup.addClass('has-change');

        $formField.val(newValue);
        $formFieldUsable.val(newValue);
        $formFieldUsable.trigger('change').focus().blur();

        ImagePosition.dismiss();
    };

    /**
     * Reset position selection
     */
    ImagePosition.reset = function() {
        var $image = ImagePosition.getPositioner();
        ImagePosition.currentModal.find('.position-marker').hide();

        var positionInfoY = ImagePosition.currentModal.find('.t3js-image-position-info-pos-y');
        var positionInfoX = ImagePosition.currentModal.find('.t3js-image-position-info-pos-x');

        positionInfoY.text('');
        positionInfoX.text('');
    };

    /**
     * Close the current open modal
     */
    ImagePosition.dismiss = function() {
        if (ImagePosition.currentModal) {
            ImagePosition.currentModal.modal('hide');
            ImagePosition.currentModal = null;
        }
    };

    return ImagePosition;
});
