<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE == 'BE') {
    /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'actions-system-cache-clear-imia-ext',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        ['source' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/CacheExt.png']
    );

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/backend.php']['constructPostProcess'][$_EXTKEY] =
        \IMIA\ImiaBaseExt\Hook\Backend\BackendController::class . '->constructPostProcess';

    $GLOBALS['TBE_STYLES']['inDocStyles_TBEstyle'] .= "\n" . \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Resources/Public/Stylesheets/doc.css');

    $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
    if ($extConf['cacheWarmer']) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'IMIA.' . $_EXTKEY,
            'system',
            'cachewarmer',
            '',
            [
                'Backend\CacheWarmer' => 'index',
            ],
            [
                'access' => 'admin',
                'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Images/Icons/CacheWarmer.svg',
                'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/ModuleCacheWarmer.xlf',
            ]
        );
    }
}